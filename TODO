<!-- vi: set ft=mkd ts=8 sw=2 et sts=2: -->
# BoundaryPatch

* Extract BoundaryPatch::setup() method. Perhaps we should make it
  a static method of some BoundaryPatchFactory class.

* Reimplement BoundaryPatch using the new EntitySeed.  This should make iteration
  over faces much faster.

* Add methods to create a BoundaryPatch containing the complement of another one.

* Implement generic boundary assemblers on BoundaryPatches. Possible ingredients:
  * Implement a FaceLocalFiniteElement that wraps the intersection coordinate
    to element coordinate transformation and LFE evaluation.
  * Use the same local assemblers for intersections exploiting the similarity
    to the entity interface.
  * Replace surfmassmatrix.hh by generic assemblers.

* Test generic BoundaryPatch prologation for leaf BoundaryPatch with mixed
  UGGrid<3> and parametrized boundary.

* Implement BoundaryPatch prologation for hierarchy of level BoundaryPatches using the Face class.

* Remove UG-specific prolongation implementations for BoundaryPatches.

* Rename the typedef ::iterator to ::Iterator

# Documentation

* Document, what a NewPFEAssembler does!

# Other stuff

* Perhaps merge staticmatrixtools.hh, and lumpmatrix.hh to matrixtools.hh
* Remove sampleonbitfield.hh?
* Don't just call methods, but do real testing in dunepythontest.cc?
* Move dunepython.hh to separate dune module?
* normalstressboundaryassembler:
  * do not pass a bare pointer

# Build system

* Add all flags to all targets, which was decided on the last developer meeting(2014)
