// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_SAMPLE_ON_BITFIELD_HH
#define DUNE_FUFEM_SAMPLE_ON_BITFIELD_HH

#include <dune/grid/common/grid.hh>
#include <dune/grid/common/rangegenerators.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/common/version.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include <dune/fufem/functions/basisgridfunction.hh>

/** \brief Prolong given coarse boundary data to all levels of a grid hierarchy.
 *
 *  \param grid The grid hierarchy.
 *  \param dirichletValues The vector to prolong the data from, the first entry is assumed to contiain the
 *                         coarse data.
 *  \param dirichletNodes  A hierarchy of bitfields that contains information which nodes have attached coarse data.
 */
template <class GridType, class VectorType>
void sampleOnBitField(const GridType& grid,
                      std::vector<VectorType>& dirichletValues,
                      const std::vector<Dune::BitSetVector<VectorType::block_type::dimension> >& dirichletNodes)
{
    // We assume that VectorType is a block vector
    const int blocksize = VectorType::block_type::dimension;

    typedef typename GridType::template Codim<0>::Entity Element;

    const int dim = GridType::dimension;

    assert((int) dirichletNodes.size()>=grid.maxLevel()+1);
    assert((int) dirichletValues.size()>=grid.maxLevel()+1);
    assert((int) dirichletValues[0].size()==grid.size(0,dim));

    // Construct GridFunction
    typename GridType::LevelGridView levelView = grid.levelGridView(0);
    typedef Dune::Functions::PQ1NodalBasis<typename GridType::LevelGridView> DuneP1Basis;
    DuneP1Basis duneP1Basis(levelView);
    typedef DuneFunctionsBasis<DuneP1Basis> P1Basis;
    P1Basis p1Basis(duneP1Basis);

    BasisGridFunction<P1Basis, VectorType> coarseFunction(p1Basis, dirichletValues[0]);

    // Set array sizes correctly
    for (int i=1; i<=grid.maxLevel(); i++) {
        dirichletValues[i].resize(grid.size(i, dim));
        dirichletValues[i] = 0;
    }

    // Loop over the elements on level 0
    for (const auto& e : elements(levelView)) {

        // Loop over all descendants of this element
        for (const auto& h : descendantElements(e,grid.maxLevel())) {

            for (size_t i=0; i<h.subEntities(dim); i++) {

                int fIdx = grid.levelIndexSet(h.level()).subIndex(h, i, dim);

                // Logical position of the fine grid lagrange point
                // in local coordinates of the coarse grid element
                Element element(h);
                Dune::FieldVector<double,dim> pos = Dune::ReferenceElements<double, dim>::general(h.type()).position(i,dim);

                while (element.level() != 0){

                    pos = element.geometryInFather().global(pos);
                    element = element.father();

                }

                typename VectorType::value_type tmp;
                coarseFunction.evaluateLocal(e, pos, tmp);

                for (int j=0; j<blocksize; j++)
                    if (dirichletNodes[h.level()][fIdx][j])
                        dirichletValues[h.level()][fIdx][j] = tmp[j];

            }

        }

    }

}


/** \brief Prolong given coarse boundary data to the leaf grid.
 *
 *  \param grid The grid hierarchy.
 *  \param coarseDirichletValues The vector containing the coarse data.
 *  \param fineDirichletValues Reference to the vector to contain the leaf data
 *  \param dirichletNodes  A hierarchy of bitfields that contains information which nodes have attached coarse data.
 */
template <class GridType, class VectorType>
void sampleOnBitField(const GridType& grid,
                      const VectorType& coarseDirichletValues,
                      VectorType& fineDirichletValues,
                      const std::vector<Dune::BitSetVector<VectorType::block_type::dimension> >& dirichletNodes)
{
    // We assume that VectorType is a block vector
    const int blocksize = VectorType::block_type::dimension;

    typedef typename GridType::template Codim<0>::Entity Element;

    const int dim = GridType::dimension;

    assert((int) dirichletNodes.size()>=grid.maxLevel()+1);
    assert((int) coarseDirichletValues.size()==grid.size(0,dim));

    // Construct GridFunction
    typedef Dune::Functions::PQ1NodalBasis<typename GridType::LevelGridView> DuneP1Basis;
    DuneP1Basis duneP1Basis(grid.levelGridView(0));
    typedef DuneFunctionsBasis<DuneP1Basis> P1Basis;
    P1Basis p1Basis(duneP1Basis);
    BasisGridFunction<P1Basis, VectorType> coarseFunction(p1Basis, coarseDirichletValues);

    // Set array sizes correctly
    fineDirichletValues.resize(grid.size(dim));
    fineDirichletValues = 0;

    typename GridType::LeafGridView leafView = grid.leafGridView();
    const auto& indexSet = leafView.indexSet();

    // Loop over the leaf elements
    for (const auto& e : elements(leafView)) {

        for (size_t i=0; i<e.subEntities(dim); i++) {

            int fIdx = indexSet.subIndex(e, i, dim);

            // Logical position of the fine grid lagrange point
            // in local coordinates of the coarse grid element
            Element element(e);
            Dune::FieldVector<double,dim> pos = Dune::ReferenceElements<double, dim>::general(e.type()).position(i,dim);

            while (element.level() != 0){

                pos = element.geometryInFather().global(pos);
                element = element.father();

            }

            typename VectorType::value_type tmp;
            coarseFunction.evaluateLocal(element, pos, tmp);

            for (int j=0; j<blocksize; j++)
                 if (dirichletNodes[e.level()][fIdx][j])
                     fineDirichletValues[fIdx][j] = tmp[j];
        }

    }

}


/** \brief Prolong given coarse boundary data to the leaf grid.
 *
 *  \param grid The grid hierarchy.
 *  \param coarseDirichletValues The vector containing the coarse data.
 *  \param fineDirichletValues Reference to the vector to contain the leaf data
 *  \param fineDirichletNodes A bitsetvector with information on which leaf nodes are attached to the coarse data.
 */
template <class GridType, class VectorType>
void sampleOnBitField(const GridType& grid,
                      const VectorType& coarseDirichletValues,
                      VectorType& fineDirichletValues,
                      const Dune::BitSetVector<VectorType::block_type::dimension>& fineDirichletNodes)
{
    // We assume that VectorType is a block vector
    const int blocksize = VectorType::block_type::dimension;
    typedef typename GridType::template Codim<0>::Entity Element;

    const int dim = GridType::dimension;

    assert((int) coarseDirichletValues.size()==grid.size(0,dim));

    // Construct GridFunction
     typedef Dune::Functions::PQ1NodalBasis<typename GridType::LevelGridView> DuneP1Basis;
    DuneP1Basis duneP1Basis(grid.levelGridView(0));
    typedef DuneFunctionsBasis<DuneP1Basis> P1Basis;
    P1Basis p1Basis(duneP1Basis);
    BasisGridFunction<P1Basis, VectorType> coarseFunction(p1Basis, coarseDirichletValues);

    // Set array sizes correctly
    fineDirichletValues.resize(grid.size(dim));
    fineDirichletValues = 0;

    typename GridType::LeafGridView leafView = grid.leafGridView();
    const auto& indexSet = leafView.indexSet();

    // Loop over the leaf elements
    for (const auto& e : elements(leafView)) {

        for (size_t i=0; i<e.subEntities(dim); i++) {

            int fIdx = indexSet.subIndex(e, i, dim);

            // Logical position of the fine grid lagrange point
            // in local coordinates of the coarse grid element
            Element element(e);
            Dune::FieldVector<double,dim> pos = Dune::ReferenceElements<double, dim>::general(e.type()).position(i,dim);

            while (element.level() != 0){

                pos = element.geometryInFather().global(pos);
                element = element.father();

            }

            typename VectorType::value_type tmp;
            coarseFunction.evaluateLocal(element, pos, tmp);

            for (int j=0; j<blocksize; j++)
                 if (fineDirichletNodes[fIdx][j])
                     fineDirichletValues[fIdx][j] = tmp[j];

        }

    }

}

#endif
