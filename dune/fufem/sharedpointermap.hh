#ifndef SHARED_POINTER_MAP_HH
#define SHARED_POINTER_MAP_HH

#include <map>
#include <memory>

#include <dune/common/exceptions.hh>

/** \brief Simple map for shared pointers
 *
 * This class aims to make the usage of maps
 * of shared pointers more convenient.
 * It extends a map<K, shared_ptr<V> >.
 *
 * \tparam K key type
 * \tparam V value typ
 * \tparam Compare the comparator (defaults to std::less)
 */
template <class K, class V, class Compare = std::less<K> >
class SharedPointerMap :
    public std::map<K, std::shared_ptr<V>, Compare>
{
        typedef typename std::shared_ptr<V> VSP;

    public:
        typedef typename std::map<K, VSP, Compare> Base;

        /** \brief Get a const shared ptr for given key
         *
         * This throws an exception if the key was not found.
         */
        const VSP& getPointer(const K& key) const
        {
            typename Base::const_iterator it = this->find(key);
            if (it == this->end())
                DUNE_THROW(Dune::Exception, "Key not found");
            return it->second;
        }

        /** \brief Get a shared ptr for given key
         *
         * This throws an exception if the key was not found.
         */
        VSP& getPointer(const K& key)
        {
            typename Base::iterator it = this->find(key);
            if (it == this->end())
                DUNE_THROW(Dune::Exception, "Key not found");
            return it->second;
        }

        /** \brief Get const refrence to value for given key
         *
         * This throws an exception if the key was not found.
         */
        const V& get(const K& key) const
        {
            return *getPointer(key);
        }

        /** \brief Get refrence to value for given key
         *
         * This throws an exception if the key was not found.
         */
        V& get(const K& key)
        {
            return *getPointer(key);
        }

        /** \brief Set key to value
         *
         * This creates a shared pointer for the given raw pointer
         * and takes ownership.
         */
        void set(const K& key, V* value)
        {
            this->operator[](key) = VSP(value);
        }

};

#endif
