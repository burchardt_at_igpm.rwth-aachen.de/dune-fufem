#ifndef P2_HIERARCHICAL_WITH_ELEMENT_BUBBLES_BASIS_HH
#define P2_HIERARCHICAL_WITH_ELEMENT_BUBBLES_BASIS_HH

/**
   @file
   @brief

   @author
 */

#include <dune/geometry/type.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/localfunctions/hierarchical/hierarchicalp2withelementbubble.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>



template <class GV, class RT=double>
class P2HierarchicalWithElementBubblesBasis :
    public FunctionSpaceBasis<GV, RT, 
        typename Dune::HierarchicalP2WithElementBubbleLocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension> >
{
    protected:
        typedef typename Dune::HierarchicalP2WithElementBubbleLocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension> LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;

    //! Parameter for mapper class
    //! In 2d we have vertex, edge, and element dofs
    template<int localDim>
    struct DofLayout
    {
        bool contains (Dune::GeometryType gt) {
            assert(gt.dim() <= 2);
            return gt.dim()==0 || gt.dim()==1 || gt.dim() == localDim;
        }
    };

    typedef Dune::MultipleCodimMultipleGeomTypeMapper<GV,DofLayout> MapperType;

    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        P2HierarchicalWithElementBubblesBasis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview),
            localFE_()
        {}

        size_t size() const
        {
            return mapper_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return localFE_;
        }

        int index(const Element& e, const int i) const
        {
            return mapper_.map(e, 
                               localFE_.localCoefficients().localKey(i).subEntity(),
                               localFE_.localCoefficients().localKey(i).codim());
        }

    protected:

    const MapperType mapper_;
        const LocalFiniteElement localFE_;
};

#endif

