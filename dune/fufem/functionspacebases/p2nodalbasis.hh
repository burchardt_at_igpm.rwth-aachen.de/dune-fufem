#ifndef P2_NODALBASIS_HH
#define P2_NODALBASIS_HH

/*
#warning This file is deprecated.  All implementations of function space bases in dune-fufem \
  are in the process of being replaced by counterparts in the new dune-functions module. \
  Those are syntactically different, but semantically very close to the dune-fufem implementations. \
  To get rid of this warning, replace all occurrences of the Q1NodalBasis<...> class in your code \
  by DuneFunctionsBasis<Dune::Functions::PQkNodalBasis<...> >.
*/

/**
   @file
   @brief

   @author
 */

#include <dune/common/version.hh>

#include <dune/geometry/type.hh>
#include <dune/common/typetraits.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/localfunctions/lagrange/pqkfactory.hh>

#include <dune/fufem/functionspacebases/functionspacebasis.hh>

template<class GridView>
class P2BasisMapper :
    public Dune::MultipleCodimMultipleGeomTypeMapper<GridView>
{
        typedef typename Dune::MultipleCodimMultipleGeomTypeMapper<GridView> Base;
        typedef typename GridView::Grid::template Codim<0>::Entity Element;

    public:

        P2BasisMapper(const GridView& gridView) :
            Base(gridView,
                 // all vertices, edges, and quadrilaterals/cubes carry a dof
                 [](Dune::GeometryType gt, int dimgrid) {return gt.dim()==0 or gt.dim()==1 or gt.isCube();})
        {}

        typename Base::Index index(const Element& e, const Dune::LocalKey& localKey) const
        {
            return this->subIndex(e, localKey.subEntity(), localKey.codim());
        }
};


template <class GV, class RT=double>
class P2NodalBasis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 2>::FiniteElementType >
{
    protected:
        typedef typename GV::Grid::ctype ctype;

        typedef typename Dune::PQkLocalFiniteElementCache<typename GV::Grid::ctype, RT, GV::dimension, 2> FiniteElementCache;
        typedef typename FiniteElementCache::FiniteElementType LFE;

        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;

        using Base::dim;
        using Base::gridview_;

    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        P2NodalBasis(const GridView& gridview) :
            Base(gridview),
            mapper_(gridview)
        {}

        size_t size() const
        {
            return mapper_.size();
        }

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            mapper_.update();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            return cache_.get(e.type());
        }

        int index(const Element& e, const int i) const
        {
            return mapper_.index(e, cache_.get(e.type()).localCoefficients().localKey(i));
        }

    protected:
        FiniteElementCache cache_;
        P2BasisMapper<GV> mapper_;
};

#endif

