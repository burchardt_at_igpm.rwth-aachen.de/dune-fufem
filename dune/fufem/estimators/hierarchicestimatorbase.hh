// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ESTIMATORS_HIERARCHIC_ESTIMATOR_BASE_HH
#define DUNE_FUFEM_ESTIMATORS_HIERARCHIC_ESTIMATOR_BASE_HH

#include <dune/common/function.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/bdmatrix.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/l2functionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/neumannboundaryassembler.hh>

template <class EnlargedGlobalBasis, int block_size, class field_type>
class HierarchicEstimatorBase
{

    using GridView = typename EnlargedGlobalBasis::GridView;
    using Grid = typename GridView::Grid;
    using Lfe = typename EnlargedGlobalBasis::LocalFiniteElement;


    enum {dim = GridView::dimension};
    enum {dimworld = GridView::dimensionworld};

    using CType = typename GridView::ctype;
    using BlockType = Dune::FieldMatrix<field_type,block_size,block_size>;
    using FieldVectorType = Dune::FieldVector<field_type,block_size>;

public:

    static void preconditionedDefectProblem(const EnlargedGlobalBasis& enlargedGlobalBasis,
                                     const Dune::BlockVector<FieldVectorType>& x,
                                     const Dune::VirtualFunction<Dune::FieldVector<CType,dimworld>, Dune::FieldVector<field_type,block_size> >* volumeTerm,
                                     const Dune::VirtualFunction<Dune::FieldVector<CType,dimworld>, Dune::FieldVector<field_type,block_size> >* neumannTerm,
                                     Dune::BlockVector<FieldVectorType>& residual,
                                     Dune::BDMatrix<BlockType>& matrix,
                                     LocalOperatorAssembler<Grid, Lfe, Lfe, BlockType>* localStiffness)
    {

        const GridView& gridView = enlargedGlobalBasis.getGridView();

        assert(x.size() == enlargedGlobalBasis.size());
        residual.resize(enlargedGlobalBasis.size());
        residual = 0;

        matrix = Dune::BDMatrix<BlockType>(enlargedGlobalBasis.size());
        matrix = 0;

        // local assembler for the volume term
        L2FunctionalAssembler<Grid, Lfe, FieldVectorType> localL2FunctionalAssembler(*volumeTerm);

        // ////////////////////////////////////////////////////////////////////////
        //   Loop over all elements and assemble diagonal entries and residual
        // ////////////////////////////////////////////////////////////////////////

        for (const auto& e : elements(gridView)) {

            // Get set of shape functions
            const auto& localFiniteElement = enlargedGlobalBasis.getLocalFiniteElement(e);
            const auto& localBasis = localFiniteElement.localBasis();

            // /////////////////////////////////////////////////////////////////////////
            //   Assemble right hand side in the enlarged space
            // /////////////////////////////////////////////////////////////////////////

            if (volumeTerm) {

                // vector for the local rhs from the volume term
                //Dune::BlockVector<Dune::FieldVector<field_type,block_size> > localVolumeVector(localBasis.size());
                typename L2FunctionalAssembler<Grid, EnlargedGlobalBasis, FieldVectorType>::LocalVector localVolumeVector(localBasis.size());

                // assemble local rhs from the volume term
                localL2FunctionalAssembler.assemble(e, localVolumeVector, localFiniteElement);

                // add to global vector
                for (size_t i=0; i<localBasis.size(); i++) {
                    int globalRow = enlargedGlobalBasis.index(e, i);
                    residual[globalRow] += localVolumeVector[i];
                }

            }

            if (neumannTerm) {

                const int quadOrder = 6;
                NeumannBoundaryAssembler<Grid, FieldVectorType> neumannAssembler(*neumannTerm,quadOrder);

                auto nIt = gridView.ibegin(e);
                auto nEndIt = gridView.iend(e);

                for (;nIt != nEndIt; ++nIt) {
                    if (!nIt->boundary())
                        continue;

                    typename NeumannBoundaryAssembler<Grid, FieldVectorType>::LocalVector localNeumannVector(localBasis.size());
                    neumannAssembler.assemble(nIt,localNeumannVector,localFiniteElement);

                    for (size_t i=0; i<localBasis.size(); i++) {
                        int globalRow = enlargedGlobalBasis.index(e, i);
                        residual[globalRow] += localNeumannVector[i];
                    }
                }
            }

            // ///////////////////////////////////////////////////
            //   Now construct the matrix and the residual
            // ///////////////////////////////////////////////////


            // Extract local solution
            Dune::BlockVector<FieldVectorType> localSolution(localBasis.size());

            for (size_t i=0; i<localBasis.size(); i++)
                localSolution[i] = x[enlargedGlobalBasis.index(e, i)];

            // Assemble element matrix with quadratic Lagrangian elements
            Dune::Matrix<BlockType> localMatrix(localBasis.size(),localBasis.size());
            localStiffness->assemble(e,localSolution, localMatrix,
                                    localFiniteElement,
                                    localFiniteElement);

            // Add to residual vector
            for (size_t i=0; i<localBasis.size(); i++) {

                int globalRow = enlargedGlobalBasis.index(e, i);

                for (size_t j=0; j<localBasis.size(); j++) {

                    int globalCol = enlargedGlobalBasis.index(e, j);

                    localMatrix[i][j].mmv(x[globalCol], residual[globalRow]);

                }

                // Assemble diagonal part of second order stiffness matrix
                matrix[globalRow][globalRow] += localMatrix[i][i];

            }

        }

    }

    /** \todo This method is not efficient.  It actually assembles the entire matrix when only
        the diagonal entries are needed!
    */
    static field_type energyNormSquared(const EnlargedGlobalBasis& enlargedGlobalBasis,
                                        const Dune::BlockVector<FieldVectorType>& x,
                                        const LocalOperatorAssembler<typename GridView::Grid,
                                        typename EnlargedGlobalBasis::LocalFiniteElement,
                                        typename EnlargedGlobalBasis::LocalFiniteElement,
                                        BlockType>& localStiffness) {

        const GridView& gridView = enlargedGlobalBasis.getGridView();

        assert(x.size() == enlargedGlobalBasis.size());
        field_type energyNormSquared = 0;

        Dune::Matrix<BlockType> localMatrix;

        // ////////////////////////////////////////////////////////////////////////
        //   Loop over all elements and assemble diagonal entries
        // ////////////////////////////////////////////////////////////////////////

        for (const auto& e : elements(gridView)) {

            // Get set of shape functions
            const auto&  enlargedGlobalFiniteElement = enlargedGlobalBasis.getLocalFiniteElement(e);

            localMatrix.setSize(enlargedGlobalFiniteElement.localBasis().size(), enlargedGlobalFiniteElement.localBasis().size());

            // Extract local solution
            Dune::BlockVector<FieldVectorType> localSolution(enlargedGlobalFiniteElement.localBasis().size());

            for (size_t i=0; i<enlargedGlobalFiniteElement.localBasis().size(); i++)
                localSolution[i] = x[enlargedGlobalBasis.index(e, i)];

            // Assemble element matrix with quadratic Lagrangian elements
            localStiffness.assemble(e, localSolution, localMatrix,
                                    enlargedGlobalFiniteElement,
                                    enlargedGlobalFiniteElement);

            // Add to the total energy
            for (size_t i=0; i<enlargedGlobalFiniteElement.localBasis().size(); i++) {

                int globalRow = enlargedGlobalBasis.index(e, i);
                FieldVectorType smallTmp(0);
                localMatrix[i][i].umv(x[globalRow], smallTmp);
                energyNormSquared += smallTmp * x[globalRow];

            }

        }
        return energyNormSquared;
    }

};

#endif
