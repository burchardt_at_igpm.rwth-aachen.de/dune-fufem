// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ESTIMATORS_REFINEMENT_INDICATOR_HH
#define DUNE_FUFEM_ESTIMATORS_REFINEMENT_INDICATOR_HH

#include <array>
#include <map>

#include <dune/localfunctions/common/localkey.hh>

/** \brief Holds refinement indicators for a hierarchical grid

Refinement indicators are produced by error estimators, and they are used
by marking strategies to mark grid elements for refinement and coarsening.
Error indicators are sets of number, each of which is associated to an
entity of the grid.
*/
template <class GridType>
class RefinementIndicator
{

    typedef double field_type;
    typedef typename GridType::LocalIdSet IdSet;
    typedef typename GridType::template Codim<0>::Entity Element;
    typedef typename IdSet::IdType IdType;

    enum {dim = GridType::dimension};

public:

    /** \brief Set up the indicator for a given grid */
    RefinementIndicator(const GridType& grid)
        : grid_(&grid)
    {}

    /** \brief Erase all entries */
    void clear() {
        for (size_t i=0; i<indicators_.size(); i++)
            indicators_[i].clear();
    }

    /** \brief Set indicators from a coefficient vector for the given basis.
    */
    template<class Basis, class Coefficients>
    void setIndicator(const Basis& basis, const Coefficients& coefficients);

    /** \brief Set indicator associated with a subentity of an element 
    */
    void set(const Element& element, unsigned int codim, unsigned int subEntity, const field_type);

    /**
     * \brief Get indicator associated with a subentity of an element 
     *
     * \return Zero, if no indicator is associated with the requested subentity
     */
    field_type value (const Element& element, unsigned int codim, unsigned int subEntity) const;

private:

    const GridType* grid_;

    // One map per codimension
    std::array<std::map<IdType, field_type>, dim+1> indicators_;

};

template <class GridType>
template <class Basis, class Coefficients>
void RefinementIndicator<GridType>::
setIndicator(const Basis& basis, const Coefficients& coefficients)
{
    // /////////////////////////////////////////////////////////////////
    //   Create maps that store the error for all elements of a codimension of
    //   a grid and which can be traversed in sorted order.
    // /////////////////////////////////////////////////////////////////
    //

    typedef typename Basis::GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename Basis::LocalFiniteElement LocalFiniteElement;

    ElementIterator it = basis.getGridView().template begin<0>();
    ElementIterator end = basis.getGridView().template end<0>();
    for(; it!= end; ++it)
    {
        const LocalFiniteElement& fe = basis.getLocalFiniteElement(*it);

        for (size_t i=0; i<fe.localBasis().size(); ++i)
        {
            int index = basis.index(*it, i);
            if (not (basis.isConstrained(index)))
            {
                const Dune::LocalKey& localKey = fe.localCoefficients().localKey(i);
                set(*it, localKey.codim(), localKey.subEntity(), coefficients[index].two_norm());
            }
        }
    }
}


// Set indicator associated with a subentity of an element
template <class GridType>
void RefinementIndicator<GridType>::
set(const Element& element, unsigned int codim, unsigned int subEntity, const field_type value)
{
    IdType id = grid_->localIdSet().subId(element, subEntity, codim);

    indicators_[codim][id] = value;
}


/* Get indicator associated with a subentity of an element */
template <class GridType>
typename RefinementIndicator<GridType>::field_type RefinementIndicator<GridType>::
value (const Element& element, unsigned int codim, unsigned int subEntity) const
{
    IdType id = grid_->localIdSet().subId(element, subEntity, codim);
    typename std::map<IdType,field_type>::const_iterator indicatorEntry = indicators_[codim].find(id);
    return (indicatorEntry != indicators_[codim].end()) ? indicatorEntry->second : 0;
}

#endif
