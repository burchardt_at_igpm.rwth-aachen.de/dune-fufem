// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_BOUNDARY_WRITER_HH
#define DUNE_FUFEM_BOUNDARY_WRITER_HH

#include <dune/common/bitsetvector.hh>
#include "boundarypatch.hh"
#include <dune/geometry/referenceelements.hh>
#include <dune/istl/bvector.hh>

template <class GridView>
void writeBoundary(const BoundaryPatch<GridView>& surface, const std::string& filename)
{
    typedef typename GridView::ctype field_type;
    enum {dim = GridView::dimension};

    const GridView& gv = surface.gridView();

    if (dim!=3 && dim !=2)
        DUNE_THROW(Dune::NotImplemented, "writeBoundary is only implemented for 2D/3D-grids!");
    if (dim==2) {
        writeLineSet(surface,filename);
        return;
    }
    const typename GridView::Traits::IndexSet& indexSet = gv.indexSet();

    Dune::BitSetVector<1> surfVertices;
    surface.getVertices(surfVertices);

    std::vector<int> globalToLocal;
        surface.makeGlobalToLocal(globalToLocal);

    // ////////////////////////////////////////////
    //   Write header
    // ////////////////////////////////////////////

    FILE* fp = fopen(filename.c_str(), "w");

    if (!fp)
        DUNE_THROW(Dune::IOError, "Couldn't open '" << filename << "' for writing!");

    fprintf(fp, "# HyperSurface 0.1 ASCII\n");
    fprintf(fp, "\n");
    fprintf(fp, "Parameters {\n");
    fprintf(fp, "    Materials {\n");
    fprintf(fp, "        outside {\n");
    fprintf(fp, "            Id 0\n");
    fprintf(fp, "        }\n");
    fprintf(fp, "        inside {\n");
    fprintf(fp, "            Id 1\n");
    fprintf(fp, "        }\n");
    fprintf(fp, "    }\n");
    fprintf(fp, "\n");
    fprintf(fp, "}\n");

    // ////////////////////////////////////////////
    //   Write vertices
    // ////////////////////////////////////////////

    fprintf(fp, "\nVertices %lu\n", surfVertices.count());

    Dune::BlockVector<Dune::FieldVector<field_type, dim> > coords(gv.size(dim));

    // resort the relevant coordinates so they appear ordered by their entities' indices
    for (const auto& v : vertices(gv))
        if (surfVertices[indexSet.index(v)][0])
            coords[indexSet.index(v)] = v.geometry().corner(0);

    // write coordinates
    for (size_t i=0; i<coords.size(); i++)
        if (surfVertices[i][0])
            fprintf(fp, "        %g %g %g\n", coords[i][0], coords[i][1], coords[i][2]);

    // ////////////////////////////////////////////
    //   Write triangles
    // ////////////////////////////////////////////

    fprintf(fp, "NBranchingPoints 0\n");
    fprintf(fp, "NVerticesOnCurves 0\n");
    fprintf(fp, "BoundaryCurves 0\n");
    fprintf(fp, "Patches 1\n");
    fprintf(fp, "{\n");
    fprintf(fp, "InnerRegion inside\n");
    fprintf(fp, "OuterRegion outside\n");
    fprintf(fp, "BoundaryID 0\n");
    fprintf(fp, "BranchingPoints 0");
    fprintf(fp, "\n");

    // Count all boundary segments.  We have to do this manually because
    // quadrilaterals need to be transformed to two triangles because
    // Amira doesn't know quadrilateral surface :-(((
    int numFaces = 0;

    // loop over all elements
    for (const auto it : surface) {

        switch (it.geometry().corners()) {
        case 3:
            numFaces++;
            break;
        case 4:
            numFaces += 2;
            break;
        default:
            DUNE_THROW(Dune::NotImplemented, "Unknown boundary segment type encountered!");
        }

    }

    fprintf(fp, "Triangles %d\n", numFaces);

    // loop over all elements
    for (const auto it : surface) {

        const auto& inside = it.inside();

        const auto& refElement = Dune::ReferenceElements<field_type, dim>::general(inside.type());

        int n = refElement.size(it.indexInInside(), 1, dim);

        fprintf(fp, "  %d %d %d\n",
                globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 0, dim), dim)] + 1,
                globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 1, dim), dim)] + 1,
                globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 2, dim), dim)] + 1);

        if (n==4)
            fprintf(fp, "  %d %d %d\n",
                    globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 3, dim), dim)] + 1,
                    globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 2, dim), dim)] + 1,
                    globalToLocal[indexSet.subIndex(inside, refElement.subEntity(it.indexInInside(), 1, 1, dim), dim)] + 1);

    }

    fprintf(fp, "}\n");

    fclose(fp);

    std::cout << "Boundary surface successfully written to: " << filename << std::endl;

}


template <class GridView, class DofType> void writeBoundaryField(
        const BoundaryPatch<GridView>& surface,
        const Dune::BlockVector<DofType>& field, const std::string& filename) {
    FILE* fp = fopen(filename.c_str(), "w");

    if (!fp)
        DUNE_THROW(Dune::IOError, "Couldn't open '" << filename << "' for writing!");

    fprintf(fp, "# AmiraMesh 3D ASCII 2.0\n");
    fprintf(fp, "# CreationDate: Wed Dec 22 14:54:06 2004\n");
    fprintf(fp, "\n\n\n");

    fprintf(fp, "nNodes %d\n\n", field.size());

    fprintf(fp, "Parameters {\n");
    fprintf(fp, "    ContentType \"SurfaceField\",\n");
    fprintf(fp, "    Encoding \"OnNodes\"\n");
    fprintf(fp, "}\n\n");

    if (DofType::dimension==1)
        fprintf(fp, "NodeData { float values } @1\n\n");
    else
        fprintf(fp, "NodeData { float[%d] values } @1\n\n", DofType::dimension);

    fprintf(fp, "# Data section follows\n");
    fprintf(fp, "@1\n");

    for (size_t i=0; i<field.size(); i++) {
        for (int j=0; j<DofType::dimension; j++)
            fprintf(fp, "%f ", field[i][j]);
        fprintf(fp, "\n");
    }

    fclose(fp);

    std::cout << "Boundary field successfully written to: " << filename << std::endl;

}

template <class GridView>
void writeLineSet(const BoundaryPatch<GridView>& surface, const std::string& filename)
{
        std::ofstream fgrid;

        fgrid.open(filename.c_str());

        fgrid << "# AmiraMesh 3D ASCII 2.0 \n";
        fgrid<<"\n";
        fgrid<<"define Lines "<<3*surface.numFaces()<<"\n";
        fgrid<<"nVertices "<<2*surface.numFaces()<<"\n";
        fgrid<<"\n";
        fgrid<<"Parameters {\n";
        fgrid<<"    ContentType \"HxLineSet\" \n";
        fgrid<<"}\n";
        fgrid<<"\n";
        fgrid<<"Lines { int LineIdx } @1\n";
        fgrid<<"Vertices { float[3] Coordinates } @2\n";
        fgrid<<"\n";
        fgrid<<"# Data section follows\n";
        fgrid<<"@1 \n";
        for (int i=0; i<surface.numFaces();i++)
            fgrid<<2*i<<"\n"<<2*i+1<<"\n"<<-1<<"\n";
        fgrid<<"\n";
        fgrid<<"@2 \n";

        for (const auto& it : surface) {
            auto geom = it.geometry();
            for (int i = 0; i <2; ++i)
                fgrid << geom.corner(i) <<" "<<0<<"\n";
        }
        fgrid.close();
}

#endif
