#ifndef DUNEDATAIO_HH
#define DUNEDATAIO_HH

#ifdef HAVE_BOOST_SERIALIZATION

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <boost/serialization/split_free.hpp>

/* supplies serialize functions for several Dune types in order to be able to use boost::serialization on them */
namespace boost {
namespace serialization {

/* as we need not not do fundamentally different things in saving and loading we can supply only one function. 
 * mark the use of the operator & that works both ways, << or >> , depending on the type of Archive.
 */
template <class Archive, class T, int n>
void serialize(Archive& ar, Dune::FieldVector<T,n>& fvec, const unsigned int version DUNE_UNUSED)
{
    for (int i=0; i<n; ++i)
        ar & fvec[i];
}

/* For Dune::BlockVectors we need to store the size separately and treat it separately during loading. Thus we need 
 * two separate save() and load() functions later to be "unified" via boost::serialization::split_free (see below)
 */
template <class Archive, class BlockType>
void save(Archive& ar, const Dune::BlockVector<BlockType>& vec, const unsigned int version DUNE_UNUSED)
{
    size_t size = vec.size();
    ar << size;
    for (size_t i=0; i<vec.size(); ++i)
        ar << vec[i];
}

template <class Archive, class BlockType>
void load(Archive& ar, Dune::BlockVector<BlockType>& vec, const unsigned int version DUNE_UNUSED)
{
    size_t size;
    ar >> size;
    vec.resize(size);
    for (size_t i=0; i<size; ++i)
        ar >> vec[i];
}

template<class Archive, class BlockType>
void serialize(Archive& ar, Dune::BlockVector<BlockType>& vec, const unsigned int version)
{
    boost::serialization::split_free(ar, vec, version);
}

}} // ends of namespaces serialization and boost

#else
#warning dunedataio.hh was included but boost_serialization was not found !
#endif

#endif

