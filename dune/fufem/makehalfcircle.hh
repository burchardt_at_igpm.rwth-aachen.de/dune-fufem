#ifndef DUNE_FUFEM_MAKE_HALF_CIRCLE_HH
#define DUNE_FUFEM_MAKE_HALF_CIRCLE_HH

#include <cmath>
#include <memory>
#include <vector>

#include <dune/common/shared_ptr.hh>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/boundarysegment.hh>

#include "arcofcircle.hh"

template<class GridType>
std::unique_ptr<GridType> createCircle(const Dune::FieldVector<double,2>& center, double r)
{
    static_assert(GridType::dimension==2, "createCircle can only be instantiated for 2d grids");
    
    using namespace Dune;

    Dune::GridFactory<GridType> factory;

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////
    double x = std::sqrt((r*r)/2.0);
    FieldVector<double,2> pos;

    pos[0] = center[0]-x;  pos[1] = center[1]-x;
    factory.insertVertex(pos);

    pos[0] = center[0]+x;  pos[1] = center[1]-x;
    factory.insertVertex(pos);

    pos[0] = center[0]-x;  pos[1] = center[1]+x;
    factory.insertVertex(pos);

    pos[0] = center[0]+x;  pos[1] = center[1]+x;
    factory.insertVertex(pos);

    factory.insertVertex(center);


    // /////////////////
    // Insert elements
    // /////////////////
    std::vector<unsigned int> cornerIDs(3);

    cornerIDs[0] = 4;
    cornerIDs[1] = 0;
    cornerIDs[2] = 1;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 4;
    cornerIDs[1] = 1;
    cornerIDs[2] = 3;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 4;
    cornerIDs[1] = 3;
    cornerIDs[2] = 2;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 4;
    cornerIDs[1] = 2;
    cornerIDs[2] = 0;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);


    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    vertices[0] = 0;  vertices[1] = 1;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, r, M_PI*5/4, M_PI*7/4)));

    vertices[0] = 1;  vertices[1] = 3;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, r, M_PI*7/4, M_PI*9/4)));

    vertices[0] = 3;  vertices[1] = 2;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, r, M_PI*1/4, M_PI*3/4)));

    vertices[0] = 2;  vertices[1] = 0;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, r, M_PI*3/4, M_PI*5/4)));


    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    return std::unique_ptr<GridType>(factory.createGrid());
}



template <class GridType>
void makeHalfCircle(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircle can only be instantiated for 2d grids");
    
    using namespace Dune;

    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////

    FieldVector<double,2> center(0);
    center[1] = 15;

    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    vertices[0] = 1;  vertices[1] = 2;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));

    vertices[0] = 2;  vertices[1] = 3;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));

    vertices[0] = 3;  vertices[1] = 0;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));


    // ////////////////////////
    //   Insert vertices
    // ////////////////////////
    Dune::FieldVector<double,2> pos;

    pos[0] = 15;   pos[1] = 15;
    factory.insertVertex(pos);

    pos[0] = -15;  pos[1] = 15;
    factory.insertVertex(pos);

    pos[0] = -7.5; pos[1] = 2.00962;
    factory.insertVertex(pos);

    pos[0] = 7.5;  pos[1] = 2.00962;
    factory.insertVertex(pos);

    // /////////////////
    // Insert elements
    // /////////////////

    std::vector<unsigned int> cornerIDs(3);
    cornerIDs[0] = 0;
    cornerIDs[1] = 1;
    cornerIDs[2] = 2;

    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 2;
    cornerIDs[1] = 3;
    cornerIDs[2] = 0;

    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();

}

template <class GridType>
void makeHalfCircleQuadTri(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleQuadTri can only be instantiated for 2d grids");
    
    using namespace Dune;

    Dune::GridFactory<GridType> factory(&grid);

    // ////////////////////
    //   Create the domain
    // //////////////////////
    FieldVector<double,2> center(0);
    center[1] = 0.4;

    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    vertices[0] = 1;  vertices[1] = 8;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI, M_PI*9/8)));

    vertices[0] = 8;  vertices[1] = 2;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*9/8, M_PI*10/8)));

    vertices[0] = 2;  vertices[1] = 4;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*10/8, M_PI*12/8)));

    vertices[0] = 4;  vertices[1] = 3;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*12/8, M_PI*14/8)));

    vertices[0] = 3;  vertices[1] = 9;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*14/8, M_PI*15/8)));

    vertices[0] = 9;  vertices[1] = 0;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 0.4, M_PI*15/8, M_PI*2)));

    // ////////////////////
    //  Insert inner nodes
    // ////////////////////
    double nodePos[15][2] = {{0.4, 0.4},
                             {-0.4, 0.4},
                             {-0.282843, 0.117157},
                             {0.282843, 0.117157},
                             {-7.34788e-17, 0},
                             {0, 0.4},
                             {0.2, 0.4},
                             {-0.2, 0.4},
                             {-0.369552, 0.246927},
                             {0.369552, 0.246927},
                             {0.113395, 0.270427},
                             {0.170737, 0.329338},
                             {-0.170737, 0.329338},
                             {-0.113395, 0.270427},
                             {1.25898e-10, 0.235214}};
    
    for (int i=0; i<15; i++)
        factory.insertVertex(*((FieldVector<double,2>*)nodePos[i]));

    // /////////////////
    // Insert elements
    // /////////////////

    int cornerIDs[12][4] = {{5, 11, 6, -1},
                            {5, 10, 11, -1},
                            {5, 14, 10, -1},
                            {14, 5, 13, -1},
                            {13, 5, 12, -1},
                            {5, 7, 12, -1},
                            {7, 1, 12, 8},
                            {12, 8, 13, 2},
                            {13, 2, 14, 4},
                            {14, 4, 10, 3},
                            {10, 3, 11, 9},
                            {11, 9, 6, 0}};

    for (int i=0; i<6; i++) {
        std::vector<unsigned int> vertices(3);
        vertices[0] = cornerIDs[i][0];
        vertices[1] = cornerIDs[i][1];
        vertices[2] = cornerIDs[i][2];
        factory.insertElement(Dune::GeometryTypes::simplex(2), vertices);
    }



    for (int i=0; i<6; i++) {
        std::vector<unsigned int> vertices(4);
        vertices[0] = cornerIDs[i+6][0];
        vertices[1] = cornerIDs[i+6][1];
        vertices[2] = cornerIDs[i+6][2];
        vertices[3] = cornerIDs[i+6][3];
        factory.insertElement(Dune::GeometryTypes::cube(2), vertices);
    }

    
    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();

}


template <class GridType>
void makeHalfCircleQuad(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleQuad can only be instantiated for 2d grids");
    
    using namespace Dune;

    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    FieldVector<double,2> center(0);
    center[1] = 15;

    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    vertices[0] = 1;  vertices[1] = 2;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));

    vertices[0] = 2;  vertices[1] = 3;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));

    vertices[0] = 3;  vertices[1] = 0;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////
    FieldVector<double,2> pos;

    pos[0] = 15;  pos[1] = 15;
    factory.insertVertex(pos);
  
    pos[0] = -15; pos[1] = 15;
    factory.insertVertex(pos);

    pos[0] = -7.5; pos[1] = 2.00962;
    factory.insertVertex(pos);

    pos[0] = 7.5; pos[1] = 2.00962;
    factory.insertVertex(pos);
    // /////////////////
    // Insert elements
    // /////////////////

    std::vector<unsigned int> cornerIDs(4);
    cornerIDs[0] = 0;
    cornerIDs[1] = 1;
    cornerIDs[2] = 3;
    cornerIDs[3] = 2;

    factory.insertElement(Dune::GeometryTypes::cube(2), cornerIDs);

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();

}


template <class GridType>
void makeHalfCircleThreeTris(GridType& grid)
{
    static_assert(GridType::dimension==2, "makeHalfCircleThreeTris can only be instantiated for 2d grids");
    
    using namespace Dune;

    Dune::GridFactory<GridType> factory(&grid);

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    FieldVector<double,2> center(0);
    center[1] = 15;

    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;

    vertices[0] = 1;  vertices[1] = 2;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI, M_PI*4/3)));

    vertices[0] = 2;  vertices[1] = 3;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*4/3, M_PI*5/3)));

    vertices[0] = 3;  vertices[1] = 0;
    factory.insertBoundarySegment(vertices, SegmentPointer(new ArcOfCircle(center, 15, M_PI*5/3, M_PI*2)));

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////
    FieldVector<double,2> pos;

    pos[0] = 15;  pos[1] = 15;
    factory.insertVertex(pos);
  
    pos[0] = -15; pos[1] = 15;
    factory.insertVertex(pos);

    pos[0] = -7.5; pos[1] = 2.00962;
    factory.insertVertex(pos);

    pos[0] = 7.5; pos[1] = 2.00962;
    factory.insertVertex(pos);

    pos[0] = 0;   pos[1] = 15;
    factory.insertVertex(pos);

    // /////////////////
    // Insert elements
    // /////////////////

    std::vector<unsigned int> cornerIDs(3);

    cornerIDs[0] = 1;    cornerIDs[1] = 2;    cornerIDs[2] = 4;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 2;    cornerIDs[1] = 3;    cornerIDs[2] = 4;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    cornerIDs[0] = 3;    cornerIDs[1] = 0;    cornerIDs[2] = 4;
    factory.insertElement(Dune::GeometryTypes::simplex(2), cornerIDs);

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    factory.createGrid();

}

#endif
