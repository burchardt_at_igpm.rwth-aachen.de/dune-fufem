// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_DIFFERENCE_NORM_SQUARED_HH
#define DUNE_FUFEM_DIFFERENCE_NORM_SQUARED_HH

/** \file
    \brief Helper method which computes the energy norm (squared) of the difference of
    the fe functions on two different refinements of the same base grid.
*/

#include <dune/geometry/type.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/common/fvector.hh>
#include <dune/common/version.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

template <class GridType>
class DifferenceNormSquared {

    // Grid dimension
    const static int dim = GridType::dimension;

    /** \brief Check whether given local coordinates are contained in the reference element
        \todo This method exists in the Dune grid interface!  But we need the eps.
    */
    static bool checkInside(const Dune::GeometryType& type,
                            const Dune::FieldVector<double, dim> &loc,
                            double eps)
    {
        switch (type.dim()) {

        case 0: // vertex
            return false;
        case 1: // line
            return -eps <= loc[0] && loc[0] <= 1+eps;
        case 2:

            if (type.isSimplex()) {
                return -eps <= loc[0] && -eps <= loc[1] && (loc[0]+loc[1])<=1+eps;
            } else if (type.isCube()) {
                return -eps <= loc[0] && loc[0] <= 1+eps
                    && -eps <= loc[1] && loc[1] <= 1+eps;
            } else {
                DUNE_THROW(Dune::GridError, "checkInside():  ERROR:  Unknown type " << type << " found!");
            }

        case 3:
            if (type.isSimplex()) {
                return -eps <= loc[0] && -eps <= loc[1] && -eps <= loc[2]
                    && (loc[0]+loc[1]+loc[2]) <= 1+eps;
            } else if (type.isPyramid()) {
                return -eps <= loc[0] && -eps <= loc[1] && -eps <= loc[2]
                    && (loc[0]+loc[2]) <= 1+eps
                    && (loc[1]+loc[2]) <= 1+eps;
            } else if (type.isPrism()) {
                return -eps <= loc[0] && -eps <= loc[1]
                    && (loc[0]+loc[1])<= 1+eps
                    && -eps <= loc[2] && loc[2] <= 1+eps;
            } else if (type.isCube()) {
                return -eps <= loc[0] && loc[0] <= 1+eps
                    && -eps <= loc[1] && loc[1] <= 1+eps
                    && -eps <= loc[2] && loc[2] <= 1+eps;
            }else {
                DUNE_THROW(Dune::GridError, "checkInside():  ERROR:  Unknown type " << type << " found!");
            }
        default:
            DUNE_THROW(Dune::GridError, "checkInside():  ERROR:  Unknown type " << type << " found!");
        }

    }

public:

    template <int blocksize>
    static void interpolate(const GridType& sourceGrid,
                            const Dune::BlockVector<Dune::FieldVector<double,blocksize> >& source,
                            const GridType& targetGrid,
                            Dune::BlockVector<Dune::FieldVector<double,blocksize> >& target)
    {
        using namespace Dune;

        // Create a leaf p1 function, which we need to be able to call 'evalall()'
        P1NodalBasis<typename GridType::LeafGridView> p1NodalBasis(sourceGrid.leafGridView());
        BasisGridFunction<P1NodalBasis<typename GridType::LeafGridView>, Dune::BlockVector<Dune::FieldVector<double,blocksize> > > sourceFunction(p1NodalBasis, source);

        // ///////////////////////////////////////////////////////////////////////////////////////////
        //   Prolong the adaptive solution onto the uniform grid in order to make it comparable
        // ///////////////////////////////////////////////////////////////////////////////////////////

        target.resize(targetGrid.size(dim));

        // handle each vertex only once
        Dune::BitSetVector<1> handled(targetGrid.size(dim), false);

        typename GridType::LeafGridView targetLeafView = targetGrid.leafGridView();
        const auto& indexSet = targetLeafView.indexSet();

        // Loop over all vertices by looping over all elements
        for (const auto& e : elements(targetLeafView)) {

            for (unsigned int i=0; i<e.subEntities(dim); i++) {

                typename GridType::template Codim<0>::Entity element(e);
                FieldVector<double,dim> pos = ReferenceElements<double, dim>::general(e.type()).position(i,dim);

                if (handled[indexSet.subIndex(e,i,dim)][0])
                    continue;

                handled[indexSet.subIndex(e,i,dim)] = true;

                assert(checkInside(element.type(), pos, 1e-7));

                // ////////////////////////////////////////////////////////////////////
                // Get an element on the coarsest grid which contains the vertex and
                // its local coordinates there
                // ////////////////////////////////////////////////////////////////////

                while (element.level() != 0){

                    pos = element.geometryInFather().global(pos);
                    element = element.father();

                    assert(checkInside(element.type(), pos, 1e-7));
                }

                // ////////////////////////////////////////////////////////////////////
                //   Find the corresponding coarse grid element on the adaptive grid.
                //   This is a linear algorithm, but we expect the coarse grid to be small.
                // ////////////////////////////////////////////////////////////////////
                LevelMultipleCodimMultipleGeomTypeMapper<GridType> uniformP0Mapper (targetGrid, 0, Dune::mcmgElementLayout());
                LevelMultipleCodimMultipleGeomTypeMapper<GridType> adaptiveP0Mapper(sourceGrid, 0, Dune::mcmgElementLayout());
                const auto coarseIndex = uniformP0Mapper.index(element);

                typename GridType::LevelGridView sourceLevelView = sourceGrid.levelGridView(0);

                for (auto&& adaptE : elements(sourceLevelView))
                    if (adaptiveP0Mapper.index(adaptE) == coarseIndex) {
                        element = std::move(adaptE);
                        break;
                    }

                // ////////////////////////////////////////////////////////////////////////
                //   Find a corresponding point (not necessarily vertex) on the leaf level
                //   of the adaptive grid.
                // ////////////////////////////////////////////////////////////////////////

                while (!element.isLeaf()) {


                    typename GridType::template Codim<0>::Entity::HierarchicIterator hIt    = element.hbegin(element.level()+1);
                    typename GridType::template Codim<0>::Entity::HierarchicIterator hEndIt = element.hend(element.level()+1);

                    FieldVector<double,dim> childPos;
                    assert(checkInside(element.type(), pos, 1e-7));

                    for (; hIt!=hEndIt; ++hIt) {

                        childPos = hIt->geometryInFather().local(pos);
                        if (checkInside(hIt->type(), childPos, 1e-7))
                            break;

                    }

                    assert(hIt!=hEndIt);
                    element = *hIt;
                    pos     = childPos;

                }

                // ////////////////////////////////////////////////////////////////////////
                //   Sample adaptive function
                // ////////////////////////////////////////////////////////////////////////
                sourceFunction.evaluateLocal(element, pos,
                                            target[indexSet.subIndex(e,i,dim)]);

            }

        }

    }


    template <int blocksize>
    static double computeNormSquared(const GridType& grid,
                              const Dune::BlockVector<Dune::FieldVector<double,blocksize> >& x,
                              const LocalOperatorAssembler<GridType,
                              typename P1NodalBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                              typename P1NodalBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                              Dune::FieldMatrix<double,blocksize,blocksize> >* localStiffness)
    {
        // ///////////////////////////////////////////////////////////////////////////////////////////
        //   Compute the energy norm
        // ///////////////////////////////////////////////////////////////////////////////////////////

        const typename GridType::Traits::LeafIndexSet& leafIndexSet = grid.leafIndexSet();
        double energyNormSquared = 0;
        P1NodalBasis<typename GridType::LeafGridView> p1NodalBasis(grid.leafGridView());
        Dune::Matrix<Dune::FieldMatrix<double,blocksize,blocksize> > localMatrix;

        typename GridType::LeafGridView leafView = grid.leafGridView();

        for (const auto& e : elements(leafView)) {

            localMatrix.setSize(p1NodalBasis.getLocalFiniteElement(e).localCoefficients().size(),
                                p1NodalBasis.getLocalFiniteElement(e).localCoefficients().size());

            localStiffness->assemble(e, localMatrix,
                                     p1NodalBasis.getLocalFiniteElement(e),
                                     p1NodalBasis.getLocalFiniteElement(e));

            for (unsigned int i=0; i<e.subEntities(dim); i++)
                for (unsigned int j=0; j<e.subEntities(dim); j++) {
                    Dune::FieldVector<double,blocksize> smallTmp(0);
                    localMatrix[i][j].umv(x[leafIndexSet.subIndex(e,j,dim)],smallTmp);
                    energyNormSquared += smallTmp * x[leafIndexSet.subIndex(e,i,dim)];
                }

        }

        return energyNormSquared;
    }

    template <int blocksize>
    static double compute(const GridType& uniformGrid,
                          const Dune::BlockVector<Dune::FieldVector<double,blocksize> >& uniformSolution,
                          const GridType& adaptiveGrid,
                          const Dune::BlockVector<Dune::FieldVector<double,blocksize> >& adaptiveSolution,
                          const LocalOperatorAssembler<GridType,
                          typename P1NodalBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                          typename P1NodalBasis<typename GridType::LeafGridView>::LocalFiniteElement,
                          Dune::FieldMatrix<double,blocksize,blocksize> >* localStiffness)
    {
        Dune::BlockVector<Dune::FieldVector<double,blocksize> > uniformAdaptiveSolution;

        interpolate(adaptiveGrid, adaptiveSolution, uniformGrid, uniformAdaptiveSolution);

        uniformAdaptiveSolution -= uniformSolution;

        return computeNormSquared(uniformGrid, uniformAdaptiveSolution, localStiffness);
    }
};

#endif
