// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef MAKE_RING_HH
#define MAKE_RING_HH

#include <memory>
#include <vector>

#include <dune/common/shared_ptr.hh>

#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/common/boundarysegment.hh>

#include <dune/fufem/arcofcircle.hh>
#include <dune/fufem/boundarypatch.hh>

/** \brief Create a 2D ring segment grid.
 *
 *  \param center - The center of the ring.
 *  \param thickness - Thickness of the ring segment.
 *  \param innerRadius - Radius of the inner circle.
 *  \param fromAngle - Angle where the segment starts.
 *  \param toAngle - Angle where the segment stops.
 *  \param grid - Store the grid in here.
 *  \param nElement - The number of initial elements. Avoid meshing problems by making a good choice.
 *  \param closeRing - Close the ring, needs fromAngle = toAngle
 *  \param quadrilateral - Make ring segment consist of quadrilaterals
 *  \param innerBoundary - If given, make a level 0 boundary patch containing the inner nodes
 *  \param outerBoundary - If given, make a level 0 boundary patch containing the outer nodes
 *
 */
template<class GridType>
void makeRingSegment2D(const Dune::FieldVector<typename GridType::ctype,2>& center,
                       const typename GridType::ctype thickness, const typename GridType::ctype innerRadius,
                       const typename GridType::ctype fromAngle, const typename GridType::ctype toAngle,
                       GridType& grid, size_t nElement, bool closeRing, bool quadrilateral = false,
                       BoundaryPatch<typename GridType::LevelGridView>* innerBoundary = NULL,
                       BoundaryPatch<typename GridType::LevelGridView>* outerBoundary = NULL)
{
    static_assert(GridType::dimension==2, "makeRingSegment2D can only be called for 2-dimensional grids");
    typedef typename GridType::ctype ctype;

    ctype eps = 1e-10;
    if ( (-eps >= fromAngle) || (fromAngle > toAngle) || (toAngle > 2*M_PI) )
        DUNE_THROW(Dune::Exception, "Parameters do not fulfill 0 <= fromAngle <= toAngle <= 2*M_PI");

    bool fullRing = closeRing && (fromAngle==0.0) && (toAngle == 2*M_PI);

    using namespace Dune;

    Dune::GridFactory<GridType> factory(&grid);

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////

    ArcOfCircle innerArc(center, innerRadius, fromAngle, toAngle);

    ctype step =1.0/((ctype) nElement);

    // insert inner ring vertices
    for (size_t i=0; i<nElement; i++)
        factory.insertVertex(innerArc(i*step));

    // if the ring is not closed also make vertices for the right boundary
    if (!fullRing)
        factory.insertVertex(innerArc(1.0));

    ArcOfCircle outerArc(center, innerRadius+thickness, fromAngle, toAngle);

    // insert outer ring vertices
    for (size_t i=0; i<nElement; i++)
        factory.insertVertex(outerArc(i*step));

    // if the ring is not closed also make vertices for the right boundary
    if (!fullRing)
        factory.insertVertex(outerArc(1.0));

    // /////////////////
    // Insert elements
    // /////////////////

    std::vector<unsigned int> cornerIDs((quadrilateral) ?4 : 3);
    GeometryType gt = (quadrilateral) ? GeometryTypes::quadrilateral : GeometryTypes::triangle;

    // if ring is closed last nodes are the first nodes
    if (!fullRing) {

        if (quadrilateral) {

            for (size_t i=0; i<nElement; i++) {
                cornerIDs[0] = i+1;
                cornerIDs[1] = i;
                cornerIDs[2] = i+nElement+2;
                cornerIDs[3] = i+nElement+1;
                factory.insertElement(gt, cornerIDs);
            }
        } else {

            for (size_t i=0; i<nElement; i++) {
                // first triangle
                cornerIDs[0] = i+1;
                cornerIDs[1] = i;
                cornerIDs[2] = i+nElement+2;
                factory.insertElement(gt, cornerIDs);

                // second
                cornerIDs[0] = nElement +i +1;
                cornerIDs[1] = nElement +i +2;
                cornerIDs[2] = i;
                factory.insertElement(gt, cornerIDs);
            }
        }
    } else {


        if (quadrilateral) {

            for (size_t i=0; i<nElement-1; i++) {
                cornerIDs[0] = i+1;
                cornerIDs[1] = i;
                cornerIDs[2] = i+nElement+1;
                cornerIDs[3] = i+nElement;
                factory.insertElement(gt, cornerIDs);
            }
            cornerIDs[0] = 0;
            cornerIDs[1] = nElement-1;
            cornerIDs[2] = nElement;
            cornerIDs[3] = 2*nElement -1;
            factory.insertElement(gt, cornerIDs);

        } else {

            for (size_t i=0; i<nElement-1; i++) {
                // first triangle
                cornerIDs[0] = i+1;
                cornerIDs[1] = i;
                cornerIDs[2] = i+nElement+1;
                factory.insertElement(gt, cornerIDs);

                // second
                cornerIDs[0] = nElement +i;
                cornerIDs[1] = nElement +i +1;
                cornerIDs[2] = i;
                factory.insertElement(gt, cornerIDs);
            }
            // first triangle
            cornerIDs[0] = 0;
            cornerIDs[1] = nElement-1;
            cornerIDs[2] = nElement;
            factory.insertElement(gt, cornerIDs);

            // second triangle
            cornerIDs[0] = 2*nElement-1;
            cornerIDs[1] = nElement;
            cornerIDs[2] = nElement -1;
            factory.insertElement(gt, cornerIDs);

        }
    }

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////
    std::vector<unsigned int> vertices(2);

    typedef typename std::shared_ptr<Dune::BoundarySegment<2> > SegmentPointer;
    ctype angleDiff = toAngle-fromAngle;

    for (size_t i=0; i<nElement-1; i++) {

        vertices[0] = i;  vertices[1] = i+1;
        factory.insertBoundarySegment(vertices,
                SegmentPointer(new ArcOfCircle(center, innerRadius, fromAngle+i*step*angleDiff,
                                                                    fromAngle+(i+1)*step*angleDiff)));

        vertices[0] = nElement+i+(!fullRing);  vertices[1] = nElement +i+1+(!fullRing);
        factory.insertBoundarySegment(vertices,
                SegmentPointer(new ArcOfCircle(center, innerRadius+thickness, fromAngle+i*step*angleDiff,
                                                                            fromAngle+(i+1)*step*angleDiff)));

    }

    // if ring is closed last nodes are the first nodes

    vertices[0] = nElement-1;  vertices[1] = (!fullRing)*nElement;
    factory.insertBoundarySegment(vertices,
            SegmentPointer(new ArcOfCircle(center, innerRadius, fromAngle+(nElement-1)*step*angleDiff,
                    toAngle)));

    vertices[0] = 2*nElement-1+(!fullRing);  vertices[1] =nElement + (!fullRing)*(nElement+1);
    factory.insertBoundarySegment(vertices,
            SegmentPointer(new ArcOfCircle(center, innerRadius+thickness, fromAngle+(nElement-1)*step*angleDiff,
                    toAngle)));

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////

    factory.createGrid();


    // create boundarypatches if desired
    if (innerBoundary) {

        BitSetVector<1> innerVert(2*(nElement+!(fullRing)));
        size_t half = innerVert.size()/2;

        for (size_t i=0; i<half; i++)
            innerVert[i] = true;

        innerBoundary->setup(grid.levelGridView(0),innerVert);
    }

    if (outerBoundary) {

        BitSetVector<1> outerVert(2*(nElement+!(fullRing)));
        size_t half = outerVert.size()/2;

        for (size_t i=0; i<half; i++)
            outerVert[half+i] = true;

        outerBoundary->setup(grid.levelGridView(0),outerVert);
    }
}


/** \todo Make tube orientation arbitrary. */
/** \brief Create a 3D tube segment prism grid along a prescribed Euclidean axis.
 *
 *  If the tube is very thin the boundary parametrization can require the improvement of the inner grid vertices.
 *  This is for example implemented in the dune-biomech/prismlayer.hh class.
 *
 *
 *  \param center - The center of the tube.
 *  \param thickness - Thickness of the ring segment.
 *  \param length - Length of the ring segment.
 *  \param innerRadius - Radius of the inner circle.
 *  \param fromAngle - Angle where the segment starts.
 *  \param toAngle - Angle where the segment stops.
 *  \param grid - Store the grid in here.
 *  \param nElementRing - The number of initial elements in the ring direction.
 *  \param nElementLength - The number of initial elements in the length direction.
 *  \param closeTube - Close the tube, needs fromAngle == 0, toAngle == 2*PI
 *  \param axis - The axis along the tube should be build 0 <= axis <= 2
 *  \param innerBoundary - If given, make a level 0 boundary patch containing the inner nodes
 *  \param outerBoundary - If given, make a level 0 boundary patch containing the outer nodes
 *
 */
template <class GridType>
std::unique_ptr<GridType> makeTubeSegment3D(
    const Dune::FieldVector<typename GridType::ctype, 3>& center,
    const typename GridType::ctype thickness,
    const typename GridType::ctype length,
    const typename GridType::ctype innerRadius,
    const typename GridType::ctype fromAngle,
    const typename GridType::ctype toAngle, unsigned int nElementRing,
    unsigned int nElementLength, bool closeTube, unsigned int axis = 0,
    bool tetrahedra = true, bool parametrizedBoundary = true,
    BoundaryPatch<typename GridType::LevelGridView> * innerBoundary = NULL,
    BoundaryPatch<typename GridType::LevelGridView> * outerBoundary = NULL)
{
    static_assert(GridType::dimension==3, "makeTubeSegment3D can only be called for 3-dimensional grids");
    typedef typename GridType::ctype ctype;

    if (fromAngle >= toAngle)
        DUNE_THROW(Dune::Exception, "Parameters do not fulfill fromAngle < toAngle");

    bool fullTube = closeTube && (fromAngle==0.0) && (toAngle == 2*M_PI);

    using namespace Dune;

    Dune::GridFactory<GridType> factory;

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////

    unsigned int nRingNodes = nElementRing+!fullTube;

    // project center on the yz-plane
    FieldVector<ctype,2> localCenter;
    localCenter[0] = center[(axis+1)%3];
    localCenter[1] = center[(axis+2)%3];

    ArcOfCircle innerArc(localCenter, innerRadius, fromAngle, toAngle);

    ctype stepRing =1.0/((ctype) nElementRing);
    ctype stepLength = length/((ctype) nElementLength);

    // insert tube vertices
    for (size_t j=0; j<=nElementLength; j++) {

        FieldVector<ctype,3> vertex(0);

        // x-coordinate is determined by the center x-coord
        vertex[axis] = center[axis] + j*stepLength;

        for (size_t i=0; i<nRingNodes; i++) {

            FieldVector<ctype,2> local = innerArc(i*stepRing);
            vertex[(axis+1)%3] = local[0]; vertex[(axis+2)%3] = local[1];

            factory.insertVertex(vertex);
        }
    }

    ArcOfCircle outerArc(localCenter, innerRadius+thickness, fromAngle, toAngle);

    // insert outer tube vertices
    for (unsigned int j=0; j<=nElementLength; j++) {

        FieldVector<ctype,3> vertex(0);

        // x-coordinate is determined by the center x-coord
        vertex[axis] = center[axis] + j*stepLength;

        for (size_t i=0; i<nRingNodes; i++) {

            FieldVector<ctype,2> local = outerArc(i*stepRing);
            vertex[(axis+1)%3] = local[0]; vertex[(axis+2)%3] = local[1];

            factory.insertVertex(vertex);
        }
    }


    unsigned int innerNodes = (nElementLength+1)*(nRingNodes);

    // /////////////////
    // Insert elements
    // /////////////////

    // if ring is closed last nodes are the first nodes

    std::vector<unsigned int> cornerIDs(4+!tetrahedra*2);

    Dune::GeometryType gt = Dune::GeometryTypes::tetrahedron;
    if (!tetrahedra)
        gt = Dune::GeometryTypes::prism;

    for (unsigned int i=0; i<nElementLength; i++) {

        unsigned int row = i*nRingNodes;

        for (unsigned int j=0; j<nRingNodes-1; j++) {

            if (tetrahedra) {

                std::vector<unsigned int> hexa = {row+j,row+j+1,
                                                  row+j+1+nElementRing,row+j+2+nElementRing,
                                                  row+j+innerNodes,row+j+1+innerNodes,
                                                  row+j+1+nElementRing+innerNodes,
                                                  row+j+2+nElementRing+innerNodes};
                //add 6 tetrahedra
                cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
                factory.insertElement(gt,cornerIDs);

            } else {
                // Insert a prism
                cornerIDs[0] = row+j;
                cornerIDs[1] = row+j+1 ;
                cornerIDs[2] = row+nElementRing+j+2;
                cornerIDs[3] = cornerIDs[0]+innerNodes;
                cornerIDs[4] = cornerIDs[1]+innerNodes;
                cornerIDs[5] = cornerIDs[2]+innerNodes;

                factory.insertElement(gt, cornerIDs);

                // Insert another prism
                cornerIDs[0] = row+nElementRing+j+2;
                cornerIDs[1] = row+nElementRing+j+1;
                cornerIDs[2] = row+j;
                cornerIDs[3] = cornerIDs[0]+innerNodes;
                cornerIDs[4] = cornerIDs[1]+innerNodes;
                cornerIDs[5] = cornerIDs[2]+innerNodes;

                factory.insertElement(gt, cornerIDs);
            }
        }

        if (fullTube) {
            if (tetrahedra) {

                std::vector<unsigned int> hexa = {row+nElementRing-1,row,
                                                  row+2*nElementRing-1,row+nElementRing,
                                                  row+nElementRing-1+innerNodes,
                                                  row+innerNodes,
                                                  row+2*nElementRing-1+innerNodes,
                                                  row+nElementRing+innerNodes};
                 //add 6 tetrahedra
                cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
                factory.insertElement(gt,cornerIDs);

            } else {
                // Insert a prism
                cornerIDs[0] = row+nElementRing-1;
                cornerIDs[1] = row;
                cornerIDs[2] = row+nElementRing;
                cornerIDs[3] = cornerIDs[0]+innerNodes;
                cornerIDs[4] = cornerIDs[1]+innerNodes;
                cornerIDs[5] = cornerIDs[2]+innerNodes;

                factory.insertElement(gt, cornerIDs);

                // Insert another prism
                cornerIDs[0] = row+nElementRing;
                cornerIDs[1] = row+2*nElementRing-1;
                cornerIDs[2] = row+nElementRing-1;
                cornerIDs[3] = cornerIDs[0]+innerNodes;
                cornerIDs[4] = cornerIDs[1]+innerNodes;
                cornerIDs[5] = cornerIDs[2]+innerNodes;

                factory.insertElement(gt, cornerIDs);
            }
        }
    }

    // /////////////////////////////
    //   Create boundary segments
    // /////////////////////////////

    if (parametrizedBoundary) {
        std::vector<unsigned int> vertices(3);

        typedef typename std::shared_ptr<Dune::BoundarySegment<3> > SegmentPointer;
        ctype angleDiff = toAngle-fromAngle;

        for (size_t i=0; i<nElementLength; i++) {

            unsigned int row = i*nRingNodes;

            FieldVector<ctype,3> localCenter = center;
            localCenter[0] += i*stepLength;

            // lower triangular faces
            for (size_t j=0; j<nElementRing-1; j++) {

                vertices[0] = row+j;  vertices[1] = row+j+1; vertices[2] = row+nRingNodes+j+1;
                factory.insertBoundarySegment(vertices,
                                              SegmentPointer(new TubeLowerSegment<ctype>(localCenter, innerRadius, stepLength,
                                                                                         fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));


                vertices[0] += innerNodes;  vertices[1] += innerNodes; vertices[2] +=innerNodes;
                factory.insertBoundarySegment(vertices,
                                              SegmentPointer(new TubeLowerSegment<ctype>(localCenter, innerRadius+thickness, stepLength,
                                                                                         fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));
            }

            // if ring is closed last nodes are the first nodes
            vertices[0] = row+nElementRing-1;
            vertices[1] = row+(!fullTube)*nElementRing;
            vertices[2] = row+(!fullTube)*(nElementRing+1) + nElementRing;

            factory.insertBoundarySegment(vertices,
                                          SegmentPointer(new TubeLowerSegment<ctype>(localCenter, innerRadius, stepLength,
                                                                                     fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));

            vertices[0] += innerNodes;
            vertices[1] += innerNodes;
            vertices[2] += innerNodes;

            factory.insertBoundarySegment(vertices,
                                          SegmentPointer(new TubeLowerSegment<ctype>(localCenter, innerRadius+thickness, stepLength,
                                                                                     fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));


            // upper triangular faces
            localCenter[0] += stepLength;

            for (size_t j=0; j<nElementRing-1; j++) {

                vertices[0] = row+nRingNodes+j;  vertices[1] = row+nRingNodes+j+1; vertices[2] = row+j;
                factory.insertBoundarySegment(vertices,
                                              SegmentPointer(new TubeUpperSegment<ctype>(localCenter, innerRadius, -stepLength,
                                                                                         fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));

                vertices[0] += innerNodes;  vertices[1] += innerNodes; vertices[2] += innerNodes;
                factory.insertBoundarySegment(vertices,
                                              SegmentPointer(new TubeUpperSegment<ctype>(localCenter, innerRadius+thickness, -stepLength,
                                                                                         fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));
            }

            // if ring is closed last nodes are the first nodes
            vertices[0] = row+nRingNodes+nElementRing-1;
            vertices[1] = row+(!fullTube)*nElementRing + nRingNodes;
            vertices[2] = row + nElementRing -1;

            factory.insertBoundarySegment(vertices,
                                          SegmentPointer(new TubeUpperSegment<ctype>(localCenter, innerRadius, -stepLength,
                                                                                     fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));

            vertices[0] += innerNodes;
            vertices[1] += innerNodes;
            vertices[2] += innerNodes;

            factory.insertBoundarySegment(vertices,
                                          SegmentPointer(new TubeUpperSegment<ctype>(localCenter, innerRadius+thickness, -stepLength,
                                                                                     fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));

        }

        // add lateral segments at the front and the back
        std::vector<unsigned int> vert(4);

        // offset to last back nodes
        int lastRow = nElementLength*nRingNodes;
        FieldVector<ctype,3> lastCenter = center;
        lastCenter[0] += length;

        for (size_t j=0; j<nElementRing-1; j++) {

            // front side
            vert[0] = j; vert[1] = j+1; vert[2] = j + innerNodes; vert[3] = j+innerNodes +1;

            factory.insertBoundarySegment(vert,
                                          SegmentPointer(new TubeLateralSegment<ctype>(center, innerRadius, thickness,
                                                                                       fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));

            // back side
            vert[0] += lastRow; vert[1] += lastRow; vert[2] += lastRow; vert[3] += lastRow;

            factory.insertBoundarySegment(vert,
                                          SegmentPointer(new TubeLateralSegment<ctype>(lastCenter, innerRadius, thickness,
                                                                                       fromAngle+j*stepRing*angleDiff, fromAngle+(j+1)*stepRing*angleDiff)));


        }

        //last segments differ depending on if the tube is closed

        // front side
        vert[0] = nElementRing-1; vert[1] = (!fullTube)*nElementRing;
        vert[2] = vert[0] + innerNodes; vert[3] = vert[1] + innerNodes;

        factory.insertBoundarySegment(vert,
                                      SegmentPointer(new TubeLateralSegment<ctype>(center, innerRadius, thickness,
                                                                                   fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));

        // back side
        vert[0] += lastRow; vert[1] += lastRow; vert[2] += lastRow; vert[3] += lastRow;

        factory.insertBoundarySegment(vert,
                                      SegmentPointer(new TubeLateralSegment<ctype>(lastCenter, innerRadius, thickness,
                                                                                   fromAngle+(nElementRing-1)*stepRing*angleDiff, toAngle)));

    }
    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    std::unique_ptr<GridType> grid = std::unique_ptr<GridType>(factory.createGrid());

    // create boundarypatches if desired
    if (innerBoundary) {

        BitSetVector<1> innerVert(2*(nElementLength+1)*nRingNodes);
        size_t half = innerVert.size()/2;

        for (size_t i=0; i<half; i++)
            innerVert[i] = true;

        innerBoundary->setup(grid->levelGridView(0),innerVert);
    }

    if (outerBoundary) {

        BitSetVector<1> outerVert(2*(nElementLength+1)*nRingNodes);
        size_t half = outerVert.size()/2;

        for (size_t i=0; i<half; i++)
            outerVert[half+i] = true;

        outerBoundary->setup(grid->levelGridView(0),outerVert);
    }

    return grid;
}

/** \todo Make torus orientation arbitrary. */
/** \todo Allow arbitrary fromAngle <= toAngle. */
/** \brief Create a 3D tube segment prism grid along the x-axis.
 *
 *  If the tube is very thin the boundary parametrization can require the improvement of the inner grid vertices.
 *  This is for example implemented in the dune-biomech/prismlayer.hh class.
 *
 *
 *  \param center - The center of the tube.
 *  \param plane The plane in which the torus lives, the first two give the horizontal plane,
 *         the first and third the vertical plane
 *  \param thickness - Thickness of the ring segment.
 *  \param horizontalRadius - Radius of the horizontal circle.
 *  \param verticalRadius - Radius of the vertical circle.
 *  \param nElementHorizontal - The number of initial elements in the horizontal ring direction.
 *  \param nElementVertical - The number of initial elements in the vertical ring direction.
 *  \param innerBoundary - If given, make a level 0 boundary patch containing the inner nodes
 *  \param outerBoundary - If given, make a level 0 boundary patch containing the outer nodes
 *
 */
template <class GridType>
std::unique_ptr<GridType> makeTorus(
    const Dune::FieldVector<typename GridType::ctype, 3>& center,
    const std::array<Dune::FieldVector<typename GridType::ctype, 3>, 3> plane,
    const typename GridType::ctype thickness,
    const typename GridType::ctype horizontalRadius,
    const typename GridType::ctype verticalRadius,
    unsigned int nElementHorizontal, unsigned int nElementVertical,
    BoundaryPatch<typename GridType::LevelGridView>* innerBoundary = NULL,
    BoundaryPatch<typename GridType::LevelGridView> * outerBoundary = NULL)
{
  static_assert(GridType::dimension == 3,
                "makeTorus can only be called for 3-dimensional grids");
    using ctype = typename GridType::ctype;
    using FV = Dune::FieldVector<ctype,3>;

    Dune::GridFactory<GridType> factory;

    // ///////////////////////
    //   Insert vertices
    // ///////////////////////

    // make circle around (0,0)
    ArcOfCircle hArc(Dune::FieldVector<ctype,2>(0), horizontalRadius+verticalRadius+thickness, 0, 2*M_PI);
    ArcOfCircle vInnerArc(Dune::FieldVector<ctype,2>(0), verticalRadius, 0, 2*M_PI);
    ArcOfCircle vOuterArc(Dune::FieldVector<ctype,2>(0), verticalRadius+thickness, 0, 2*M_PI);

    ctype stepH =1.0/nElementHorizontal;
    ctype stepV = 1.0/nElementVertical;

    // insert torus vertices
    for (size_t j=0; j<nElementHorizontal; j++) {

        // compute center of the vertical rings
        FV ringCenter = center;
        auto corr = hArc(j*stepH);
        for (int i=0; i<2; i++)
            ringCenter.axpy(corr[i],plane[i]);

        //auto vPlane = ringCenter - plane[0];
        //vPlane /= vPlane.two_norm();

        auto vPlane = ringCenter - center;
        vPlane /= vPlane.two_norm();

        // compute coordinates on the inner torus
        for (size_t i=0; i<nElementVertical; i++) {

            auto local = vInnerArc(i*stepV);
            auto coord = ringCenter;
            coord.axpy(local[0],vPlane);
            coord.axpy(local[1],plane[2]);

            factory.insertVertex(coord);
        }

        // compute coordinates on the outer torus
        for (size_t i=0; i<nElementVertical; i++) {

            auto local = vOuterArc(i*stepV);
            auto coord = ringCenter;
            coord.axpy(local[0],vPlane);
            coord.axpy(local[1],plane[2]);

            factory.insertVertex(coord);
        }
    }

    unsigned int innerNodes = nElementHorizontal*nElementVertical;

    // /////////////////
    // Insert elements
    // /////////////////

    // if ring is closed last nodes are the first nodes

    std::vector<unsigned int> cornerIDs(4);
    Dune::GeometryType gt;gt.makeTetrahedron();

    for (unsigned int i=0; i<nElementHorizontal-1; i++) {

        unsigned int row = i*2*nElementVertical;

        for (unsigned int j=0; j<nElementVertical-1; j++) {


                std::vector<unsigned int> hexa = {row+j,row+j+1,
                                                  row+j+2*nElementVertical,row+j+1+2*nElementVertical,
                                                  row+j+nElementVertical,row+j+1+nElementVertical,
                                                  row+j+3*nElementVertical,
                                                  row+j+1+3*nElementVertical};

                //add 6 tetrahedra
                cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
                factory.insertElement(gt,cornerIDs);
                cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
                factory.insertElement(gt,cornerIDs);

        }

        // last element in each vertical circle couples with the first nodes
        std::vector<unsigned int> hexa = {row+nElementVertical-1,row,
                                          row+3*nElementVertical-1,row+2*nElementVertical,
                                          row+2*nElementVertical-1,
                                          row+nElementVertical,
                                          row+4*nElementVertical-1,
                                          row+3*nElementVertical};
        //add 6 tetrahedra
        cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
        factory.insertElement(gt,cornerIDs);
    }

    // the last row couples with the first
    unsigned int row = (nElementHorizontal-1)*2*nElementVertical;

    for (unsigned int j=0; j<nElementVertical-1; j++) {


        std::vector<unsigned int> hexa = {row+j,row+j+1, j,j+1,
                                          row+j+nElementVertical,row+j+1+nElementVertical,
                                          j+nElementVertical,
                                          j+1+nElementVertical};

        //add 6 tetrahedra
        cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
        factory.insertElement(gt,cornerIDs);

    }

    // last element in each vertical circle couples with the first nodes
    std::vector<unsigned int> hexa = {row+nElementVertical-1,row,
                                      nElementVertical-1,0,
                                      row+2*nElementVertical-1,
                                      row+nElementVertical,
                                      2*nElementVertical-1,
                                      nElementVertical};
        //add 6 tetrahedra
        cornerIDs={hexa[0],hexa[1],hexa[3],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[5],hexa[1],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[4],hexa[5],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[6],hexa[4],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[2],hexa[6],hexa[7]};
        factory.insertElement(gt,cornerIDs);
        cornerIDs={hexa[0],hexa[3],hexa[2],hexa[7]};
        factory.insertElement(gt,cornerIDs);

    // //////////////////////////////////////
    //   Finish initialization
    // //////////////////////////////////////
    std::unique_ptr<GridType> grid = std::unique_ptr<GridType>(factory.createGrid());

    // create boundarypatches if desired
    if (innerBoundary) {

        Dune::BitSetVector<1> innerVert(2*innerNodes,false);

        for (unsigned i=0; i<nElementHorizontal; i++) {
            auto  row = i*2*nElementVertical;
            for (unsigned j=0; j<nElementVertical; j++)
                innerVert[row+j] = true;
        }

        innerBoundary->setup(grid->levelGridView(0),innerVert);
    }

    if (outerBoundary) {

        Dune::BitSetVector<1> outerVert(2*innerNodes,false);

        for (unsigned i=0; i<nElementHorizontal; i++) {
            auto  row = i*2*nElementVertical;
            for (unsigned j=nElementVertical; j<2*nElementVertical; j++)
                outerVert[row+j] = true;
        }

        outerBoundary->setup(grid->levelGridView(0),outerVert);
    }

    return grid;
}

#endif
