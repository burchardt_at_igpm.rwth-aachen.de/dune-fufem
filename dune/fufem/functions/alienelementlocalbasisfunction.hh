#ifndef ALIEN_ELEMENT_LOCALBASIS_FUNCTION_HH
#define ALIEN_ELEMENT_LOCALBASIS_FUNCTION_HH

/**
   @file alienelementlocalbasisfunction.hh
   @brief Evaluate local basis functions on different element

   @author graeser@math.fu-berlin.de
 */

#include <vector>
#include <map>

#include <dune/common/function.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/fufem/functions/cachedcomponentwrapper.hh>


/** \brief Evaluate local basis functions on different element
 *
 * This class allows to evaluate single local basis functions on elements
 * by using local coordinates of different element. This is necessary
 * to allow evaluation of local basis functions within LocalInterpolation
 * objects.
 *
 * \tparam Entity The used entity type
 * \tparam LocalBasis The local basis type
 */
template <class Entity, class FE, class Base
    =typename Dune::Function<
        const typename FE::Traits::LocalBasisType::Traits::DomainType&,
        typename FE::Traits::LocalBasisType::Traits::RangeType&> >
class AlienElementLocalBasisFunction :
    public CachedComponentWrapper<AlienElementLocalBasisFunction<Entity, FE, Base>,
    typename std::vector<typename FE::Traits::LocalBasisType::Traits::RangeType>,
    Base>
{
    public:
        typedef typename std::vector<typename FE::Traits::LocalBasisType::Traits::RangeType> AllRangeType;
        typedef typename FE::Traits::LocalBasisType LocalBasis;
        typedef typename FE::Traits::LocalBasisType::Traits::DomainType DomainType;
        typedef typename FE::Traits::LocalBasisType::Traits::RangeType RangeType;

    private:
        typedef AlienElementLocalBasisFunction<Entity, FE, Base> MyType;
        typedef CachedComponentWrapper<MyType, AllRangeType, Base> BaseClass;

    public:

        /** \brief Construct function from entities and local basis.
         *
         * \param entity Entity where the function should be evaluated. Use local coordinates w.r.t. this entity.
         * \param alienEntity Entity the local basis lives on.
         * \param localBasis Local basis on alienEntity
         */
        AlienElementLocalBasisFunction(const Entity& entity, const Entity& alienEntity, const LocalBasis& localBasis, const int i=0) :
            BaseClass(i),
            entity_(entity),
            alienEntity_(alienEntity),
            localBasis_(localBasis)
        {}


        /** \brief Evaluate all local basis functions
         *
         * \param x Local coordinates with respect to entity given in constructor.
         * \param yy The result vector containing all values is stored here.
         */
        void evaluateAll(const DomainType& x, std::vector<RangeType>& yy) const
        {
            const auto& alienRefElement = Dune::ReferenceElements<double, Entity::dimension>::general(alienEntity_.type());

            const DomainType globalPosition = entity_.geometry().global(x);
            const DomainType alienLocalPosition = alienEntity_.geometry().local(globalPosition);

            if (alienRefElement.checkInside(alienLocalPosition))
                localBasis_.evaluateFunction(alienLocalPosition, yy);
            else
            {
                yy.resize(localBasis_.size());
                for(size_t j=0; j<localBasis_.size(); ++j)
                    yy[j] = 0;
            }
        }

    private:
        const Entity& entity_;
        const Entity& alienEntity_;
        const LocalBasis& localBasis_;
};

#endif

