#ifndef CACHED_COMPONENT_WRAPPER_HH
#define CACHED_COMPONENT_WRAPPER_HH

/**
   @file
   @brief Cache evaluations of a single component of a function

   @author graeser@math.fu-berlin.de
 */

#include <vector>
#include <map>

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>



template<class RT>
struct Components
{
    template<class RFT>
    static void setComponent(RT& x, int i, const RFT& xi)
    {
        x = xi;
    }

    template<class RFT>
    static void getComponent(const RT& x, int i, RFT& xi)
    {
        xi = x;
    }
    static int size(const RT& x)
    {
        return 1;
    }
};

template<class T, int n>
struct Components<typename Dune::FieldVector<T, n> >
{
    typedef typename Dune::FieldVector<T, n> RT;

    template<class RFT>
    static void setComponent(RT& x, int i, const RFT& xi)
    {
        x[i] = xi;
    }

    template<class RFT>
    static void getComponent(const RT& x, int i, RFT& xi)
    {
        xi = x[i];
    }

    static int size(const RT& x)
    {
        return n;
    }
};

template<class T, int n, int m>
struct Components<typename Dune::FieldMatrix<T, n, m> >
{
    typedef typename Dune::FieldMatrix<T, n, m> RT;

    template<class RFT>
    static void setComponent(RT& x, int i, const RFT& xi)
    {
        x[i/m][i%m] = xi;
    }

    template<class RFT>
    static void getComponent(const RT& x, int i, RFT& xi)
    {
        xi = x[i/m][i%m];
    }

    static int size(const RT& x)
    {
        return n*m;
    }
};

template<class T>
struct Components<typename std::vector<T> >
{
    typedef typename std::vector<T> RT;

    template<class RFT>
    static void setComponent(RT& x, int i, const RFT& xi)
    {
        x[i] = xi;
    }

    template<class RFT>
    static void getComponent(const RT& x, int i, RFT& xi)
    {
        xi = x[i];
    }

    static int size(const RT& x)
    {
        return x.size();
    }
};



/** \brief Cached evaluation of local basis functions
 *
 * This class allows to evaluate single local basis functions efficiently
 * by providing a cache for the evaluation of all functions.
 *
 * \tparam Imp Implementation providing the evaluateAll method
 * \tparam FunctionBaseClass The base class for this wrapper
 */
template <class Imp, class AllRangeType, class FunctionBaseClass>
class CachedComponentWrapper :
    public FunctionBaseClass
{
    public:
        typedef typename FunctionBaseClass::DomainType DomainType;
        typedef typename FunctionBaseClass::RangeType RangeType;

    private:
        typedef DomainType Position;
        struct DomainCmp
        {
            bool operator() (const DomainType& a, const DomainType& b) const
            {
                for(int i=0; i<DomainType::dimension; ++i)
                {
                    if (a[i]<b[i])
                        return true;
                    if (a[i]>b[i])
                        return false;
                }
                return false;
            }
        };
        typedef typename std::map<DomainType, AllRangeType, DomainCmp> EvaluationCache;

    public:

        CachedComponentWrapper(const int i=0) :
            i_(i)
        {}

        /** \brief Select local basis function to evaluate.
         *
         * \param i Index of the local basis function within the local basis to evaluate next.
         */
        void setIndex(int i)
        {
            i_ = i;
        }

        /** \brief Evaluate local basis function selected last using setIndex
         *
         * \param x Local coordinates with repect to entity given in constructor.
         * \param y The result is stored here.
         */
        void evaluate(const DomainType& x, RangeType& y) const
        {
            typename EvaluationCache::const_iterator it = evalCache_.find(x);
            if (it != evalCache_.end())
                Components<AllRangeType>::getComponent(it->second, i_, y);
            else
            {
                AllRangeType& yy = evalCache_[x];
                asImp().evaluateAll(x, yy);
                Components<AllRangeType>::getComponent(yy, i_, y);
            }
        }

    protected:

        const Imp& asImp () const
        {
            return static_cast<const Imp &> (*this);
        }

        int i_;

        mutable EvaluationCache evalCache_;
};

#endif

