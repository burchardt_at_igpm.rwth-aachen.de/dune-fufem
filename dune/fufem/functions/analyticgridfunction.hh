// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef ANALYTIC_GRID_FUNCTION_HH
#define ANALYTIC_GRID_FUNCTION_HH

#include <memory>

#include <dune/common/exceptions.hh>
#include <dune/common/shared_ptr.hh>

#include <dune/fufem/functions/virtualdifferentiablefunction.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>




/**
 * \brief A Wrapper that implements evaluation in local coordinates
 *
 * This implements evaluateLocal() using the evaluate() method
 * of the wrapped function. If the wrapped function is derived
 * from VirtualDifferentiableFunction evaluateDerivativeLocal()
 * is implemented using evaluateDerivative(). Otherwise it throws
 * an exception.
 *
 * \tparam Grid the underlying GridType
 * \tparam F the wrapped function type
 *
 */
template<class Grid, class F>
class AnalyticGridFunction :
    public VirtualGridFunction<Grid, typename F::RangeType>
{
        typedef VirtualGridFunction<Grid, typename F::RangeType> Base;
        typedef AnalyticGridFunction<Grid, F> This;

    public:

        typedef typename Base::Element Element;
        typedef typename Base::RangeType RangeType;
        typedef typename Base::DomainType DomainType;
        typedef typename Base::LocalDomainType LocalDomainType;
        typedef typename Base::DerivativeType DerivativeType;

        /**
         * \brief Setup analytic grid function
         *
         * \param grid the underlying grid
         * \param f function that should implement evaluate()
         */
        AnalyticGridFunction(const Grid& grid, const F& f) :
            Base(grid),
            f_(f)
        {}

        /**
         * \copydoc VirtualDifferentiableFunction::evaluate
         */
        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            f_.evaluate(x, y);
        }

        /**
         * \brief evaluation of 1st derivative
         *
         * If wrapped function is not derived from VirtualDifferentiableFunction
         * an exceptions is thrown.
         *
         *  \param x point at which to evaluate the derivative
         *  \param d will contain the derivative at x after return
         */
        virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
        {
            typedef VirtualDifferentiableFunction<DomainType, RangeType> DifferentiableBase;
            const DifferentiableBase* fAsDifferentiable = dynamic_cast<const DifferentiableBase*>(&f_);
            if (fAsDifferentiable)
                fAsDifferentiable->evaluateDerivative(x, d);
            else
                DUNE_THROW(Dune::NotImplemented, "Wrapped function does not implement evaluateDerivative");
        }

        /**
         * \brief Function evaluation in local coordinates.
         *
         * Uses evaluate() of wrapped function in global coordinates.
         *
         * \param e Evaluate in local coordinates with respect to this element.
         * \param x Argument for function evaluation in local coordinates.
         * \param y Result of function evaluation.
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            f_.evaluate(e.geometry().global(x), y);
        }

        /** \brief evaluation of derivative in local coordinates
         *
         * Uses evaluateDerivative() of wrapped function in global coordinates
         * if wrapped function is derived from VirtualDifferentiableFunction.
         * Otherwise an exceptions is thrown.
         *
         *  \param e Evaluate in local coordinates with respect to this element.
         *  \param x point in local coordinates at which to evaluate the derivative
         *  \param d will contain the derivative at x after return
         */
        virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
        {
            This::evaluateDerivative(e.geometry().global(x), d);
        }

        /**
         * \brief Check whether local evaluation is possible
         *
         * \param e Returns always true.
         */
        virtual bool isDefinedOn(const Element& e) const
        {
            return true;
        }

    protected:

        const F& f_;
};



//namespace Functions
//{

/**
 * \brief Create grid function from any function
 *
 * If the function passed as argument is a already a grid function
 * a non-owning shared_ptr to this function is returned. Otherwise
 * a shared_ptr to a newly created AnalyticGridFunction is returned.
 *
 * \param grid Target grid for the desired grid function
 * \param f Function to be wrapped a grid function
 */
template<class GridType, class FunctionType>
std::shared_ptr<const VirtualGridFunction<GridType, typename FunctionType::RangeType> > makeGridFunction(const GridType& grid, const FunctionType& f)
{
    typedef VirtualGridFunction<GridType, typename FunctionType::RangeType> GF;
    typedef AnalyticGridFunction<GridType, FunctionType> AGF;
    typedef typename std::shared_ptr<const GF> GFSP;
    const GF* gf = dynamic_cast<const GF*>(&f);

    if (gf)
        return Dune::stackobject_to_shared_ptr(*gf);

    return GFSP(new AGF(grid, f));
}

//} // end namespace Functions

#endif

