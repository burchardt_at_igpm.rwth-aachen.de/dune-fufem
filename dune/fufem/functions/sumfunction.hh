// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUM_FUNCTION_HH
#define SUM_FUNCTION_HH

#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/function.hh>

/** \brief  A class implementing linear combinations of several functions.
 *
 *  The function \f$ \sum a_if_i:A\rightarrow B \f$ where \f$ (\sum a_if_i)(x) = \sum a_i\cdot f_i(x) \f$
 *  is implemented.
 *  \tparam DT type of \f$ domain(f_i)\f$
 *  \tparam RT type of \f$ range(f_i)\f$
 */
template <typename DT, typename RT>
class SumFunction :
    virtual public Dune::VirtualFunction<DT, RT>
{
    typedef Dune::VirtualFunction<DT,RT> BaseType;
public:
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;

    /** \brief Default Constructor
     *
     */
    SumFunction()
    {}

    /** \brief Constructor
     *
     */
    SumFunction(std::vector<const BaseType*>& functions, std::vector<double>& coefficients):
        functions_(functions),
        coefficients_(coefficients)
    {
        if (functions_.size() != coefficients.size())
            DUNE_THROW(Dune::Exception,"You have not supplied the correct number of coefficients in  SumFunction::SumFunction(std::vector<BaseType*>&, std::vector<double>&) (sumfunction.hh)");
    }

    /** \brief evaluation using evaluate of the respective functions
     *
     *  \param x point in \f$ A \f$ at which to evaluate
     *  \param y variable to store the function value in
     */
    virtual void evaluate(const DomainType& x,  RangeType& y) const
    {
        y = 0.0;

        if (functions_.size()==0)
            DUNE_THROW(Dune::Exception,"No function has been registered for summed evaluation in SumFunction::evaluate (sumfunction.hh)");

        RangeType ytemp;

        for (size_t i=0; i<functions_.size(); ++i)
        {
            functions_[i]->evaluate(x,ytemp);
            ytemp *= coefficients_[i];
            y += ytemp;
        }

        return;
    }

    /** \brief register functions to be summed up
     *
     * \param coefficient for summand function
     * \param function summand function to register
     */
    virtual void registerFunction(double coefficient, const BaseType& function)
    {
        functions_.push_back(&function);
        coefficients_.push_back(coefficient);
    }

    ~SumFunction(){}

private:
    std::vector<const BaseType*> functions_;
    std::vector<double> coefficients_;

};

#endif

