// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUM_GRID_FUNCTION_HH
#define SUM_GRID_FUNCTION_HH

#include <vector>

#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief  A class implementing linear combinations of several GridFunctions.
 *
 *  The function \f$ \sum a_if_i:GridType:[...]:GlobalCoordinate\rightarrow B \f$ where \f$ (\sum a_if_i)(x) = \sum a_i\cdot f_i(x) \f$
 *  is implemented.
 *  \tparam  GridType the GridType of the grid on which the GridFunctions are given
 *  \tparam  RT type of \f$ range(f_i)\f$
 */
template <typename GridType, typename RT>
class SumGridFunction:
    public VirtualGridFunction<GridType, RT>
{

    typedef VirtualGridFunction<GridType, RT> BaseType;
    typedef typename GridType::Traits::template Codim<0>::Entity Element;

public:

    typedef typename BaseType::LocalDomainType LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;


    /** \brief Default Constructor
     *
     */
    SumGridFunction(const GridType& grid):
        BaseType(grid)
    {}

    /** \brief summed up local evaluation using evaluateLocal of the registered functions
     *
     *  \param e Grid Element on which to evaluate locally
     *  \param x point at which to evaluate
     *  \param y object to store the function value in
     */
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
    {
        if (not(isDefinedOn(e)))
            DUNE_THROW(Dune::Exception, "SumGridFunction not defined on given element.");

        y = 0.0;

        if (functions_.size()==0)
            DUNE_THROW(Dune::Exception,"No function has been registered for summed evaluation");

        RangeType ytemp;

        for (size_t i=0; i<functions_.size(); ++i)
        {
            functions_[i]->evaluateLocal(e,x,ytemp);
            ytemp *= coefficients_[i];
            y += ytemp;
        }

        return;
    }

    /** \brief evaluation of derivative in local coordinates
     *
     *  \param e Evaluate in local coordinates with respect to this element.
     *  \param x point in local coordinates at which to evaluate the derivative
     *  \param d will contain the derivative at x after return
     */
    virtual void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
    {
        if (not(isDefinedOn(e)))
            DUNE_THROW(Dune::Exception, "SumGridFunction not defined on given element.");

        d = 0.0;

        if (functions_.size()==0)
            DUNE_THROW(Dune::Exception,"No function has been registered for summed evaluation");

        DerivativeType dtemp;

        for (size_t i=0; i<functions_.size(); ++i)
        {
            functions_[i]->evaluateDerivativeLocal(e,x,dtemp);
            dtemp *= coefficients_[i];
            d += dtemp;
        }

        return;
    }

    /**
     * \brief Check whether local evaluation is possible
     *
     * \param e Return true if local evaluation is possible on this element.
     */
    virtual bool isDefinedOn(const Element& e) const
    {
        bool r=true;
        for (size_t i=0; i<functions_.size(); ++i)
            r = r and functions_[i]->isDefinedOn(e);

        return r;
    }

    /** \brief register functions to be summed up
     *
     * \param coefficient of summand function
     * \param function summand (grid-)function to register
     */
    virtual void registerFunction(double coefficient, const BaseType& function)
    {
        functions_.push_back(&function);
        coefficients_.push_back(coefficient);
    }

    ~SumGridFunction(){}

private:
    std::vector<const BaseType*> functions_;
    std::vector<double> coefficients_;

};

#endif

