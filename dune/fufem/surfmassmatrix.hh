#ifndef SURFACE_MASSMATRIX_HH
#define SURFACE_MASSMATRIX_HH

#warning This file is deprecated!  Use dune/fufem/assemblers/boundaryoperatorassembler.hh instead!

/** \file
    \brief Contains a method which assembles the FE mass matrix of a boundary patch of a grid.
*/

#include <dune/geometry/quadraturerules.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/assemblers/localassemblers/massassembler.hh>

template <class GridView, int blocksize>
void assembleSurfaceMassMatrix(const BoundaryPatch<GridView>& boundary,
                               Dune::BCRSMatrix<Dune::FieldMatrix<double,blocksize,blocksize> >& matrix)
{
    using namespace Dune;

    std::vector<int> globalToLocal = boundary.makeGlobalToLocal();

    // we need a P1 nodal basis as the shape function factory
    using Basis = P1NodalBasis<GridView,double>;
    Basis p1Basis(boundary.gridView());

    // /////////////////////////////////////////////////
    //   Compute matrix occupation structure
    // /////////////////////////////////////////////////

    MatrixIndexSet indices(boundary.numVertices(), boundary.numVertices());

    for (const auto& it : boundary) {

        const auto& inside = it.inside();
        // Get set of shape functions on this element
        const auto& localFiniteElement = p1Basis.getLocalFiniteElement(inside);
        size_t nDofs = localFiniteElement.localBasis().size();

        for (size_t i=0; i<nDofs; i++) {

            int globalRow = globalToLocal[p1Basis.index(inside, i)];

            // Degree of freedom that is not on the boundary
            if (globalRow < 0)
                continue;

            for (size_t j=0; j<nDofs; j++) {

                int globalCol = globalToLocal[p1Basis.index(inside, j)];

                // Degree of freedom that is on the boundary
                if (globalCol >= 0)
                    indices.add(globalRow,globalCol);

            }

        }

    }


    // /////////////////////////////////////////////////
    //   Compute matrix values
    // /////////////////////////////////////////////////

    // set up the matrix
    indices.exportIdx(matrix);
    matrix = 0;

    MassAssembler<typename GridView::Grid, typename Basis::LocalFiniteElement, typename Basis::LocalFiniteElement> localMass;

    for (auto it = boundary.begin(); it != boundary.end(); it++) {

        const auto& inside = it->inside();

        const auto& localFiniteElement = p1Basis.getLocalFiniteElement(inside);
        size_t nDofs = localFiniteElement.localBasis().size();

        Matrix<FieldMatrix<double,1,1> > scalarMat(nDofs, nDofs);
        localMass.assemble(it, scalarMat, localFiniteElement, localFiniteElement);

        for (size_t i=0; i<nDofs; i++) {

            int globalRow = globalToLocal[p1Basis.index(inside, i)];

            // Degree of freedom that is not on the boundary
            if (globalRow < 0)
                continue;

            for (size_t j=0; j<nDofs; j++) {

                int globalCol = globalToLocal[p1Basis.index(inside, j)];

                // Degree of freedom that is not on the boundary
                if (globalCol < 0)
                    continue;

                Dune::MatrixVector::addToDiagonal(matrix[globalRow][globalCol], scalarMat[i][j]);
            }
        }
    }
}

#endif
