// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_DG_INDEX_SET_HH
#define DUNE_FUFEM_DG_INDEX_SET_HH

#include <vector>

#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/mcmgmapper.hh>

/** \brief An index set for first-order DG functions on the grid boundary */
template <class GridView>
class DGIndexSet {

public:

    DGIndexSet(const GridView& gridView) : mapper_(gridView,Dune::mcmgLayout(Dune::Codim<1>()))
    {
        setup(gridView);
    }

    void setup(const GridView& gridView) {

        mapper_.update();

        const int dim = GridView::dimension;

        // Compute the number of vertices for each face
        faceOffsets_.resize(gridView.size(1) + 1);

        for (const auto& e:  elements(gridView)) {

            const auto& refElement = Dune::ReferenceElements<typename GridView::ctype, dim>::general(e.type());

            // Alternative without NeighborIterator
            // Get the number of vertices for each face
            for (int i=0; i<refElement.size(1); i++)
                faceOffsets_[mapper_.subIndex(e, i, 1) + 1] = refElement.size(i,1,dim);

        }

        // Accumulate
        faceOffsets_[0] = 0;
        for (size_t i=1; i<faceOffsets_.size(); i++)
            faceOffsets_[i] += faceOffsets_[i-1];
    }

    int size() const {
        return faceOffsets_[faceOffsets_.size()-1];
    }

    int operator()(const typename GridView::template Codim<0>::Entity& element, int subFace) const {
        return faceOffsets_[mapper_.subIndex(element, subFace, 1)];
    }

    std::vector<int> faceOffsets_;

    Dune::MultipleCodimMultipleGeomTypeMapper<GridView> mapper_;
};

#endif
