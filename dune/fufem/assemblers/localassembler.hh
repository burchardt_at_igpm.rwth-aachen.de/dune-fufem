// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef LOCAL_ASSEMBLER_HH
#define LOCAL_ASSEMBLER_HH

#include <type_traits>

#include <dune/common/fmatrix.hh>
#include <dune/istl/matrix.hh>


/** \brief Abstract base class for local assemblers
 *
 *  \tparam GridType The grid we are assembling on
 *  \tparam TrialLocalFE the local finite element of the trial space
 *  \tparam AnsatzLocalFE the local finite element of the ansatz space
 *  \tparam T the block type
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, typename T>
class LocalAssembler 
{

    public:

        typedef typename GridType::template Codim<0>::Entity::Entity Element;
        typedef Dune::Matrix< Dune::FieldMatrix<int,1,1> > BoolMatrix;
        typedef typename Dune::Matrix<T> LocalMatrix;
        typedef typename T::field_type field_type;
        typedef T MatrixEntry;

        enum {ndofs = T::cols};

        /** \brief After return BoolMatrix isNonZero contains the bit pattern of coupling basis functions
         *
         *  \param element the grid element on which to operate
         *  \param isNonZero will contain bit pattern of coupling basis functions after return
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        virtual void indices(const Element& element, BoolMatrix& isNonZero,
                const TrialLocalFE& tFE,
                const AnsatzLocalFE& aFE) const = 0;

        /** \brief Assemble a local problem providing a local solution.
         *
         *  Since this is a linear assembler the solution is simply discarded.
         */
        virtual void assemble(const Element& element, 
                const Dune::BlockVector<Dune::FieldVector<field_type,ndofs> >& localSolution,
                LocalMatrix& localMatrix,
                const TrialLocalFE& tFE,
                const AnsatzLocalFE& aFE) const
        {
            assemble(element, localMatrix, tFE, aFE);
        }

        /** \brief Assemble a local problem
         *
         *  \param element the grid element on which to operate
         *  \param localMatrix will contain the assembled element matrix
         *  \param tFE the local finite element in the trial space used on element
         *  \param aFE the local finite element in the ansatz space used on element
         */
        virtual void assemble(const Element& element, LocalMatrix& localMatrix,
                const TrialLocalFE& tFE,
                const AnsatzLocalFE& aFE) const = 0;


        /** \brief Check if trial and ansatz local finite element are the same
         *
         *  \param tFE a local finite element in the trial space
         *  \param aFE a local finite element in the ansatz space
         */
        static bool isSameFE(const TrialLocalFE& tFE, const AnsatzLocalFE& aFE)
        {
            if (not std::is_same<TrialLocalFE, AnsatzLocalFE>::value)
                return false;
            return &tFE == reinterpret_cast<const TrialLocalFE*>(&aFE);
        }

};


#endif

