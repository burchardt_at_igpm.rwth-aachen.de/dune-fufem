// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"


namespace Dune {
namespace Fufem {


//! Generic global assembler for operators on a gridview
template <class TrialBasis, class AnsatzBasis>
class DuneFunctionsOperatorAssembler
{
  using GridView = typename TrialBasis::GridView;

public:
  //! create assembler for grid
  DuneFunctionsOperatorAssembler(const TrialBasis& tBasis, const AnsatzBasis& aBasis) :
      trialBasis_(tBasis),
      ansatzBasis_(aBasis)
  {}



  template <class PatternBuilder>
  void assembleBulkPattern(PatternBuilder& patternBuilder) const
  {
    patternBuilder.resize(trialBasis_, ansatzBasis_);

    auto trialLocalView     = trialBasis_.localView();
    auto trialLocalIndexSet = trialBasis_.localIndexSet();

    auto ansatzLocalView     = ansatzBasis_.localView();
    auto ansatzLocalIndexSet = ansatzBasis_.localIndexSet();

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      trialLocalView.bind(element);
      trialLocalIndexSet.bind(trialLocalView);

      ansatzLocalView.bind(element);
      ansatzLocalIndexSet.bind(ansatzLocalView);

      // Add element stiffness matrix onto the global stiffness matrix
      for (size_t i=0; i<trialLocalIndexSet.size(); ++i)
      {
        auto row = trialLocalIndexSet.index(i);
        for (size_t j=0; j<ansatzLocalIndexSet.size(); ++j)
        {
          auto col = ansatzLocalIndexSet.index(j);
          patternBuilder.insertEntry(row,col);
        }
      }
    }
  }



  template <class PatternBuilder>
  void assembleSkeletonPattern(PatternBuilder& patternBuilder) const
  {
    patternBuilder.resize(trialBasis_, ansatzBasis_);

    auto insideTrialLocalView       = trialBasis_.localView();
    auto insideTrialLocalIndexSet   = trialBasis_.localIndexSet();

    auto insideAnsatzLocalView      = ansatzBasis_.localView();
    auto insideAnsatzLocalIndexSet  = ansatzBasis_.localIndexSet();

    auto outsideTrialLocalView      = trialBasis_.localView();
    auto outsideTrialLocalIndexSet  = trialBasis_.localIndexSet();

    auto outsideAnsatzLocalView     = ansatzBasis_.localView();
    auto outsideAnsatzLocalIndexSet = ansatzBasis_.localIndexSet();

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      insideTrialLocalView.bind(element);
      insideTrialLocalIndexSet.bind(insideTrialLocalView);

      insideAnsatzLocalView.bind(element);
      insideAnsatzLocalIndexSet.bind(insideAnsatzLocalView);

      /* Coupling on the same element */
      for (size_t i = 0; i < insideTrialLocalIndexSet.size(); i++) {
        auto row = insideTrialLocalIndexSet.index(i);
        for (size_t j = 0; j < insideAnsatzLocalIndexSet.size(); j++) {
          auto col = insideAnsatzLocalIndexSet.index(j);
          patternBuilder.insertEntry(row,col);
        }
      }

      for (const auto& is : intersections(trialBasis_.gridView(), element))
      {
        /* Elements on the boundary have been treated above, iterate along the inner edges */
        if (!is.boundary())
        {
          // Current outside element
          const auto& outsideElement = is.outside();

          // Bind the outer parts to the outer element
          outsideTrialLocalView.bind(outsideElement);
          outsideTrialLocalIndexSet.bind(outsideTrialLocalView);

          outsideAnsatzLocalView.bind(outsideElement);
          outsideAnsatzLocalIndexSet.bind(outsideAnsatzLocalView);

          // We assume that all basis functions of the inner element couple with all basis functions from the outer one
          for (size_t i=0; i<insideTrialLocalIndexSet.size(); ++i)
          {
            auto row = insideTrialLocalIndexSet.index(i);
            for (size_t j=0; j<outsideAnsatzLocalIndexSet.size(); ++j)
            {
              auto col = outsideAnsatzLocalIndexSet.index(j);
              patternBuilder.insertEntry(row,col);
            }
          }
        }
      }
    }
  }



  template <class MatrixBackend, class LocalAssembler>
  void assembleBulkEntries(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler) const
  {
    auto trialLocalView     = trialBasis_.localView();
    auto trialLocalIndexSet = trialBasis_.localIndexSet();

    auto ansatzLocalView     = ansatzBasis_.localView();
    auto ansatzLocalIndexSet = ansatzBasis_.localIndexSet();

    using Field = std::decay_t<decltype(matrixBackend(trialLocalIndexSet.index(0), ansatzLocalIndexSet.index(0)))>;
    using LocalMatrix = Dune::Matrix<Dune::FieldMatrix<Field,1,1>>;

    auto localMatrix = LocalMatrix(trialLocalView.maxSize(), ansatzLocalView.maxSize());

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      trialLocalView.bind(element);
      trialLocalIndexSet.bind(trialLocalView);

      ansatzLocalView.bind(element);
      ansatzLocalIndexSet.bind(ansatzLocalView);

      localAssembler(element, localMatrix, trialLocalView, ansatzLocalView);

      // Add element stiffness matrix onto the global stiffness matrix
      for (size_t localRow=0; localRow<trialLocalIndexSet.size(); ++localRow)
      {
        auto row = trialLocalIndexSet.index(localRow);
        for (size_t localCol=0; localCol<ansatzLocalIndexSet.size(); ++localCol)
        {
          auto col = ansatzLocalIndexSet.index(localCol);
          matrixBackend(row,col) += localMatrix[localRow][localCol];
        }
      }
    }
  }


  /* This variant of the IntersectionOperatorAssembler that works
   * with dune-functions bases.
   *
   * Currently not supported are constrained bases and lumping.
   *
   * Note that this was written (and tested) having discontinuous Galerkin bases in mind.
   * There may be cases where things do not work as expected for standard (continuous) Galerkin spaces.
   */
  template <class MatrixBackend, class LocalAssembler, class LocalBoundaryAssembler>
  void assembleSkeletonEntries(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler, LocalBoundaryAssembler&& localBoundaryAssembler) const
  {
    Dune::MultipleCodimMultipleGeomTypeMapper<GridView> faceMapper(trialBasis_.gridView(), mcmgElementLayout());

    auto insideTrialLocalView       = trialBasis_.localView();
    auto insideTrialLocalIndexSet   = trialBasis_.localIndexSet();

    auto insideAnsatzLocalView      = ansatzBasis_.localView();
    auto insideAnsatzLocalIndexSet  = ansatzBasis_.localIndexSet();

    auto outsideTrialLocalView      = trialBasis_.localView();
    auto outsideTrialLocalIndexSet  = trialBasis_.localIndexSet();

    auto outsideAnsatzLocalView     = ansatzBasis_.localView();
    auto outsideAnsatzLocalIndexSet = ansatzBasis_.localIndexSet();

    using Field = std::decay_t<decltype(matrixBackend(insideTrialLocalIndexSet.index(0), insideAnsatzLocalIndexSet.index(0)))>;
    using LocalMatrix = Dune::Matrix<Dune::FieldMatrix<Field,1,1>>;
    using MatrixContainer = Dune::Matrix<LocalMatrix>;
    auto matrixContrainer = MatrixContainer(2,2);

    // Resize
    matrixContrainer[0][0].setSize(insideTrialLocalView.maxSize(), insideAnsatzLocalView.maxSize());
    matrixContrainer[0][1].setSize(insideTrialLocalView.maxSize(), outsideAnsatzLocalView.maxSize());
    matrixContrainer[1][0].setSize(outsideTrialLocalView.maxSize(), insideAnsatzLocalView.maxSize());
    matrixContrainer[1][1].setSize(outsideTrialLocalView.maxSize(), outsideAnsatzLocalView.maxSize());

    for (const auto& element : elements(trialBasis_.gridView()))
    {
      insideTrialLocalView.bind(element);
      insideTrialLocalIndexSet.bind(insideTrialLocalView);

      insideAnsatzLocalView.bind(element);
      insideAnsatzLocalIndexSet.bind(insideAnsatzLocalView);

      for (const auto& is : intersections(trialBasis_.gridView(), element))
      {

        /* Assemble (depending on whether we have a boundary edge or not) */
        if (is.boundary())
        {
          auto& localMatrix = matrixContrainer[0][0];
          localBoundaryAssembler(is, localMatrix, insideTrialLocalView, insideAnsatzLocalView);

          for (size_t i=0; i< insideTrialLocalIndexSet.size(); i++) {
            for (size_t j=0; j< insideAnsatzLocalIndexSet.size(); j++)
              matrixBackend(insideTrialLocalIndexSet.index(i), insideAnsatzLocalIndexSet.index(j))+=localMatrix[i][j];
          }
        }
        else
        {
          /* Only handle every intersection once; we choose the case where the inner element has the lower index
           *
           * This should also work with grids where elements can have different geometries. TODO: Actually test this somewhere!*/
          const auto& outsideElement = is.outside();
          if (faceMapper.index(element) > faceMapper.index(outsideElement))
            continue;


          // Bind the outer parts to the outer element
          outsideTrialLocalView.bind(outsideElement);
          outsideTrialLocalIndexSet.bind(outsideTrialLocalView);

          outsideAnsatzLocalView.bind(outsideElement);
          outsideAnsatzLocalIndexSet.bind(outsideAnsatzLocalView);

          localAssembler(is, matrixContrainer, insideTrialLocalView, insideAnsatzLocalView, outsideTrialLocalView, outsideAnsatzLocalView);

          for (size_t i=0; i < insideTrialLocalIndexSet.size(); i++)
          {
            auto row = insideTrialLocalIndexSet.index(i);
            // inside x inside
            for (size_t j=0; j < insideTrialLocalIndexSet.size(); j++)
            {
                auto col = insideTrialLocalIndexSet.index(j);
                matrixBackend(row, col) += matrixContrainer[0][0][i][j];
            }
            // inside x outside
            for (size_t j=0; j < outsideAnsatzLocalIndexSet.size(); j++)
            {
              auto col = outsideAnsatzLocalIndexSet.index(j);
              matrixBackend(row, col) += matrixContrainer[0][1][i][j];
            }
          }

          for (size_t i=0; i < outsideTrialLocalIndexSet.size(); i++)
          {
            auto row = outsideTrialLocalIndexSet.index(i);
            // outside x inside
            for (size_t j=0; j < insideAnsatzLocalIndexSet.size(); j++)
            {
              auto col = insideAnsatzLocalIndexSet.index(j);
              matrixBackend(row, col) += matrixContrainer[1][0][i][j];
            }
            // outside x outside
            for (size_t j=0; j < outsideAnsatzLocalIndexSet.size(); j++)
            {
              auto col = outsideAnsatzLocalIndexSet.index(j);
              matrixBackend(row, col) += matrixContrainer[1][1][i][j];
            }
          }
        }
      }
    }
  }


  template <class MatrixBackend, class LocalAssembler>
  void assembleBulk(MatrixBackend&& matrixBackend, LocalAssembler&& localAssembler) const
  {
    auto patternBuilder = matrixBackend.patternBuilder();
    assembleBulkPattern(patternBuilder);
    patternBuilder.setupMatrix();
    assembleBulkEntries(matrixBackend, std::forward<LocalAssembler>(localAssembler));
  }

protected:
  const TrialBasis& trialBasis_;
  const AnsatzBasis& ansatzBasis_;
};


/**
 * \brief Create DuneFunctionsOperatorAssembler
 */
template <class TrialBasis, class AnsatzBasis>
auto duneFunctionsOperatorAssembler(const TrialBasis& trialBasis, const AnsatzBasis& ansatzBasis)
{
  return DuneFunctionsOperatorAssembler<TrialBasis, AnsatzBasis>(trialBasis, ansatzBasis);
}



} // namespace Fufem
} // namespace Dune


#endif // DUNE_FUFEM_ASSEMBLERS_DUNEFUNCTIONSOPERATORASSEMBLER_HH

