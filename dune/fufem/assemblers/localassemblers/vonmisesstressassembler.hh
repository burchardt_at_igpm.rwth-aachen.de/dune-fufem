// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef VON_MISES_STRESS_ASSEMBLER_HH
#define VON_MISES_STRESS_ASSEMBLER_HH

#include <cmath>
#include <memory>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Computes the von Mises stress at the element center.
 *
 *  The von Mises stress is given by
 *
 *   sqrt(sigma_x^2 + sigma_y^2 + sigma_z^2 - sigma_x*sigma_y -sigma_x*sigma_z -sigma_y*sigma_z + 3*(tau_xy^2 + tau_xz^2 +tau_yz^2) )
 *
 * where
 *      sigma_* - the normal stresses
 *      tau_**  - the shear stresses
 *
 * To get the correct global distribution this assembler has to be used with P0 global basis!
 */
template <class GridType, class TrialLocalFE>
class VonMisesStressAssembler :
    public LocalFunctionalAssembler<GridType, TrialLocalFE, Dune::FieldVector<typename GridType::ctype,1> >

{
    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype,1> T;
    typedef LocalFunctionalAssembler<GridType, TrialLocalFE,T> Base;

public:
    typedef typename Base::Element Element;
    typedef typename Base::LocalVector LocalVector;
    typedef VirtualGridFunction<GridType, Dune::FieldVector<ctype,GridType::dimension> > GridFunction;

    /**
     * \brief Create assembler for grid
     * \param E  Young's modulus
     * \param nu Poisson's ratio
     */
    VonMisesStressAssembler(double E, double nu, std::shared_ptr<GridFunction const> displace) :
        lambda_(E * nu / (1.0+nu) / (1.0-2.0*nu) ),
        twoMu_(E / (1+nu) ),
        displace_(displace)
    {}

    /** \brief Assemble the von Mises stress. Since it is not really a functional the trial finite element is not used. */
    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    {

        if (tFE.localBasis().size() != 1)
            DUNE_THROW(Dune::Exception, "Use P0Basis in the FunctionalAssembler or this will not return the correct P0 data!");

        localVector = 0.0;

        // get entity geometry
        const typename Element::Geometry geometry = element.geometry();

        // get center of the element
        Dune::FieldVector<ctype, dim> center = geometry.local(geometry.center());

        // evaluate the displacement gradient at the center of the element
        typename GridFunction::DerivativeType localDispGradient;
        if (displace_->isDefinedOn(element))
            displace_->evaluateDerivativeLocal(element, center, localDispGradient);
        else
            displace_->evaluateDerivative(geometry.center(),localDispGradient);

        // Compute strain tensor from the displacement gradient
        SymmetricTensor<dim> strain;
        computeStrain(localDispGradient,strain);

        // and the stress
        SymmetricTensor<dim> stress = hookeTimesStrain(strain);

        localVector[0] = getVonMises(stress);
    }


private:
    //! Lame's first parameter
    const ctype lambda_;
    //! Lame's second parameter (also known as G)
    const ctype twoMu_;

    /** \brief The displacement.*/
    std::shared_ptr<GridFunction const> const displace_;

    /** \brief Compute linear strain tensor from the deformation gradient. */
    void computeStrain(const Dune::FieldMatrix<double, dim, dim>& grad, SymmetricTensor<dim>& strain) const {

        for (int i=0; i<dim ; ++i)
        {
            strain(i,i) = grad[i][i];
            for (int j=i+1; j<dim; ++j)
                strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
        }

    }

    /** \brief Compute the stress tensor from strain tensor. */
    SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {

        SymmetricTensor<dim> stress = strain;
        stress *= twoMu_;
        stress.addToDiag(lambda_ * strain.trace());
        return stress;
    }

    /** \brief Compute the von Mises stress from the stress tensor. */
    double getVonMises(const SymmetricTensor<dim> stress) const {

        double vonMises =  0;

        for (int i=0; i<dim-1; i++)
            for (int j=i+1; j<dim; j++) {
                vonMises += (stress(i,i) - stress(j,j)) * (stress(i,i) - stress(j,j));
                vonMises += 6 * (stress(i,j) * stress(i,j));
        }

        return std::sqrt(vonMises / 2);
    }


};

#endif


