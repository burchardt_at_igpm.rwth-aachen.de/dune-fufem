#ifndef DUNE_FUFEM_DUNEFUNCTIONS_L2_FUNCTIONAL_ASSEMBLER_HH
#define DUNE_FUFEM_DUNEFUNCTIONS_L2_FUNCTIONAL_ASSEMBLER_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>



namespace Dune {
namespace Fufem {



/**
 * \brief Local finite element assembler for L^2 functionals.
 *
 * In contrast to the L2FunctionalAssembler this uses the
 * function- and gridfunction-interface of dune-functions.
 */
template <class G, class TrialLocalFE, class F, class T=Dune::FieldVector<double,1> >
class DuneFunctionsL2FunctionalAssembler :
    public LocalFunctionalAssembler<G, TrialLocalFE, T>
{
    private:

        using Grid = G;
        using Base = LocalFunctionalAssembler<Grid, TrialLocalFE, T>;
        using GlobalCoordinate = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
        using LocalCoordinate = typename Grid::template Codim<0>::Geometry::LocalCoordinate;
        using Function = F;
        using LocalFunction = std::decay_t<decltype(localFunction(std::declval<Function>()))>;

        static const int dim = Grid::dimension;
        static const int dimworld = Grid::dimensionworld;

    public:

        using Element = typename Base::Element;
        using Geometry = typename Base::Element::Geometry;
        using LocalVector = typename Base::LocalVector;

        /**
         * \brief Create DuneFunctionsL2FunctionalAssembler
         *
         * Creates a local functional assembler for an L2-functional.
         * The QuadratureRuleKey given here does only specify what is
         * needed to integrate f. Depending on this the quadrature rule
         * is selected automatically such that fv for test functions v
         * can be integrated.
         * If you, e.g., specify quadrature order 1 for f and the test
         * functions need order 2 the selected quadrature rule will
         * have order 3.
         *
         * Depending of whether f is passed as L-value or R-value
         * a reference or copy of f will be stored.
         *
         * \param f The L2 function
         * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
         */
        template<class FF>
        DuneFunctionsL2FunctionalAssembler(FF&& f, const QuadratureRuleKey& fQuadKey) :
            f_(Dune::wrap_or_move<const Function>(std::forward<FF>(f))),
            localF_(localFunction(*f_)),
            functionQuadKey_(fQuadKey)
        {}

        /**
         * \brief Create DuneFunctionsL2FunctionalAssembler
         *
         * Using this constructor the quadrature order will be selected
         * such that the test functions can be integrated exactly.
         *
         * Depending of whether f is passed as L-value or R-value
         * a reference or copy of f will be stored.
         *
         * \param f The L2 function
         */
        template<class FF, disableCopyMove<DuneFunctionsL2FunctionalAssembler, FF> = 0 >
        DuneFunctionsL2FunctionalAssembler(F&& f) :
            f_(Dune::wrap_or_move<const Function>(std::forward<FF>(f))),
            localF_(localFunction(f_)),
            functionQuadKey_(dim, 0)
        {}

        virtual void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const override
        {
            using FERange = typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType;

            localF_.bind(element);

            // get geometry
            const auto& geometry = element.geometry();

            localVector = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.product(functionQuadKey_);
            const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<FERange> values(tFE.localBasis().size());

            // TODO: This currently assumes f to be a DiscreteGlobalBasisFunction in the dune-functions sense
            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const auto& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                // compute values of function
                auto f_pos = localF_(quadPos);

                // and vector entries
                for(size_t i=0; i<values.size(); ++i)
                {
                    localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, (T)f_pos);
                }
            }
            return;
        }

    private:
        std::shared_ptr<const Function> f_;
        mutable LocalFunction localF_;
        const QuadratureRuleKey functionQuadKey_;
};



template<class G, class LFE, class F>
auto makeDuneFunctionsL2FunctionalAssembler(const F& f, QuadratureRuleKey quadKey) {
  return DuneFunctionsL2FunctionalAssembler<G, LFE, F>(f, quadKey);
}



} // end namespace Fufem
} // end namespace Dune



#endif
