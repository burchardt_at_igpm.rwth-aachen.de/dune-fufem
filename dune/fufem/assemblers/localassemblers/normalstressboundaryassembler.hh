#ifndef NORMAL_STRESS_BOUNDARY_ASSEMBLER_HH
#define NORMAL_STRESS_BOUNDARY_ASSEMBLER_HH

#include <dune/common/fvector.hh>
#include <dune/common/version.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/assemblers/localboundaryassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

/** \brief Computes the normal stress of a linear elastic material. The type of the trial basis
 *         determines to which dofs the stresses are associated with.
 *
 */
template <class GridType>
class NormalStressBoundaryAssembler :
    public LocalBoundaryAssembler<GridType, Dune::FieldVector<typename GridType::ctype ,GridType::dimension> >

{
    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype,dim> T;

public:
    typedef typename LocalBoundaryAssembler<GridType,T>::LocalVector LocalVector;
    typedef VirtualGridFunction<GridType, Dune::FieldVector<ctype,GridType::dimension> > GridFunction;

    //! Create assembler for grid
    NormalStressBoundaryAssembler(double E, double nu, const GridFunction* displace, int order) :
        E_(E),
        nu_(nu),
        displace_(displace),
        order_(order)
    {}

    /** \brief Assemble the normal stress at a boundary face.*/
    template <class TrialLocalFE, class BoundaryIterator>
    void assemble(const BoundaryIterator& it, LocalVector& localVector, const TrialLocalFE& tFE) {
        const auto inside = it->inside();

        localVector = 0.0;

        // geometry of the boundary face
        const typename BoundaryIterator::Intersection::Geometry segmentGeometry = it->geometry();

        // get quadrature rule
        const int order = inside.type().isSimplex()
            ? 2*(order_-1)
            : 2*(order_*dim-1);

        // get quadrature rule
        const Dune::QuadratureRule<ctype, dim-1>& quad = QuadratureRuleCache<ctype, dim-1>::rule(segmentGeometry.type(), order, false );

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt)
        {
            // get quadrature point
            const Dune::FieldVector<ctype,dim-1>& quadPos = quad[pt].position();

            // get integration factor
            const ctype integrationElement = segmentGeometry.integrationElement(quadPos);

            // get outer unit normal at quad pos
            Dune::FieldVector<ctype, dim> normalAtQuadPos = it->unitOuterNormal(quadPos);

            // position of the quadrature point within the element
            const Dune::FieldVector<ctype,dim> elementQuadPos = it->geometryInInside().global(quadPos);

            // evaluate the displacement gradient at quad point of the element
            typename GridFunction::DerivativeType localDispGradient;
            if (displace_->isDefinedOn(inside))
                displace_->evaluateDerivativeLocal(inside, elementQuadPos, localDispGradient);
            else
                displace_->evaluateDerivative(segmentGeometry.global(quadPos),localDispGradient);

            // Compute strain tensor from the displacement gradient
            SymmetricTensor<dim> strain;
            computeStrain(localDispGradient,strain);

            // and the stress
            SymmetricTensor<dim> stress = hookeTimesStrain(strain);

            normalAtQuadPos *= quad[pt].weight()*integrationElement;
            typename LocalVector::block_type normalStressAtQuadPos;
            stress.umv(normalAtQuadPos,normalStressAtQuadPos);

            for (size_t k=0; k<localVector.size(); k++)
                localVector[k] += normalStressAtQuadPos;
        }
    }

    ~NormalStressBoundaryAssembler() {}

private:
    /** \brief Young's modulus */
    double E_;

        /** \brief The Poisson ratio */
    double nu_;

    /** \brief The displacement.*/
    const GridFunction* displace_;

    /** \brief The gridfunction is usually corresponding to some finite element solution. This is its order.*/
    int order_;

    /** \brief Compute linear strain tensor from the deformation gradient. */
    void computeStrain(const Dune::FieldMatrix<double, dim, dim>& grad, SymmetricTensor<dim>& strain) const {

        for (int i=0; i<dim ; ++i)
        {
            strain(i,i) = grad[i][i];
            for (int j=i+1; j<dim; ++j)
                strain(i,j) = 0.5*(grad[i][j] + grad[j][i]);
        }

    }

    /** \brief Compute the stress tensor from strain tensor. */
    SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {

        SymmetricTensor<dim> stress = strain;
        stress *= E_/(1+nu_);

        stress.addToDiag(E_*nu_ / ((1+nu_)*(1-2*nu_)) * strain.trace());
        return stress;
    }

};

#endif


