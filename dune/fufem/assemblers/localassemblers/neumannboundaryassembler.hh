// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef NEUMANN_BOUNDARY_ASSEMBLER_HH
#define NEUMANN_BOUNDARY_ASSEMBLER_HH

#include <memory>

#include <dune/common/function.hh>
#include <dune/common/fvector.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

#include <dune/fufem/assemblers/localboundaryassembler.hh>

/** \brief Local Neumann boundary assembler
 * 
 * \tparam GridType The grid we are assembling for
 * \tparam T Type used for the set of dofs at one node
 */
template <class GridType, class T=Dune::FieldVector<typename GridType::ctype,1> >
class NeumannBoundaryAssembler :
    public LocalBoundaryAssembler<GridType, T>

{
    private:
        static const int dim = GridType::dimension;
        typedef typename GridType::ctype ctype;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef VirtualGridFunction<GridType, T> GridFunction;

    public:
        typedef typename LocalBoundaryAssembler<GridType,T>::LocalVector LocalVector;

        typedef typename Dune::VirtualFunction<GlobalCoordinate, T> Function;

        /** \brief Constructor
         * \param neumann Neumann force function
         * \param order The quadrature order used for numerical integration
         */
        NeumannBoundaryAssembler(const Function& neumann, int order=2) :
            neumann_(Dune::stackobject_to_shared_ptr(neumann)),
            order_(order)
        {}

        /** \brief Constructor
         * \param neumann Neumann force function
         * \param order The quadrature order used for numerical integration
         */
        NeumannBoundaryAssembler(std::shared_ptr<const Function> neumann, int order=2)
            : neumann_(neumann)
            , order_(order)
        {
            /* Nothing. */
        }

        template <class TrialLocalFE, class BoundaryIterator>
        void assemble(const BoundaryIterator& it, LocalVector& localVector, const TrialLocalFE& tFE)
        {
            typedef typename Dune::template FieldVector<ctype,dim> FVdim;
            typedef typename Dune::template FieldVector<ctype,T::dimension> FV;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            localVector = 0.0;

            // geometry of the boundary face
            const typename BoundaryIterator::Intersection::Geometry segmentGeometry = it->geometry();


            // get quadrature rule
            const Dune::QuadratureRule<ctype, dim-1>& quad = QuadratureRuleCache<ctype, dim-1>::rule(segmentGeometry.type(), order_, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                const auto inside = it->inside();

                // get quadrature point
                const Dune::FieldVector<ctype,dim-1>& quadPos = quad[pt].position();

                // get integration factor
                const ctype integrationElement = segmentGeometry.integrationElement(quadPos);

                // position of the quadrature point within the element
                const FVdim elementQuadPos = it->geometryInInside().global(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(elementQuadPos, values);

                // Evaluate neumann function at quadrature point. If it is a grid function use that to speed up the evaluation
                FV neumannVal;

                const GridFunction* gf = dynamic_cast<const GridFunction*>(neumann_.get());
                if (gf and gf->isDefinedOn(inside))
                    gf->evaluateLocal(inside, elementQuadPos, neumannVal);
                else
                    neumann_->evaluate(segmentGeometry.global(quadPos),neumannVal);

                // and vector entries
                for (size_t i=0; i<values.size(); ++i)
                {
                    localVector[i].axpy(values[i]*quad[pt].weight()*integrationElement, neumannVal);
                }
            }
            return;
        }

    private:
        const std::shared_ptr<const Function> neumann_;
        
        // quadrature order
        const int order_;
};

#endif

