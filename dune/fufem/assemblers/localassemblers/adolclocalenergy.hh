// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_ADOLCLOCALENERGY_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_ADOLCLOCALENERGY_HH

#include <dune/fufem/utilities/adolcnamespaceinjections.hh>

// Includes for the ADOL-C automatic differentiation library
// Need to come before (almost) all others.
#ifdef HAVE_ADOLC
#include <adolc/adolc.h>
#include <adolc/drivers/drivers.h>    // use of "Easy to Use" drivers
#include <adolc/taping.h>
#endif

#include <dune/common/fvector.hh>

/** \brief Namespace for methods that are needed by the automatic differentiation library Adol-C.*/
namespace Adolc {

/** \brief Abstract base class for a local energy function using the adouble coordinate type
 *          provided by the Adol-C library which allows to compute the exact linearization and
 *          hessian using the corresponding local assembler classes. The energy function is
 *          assumed to be of the form
 *
 *          \f[
 *              J:S(T)^d\longrightarrow\mathbb{R}
 *
 *              J(v) = \sum_{t\in T} J_t(v_t)
 *
 *          \f]
 *
 *          where
 *              \f$T\f$ - Grid
 *              \f$S(T)\f$ - Finite element space
 *              \f$d\f$ - Number of components
 *              \f$t\f$ - Element of the grid
 *              \f$J_t\f$ - Restriction of the energy to the element
 *              \f$v_t\f$ - Restriction of the finite element function to the element
 *
 *  The member 'energy' represents the evaluation of the energy functional restricted to an element,
 *  i.e. \f$J_t(v_t)\f$
 *
 *  \tparam GridType            The GridType
 *  \tparam LocalFiniteElement  Finite element that the energy is evaluated at.
 *  \tparam spacedim            Number of components of the solution space
 *
 *  If you want to use the automatic linearization and hessian assemblers, write a local energy
 *  that implements this class and use the corresponding assembler adolclinearizationassembler.hh
 *  or adolchessianassembler.hh
 *
 *  *Important* Make sure that you include this class before including any other headers so that
 *              the operations from adolcnamespacinjections.hh
 */
template <class GridType, class LocalFiniteElement, int spaceDim>
class LocalEnergy
{
    public:
        typedef typename GridType::template Codim<0>::Entity Element;
        typedef LocalFiniteElement Lfe;
        typedef Dune::FieldVector<adouble,spaceDim> TA;
        typedef std::vector<TA> CoefficientVectorType;
        typedef adouble ReturnType;

        /** \brief Evaluate the local energy.
         *
         *  \param element      Element on which the energy is evaluated.
         *  \param lfe          The local finite element of the discretized configuration.
         *  \param localCoeff   Coefficient vector corr. to the local finite element
         */
        virtual ReturnType energy(const Element& element, const LocalFiniteElement& lfe,
                            const CoefficientVectorType& localCoeff) const =0;
};

/** \brief Method to tape the energy evaluation, needed by Adol-C to perform automatic differentiation.*/
template <class GridType, class Lfe, int spaceDim>
static typename Lfe::Traits::LocalBasisType::Traits::RangeFieldType tapeEnergy(
        const typename GridType::template Codim<0>::Entity& element,
        const Lfe& localFiniteElement,
        const std::vector<Dune::FieldVector<typename Lfe::Traits::LocalBasisType::Traits::RangeFieldType,spaceDim> >& localCoeff,
        const LocalEnergy<GridType,Lfe,spaceDim>* localEnergy)
{
    typename Lfe::Traits::LocalBasisType::Traits::RangeFieldType pureEnergy;

    typedef LocalEnergy<GridType, Lfe, spaceDim> LocalEnergy;
    typename LocalEnergy::CoefficientVectorType localCoeffA(localCoeff.size());

    // tell ADOL-C where the functional is defined
    trace_on(1);

    adouble energy = 0;

    // initialize active variables of type adouble, this way ADOL-C w.r.t which
    // variables it should derive the functional
    for (size_t i=0; i<localCoeff.size(); i++)
        for (size_t j=0; j<localCoeff[i].size(); j++)
            localCoeffA[i][j] <<= localCoeff[i][j];

    energy = localEnergy->energy(element,localFiniteElement,localCoeffA);

    // set the dependent output variable
    energy >>= pureEnergy;

    // tell ADOL-C that the functional definition ends here
    trace_off();

    return pureEnergy;
}

}
#endif
