// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_OPERATOR_ASSEMBLER_HH
#define DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_OPERATOR_ASSEMBLER_HH

#include <array>
#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/staticmatrixtools.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Assemble the second derivative of the geometrically exact St.-Venant-Kirchhoff material
 *
 *  \f[
 *      J(u) = \int_{\Omega} E(u):C:E(u) dx
 *  \f]
 *
 *  which results in the bilinearform
 *
 *    \f[
 *      a_u(v,w) = \int_{\Omega} \sigma(u)E''_u(v,w) + (C:E'_u(v)):E'_u(w) dx
 *    \f]
 *   with
 *      - \f$D\varphi\f$: the deformation gradient
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$E'_u(v)\f$: the linearized strain tensor at the point u in direction v
 *      - \f$\sigma(u) = C:E(u)\f$: the second Piola-Kirchhoff stress tensor
 *      - \f$C\f$: the Hooke tensor
 *      - \f$Dv\f$: the gradient of the test function
 *
 */
template <class GridType, class TrialLocalFE, class AnsatzLocalFE>
class GeomExactStVenantKirchhoffOperatorAssembler
    : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, Dune::FieldMatrix<typename GridType::ctype ,GridType::dimension,GridType::dimension> >
{


    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    
    typedef Dune::VirtualFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<ctype,dim> > Function;
    typedef VirtualGridFunction<GridType, Dune::FieldVector<ctype,dim> > GridFunction;

public:
    typedef typename Dune::FieldMatrix<ctype,dim,dim> T;

    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >::Element Element;
    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE,T >::BoolMatrix BoolMatrix;
    typedef typename LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE,T >::LocalMatrix LocalMatrix;

   GeomExactStVenantKirchhoffOperatorAssembler(ctype E, ctype nu, std::shared_ptr<GridFunction> configuration)
        : E_(E), nu_(nu), configuration_(configuration)
    {}

    GeomExactStVenantKirchhoffOperatorAssembler(ctype E, ctype nu) :
        E_(E), nu_(nu)
    {}

    void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        isNonZero = true;
    }

    void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
        typedef Dune::FieldVector<ctype,dim> FVdim;
        typedef Dune::FieldMatrix<ctype,dim,dim> FMdimdim;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

        int rows = tFE.localBasis().size();
        int cols = aFE.localBasis().size();

        localMatrix.setSize(rows,cols);
        localMatrix = 0.0;

        // get quadrature rule
        const int order = (element.type().isSimplex()) 
            ? (tFE.localBasis().order())
            : (tFE.localBasis().order()*dim);
        const Dune::template QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<FVdim> gradients(tFE.localBasis().size());

        // store entity geometry mapping
        const typename Element::Geometry geometry = element.geometry();

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const FVdim& quadPos = quad[pt].position();

            // get transposed inverse of Jacobian of transformation
            const FMdimdim& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradients of base functions
            for (size_t i=0; i<gradients.size(); ++i) {

                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);

            }

            // evaluate displacement gradients the configuration at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (configuration_->isDefinedOn(element))
                configuration_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                configuration_->evaluateDerivative(geometry.global(quadPos),localConfGrad);

            // compute linear stress of nonlinear strain
            SymmetricTensor<dim> strain;
            computeStrain(localConfGrad,strain);
            SymmetricTensor<dim> stress = hookeTimesStrain(strain);

            // compute deformation gradient of the configuration
            for (int i=0;i<dim;i++)
                localConfGrad[i][i] += 1;                

            // /////////////////////////////////////////////
            //   Compute linearized strain for all shape functions
            // /////////////////////////////////////////////
            std::vector<std::array<FMdimdim, dim > > defGrad(rows);
            std::vector<std::array<SymmetricTensor<dim>,dim> > linStrain(rows);

            for (int i=0; i<rows; i++) {

                for (int k=0; k<dim; k++) {

                    // The deformation gradient of the shape function
                    defGrad[i][k] = 0;
                    defGrad[i][k][k] = gradients[i];

                    // The linearized strain is the symmetric product of defGrad and (Id+localConf)
                    linStrain[i][k]=symmetricProduct(defGrad[i][k],localConfGrad);
                }

            }


            // /////////////////////////////////////////////////
            //   Assemble matrix
            // /////////////////////////////////////////////////
            ctype z = quad[pt].weight() * integrationElement;
            for (int row=0; row<rows; row++)
                for (int rcomp=0; rcomp<dim; rcomp++) {

                    // compute Hooke times linearized strain
                    SymmetricTensor<dim> linStress = hookeTimesStrain(linStrain[row][rcomp]);

                    for (int col=0; col<cols; col++)
                        for (int ccomp=0; ccomp<dim; ccomp++) {
                            localMatrix[row][col][rcomp][ccomp] += (linStress*linStrain[col][ccomp])*z;
                            localMatrix[row][col][rcomp][ccomp] += (stress*symmetricProduct(defGrad[row][rcomp],defGrad[col][ccomp]))*z;

                        }
                }
        }
    }

    /** \brief Set new configuration to be assembled at. Needed e.g. during Newton iteration.*/
    void setConfiguration(std::shared_ptr<Function> newConfig) {
        configuration_ = std::dynamic_pointer_cast<GridFunction>(newConfig);
        if (!configuration_)
            DUNE_THROW(Dune::Exception,"In [GeomExactStVenantKirchhoffOperatorAssembler]: You need to provide a GridFunction describing the displacement!");
    }

protected:
    /** \brief Young's modulus */
    ctype E_;

    /** \brief The Poisson ratio */
    ctype nu_;

    /** \brief The configuration at which the linearized operator is evaluated.*/
    std::shared_ptr<GridFunction> configuration_;

    /** \brief Compute nonlinear strain tensor from the deformation gradient. */
    void computeStrain(const Dune::FieldMatrix<ctype, dim, dim>& grad, SymmetricTensor<dim>& strain) const {
        strain = 0;
        for (int i=0; i<dim ; ++i) {
            strain(i,i) +=grad[i][i];

            for (int k=0;k<dim;++k)
                strain(i,i) += 0.5*grad[k][i]*grad[k][i];

            for (int j=i+1; j<dim; ++j) {
                strain(i,j) += 0.5*(grad[i][j] + grad[j][i]);
                for (int k=0;k<dim;++k)
                    strain(i,j) += 0.5*grad[k][i]*grad[k][j];
            }
        }
    }

    /** \brief Compute the stress tensor from the nonlinear strain tensor. */
    SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {

        SymmetricTensor<dim> stress = strain;
        stress *= E_/(1+nu_);

        ctype f = E_*nu_ / ((1+nu_)*(1-2*nu_)) * strain.trace();

        stress.addToDiag(f);

        return stress;
    }

    /** \brief Compute the symmetric product of two matrices, i.e.
     *      0.5*(A^T * B + B^T * A )
     */
    SymmetricTensor<dim> symmetricProduct(const Dune::FieldMatrix<ctype, dim, dim>& a, const Dune::FieldMatrix<ctype, dim, dim>& b) const {

        SymmetricTensor<dim> result(0.0);
        for (int i=0;i<dim; i++)
            for (int j=i;j<dim;j++)
                for (int k=0;k<dim;k++)
                    result(i,j)+= a[k][i]*b[k][j]+b[k][i]*a[k][j];   
        result *= 0.5;                
        
        return result;
    }

};


#endif

