// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef WEIGHTED_MASS_ASSEMBLER_HH
#define WEIGHTED_MASS_ASSEMBLER_HH

#include <memory>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/addtodiagonal.hh>

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include "dune/fufem/assemblers/localoperatorassembler.hh"

#include "dune/fufem/functions/analyticgridfunction.hh"

/**
 * \brief Weighted local mass assembler
 *
 * This local operator assembler allows to assemble the bilinear form
 * \f[
 *   b(u,v) = \int_\Omega f(x) u(x) v(x)\,dx
 * \f]
 * for a fixed weighting function \f$ f:\Omega \to \mathbb{R} \f$.
 **/
template <class GridType, class TrialLocalFE, class AnsatzLocalFE, class FunctionType, class T=Dune::FieldMatrix<double,1,1> >
class WeightedMassAssembler : public LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE, T >
{
    private:
        typedef typename FunctionType::RangeType RangeType;
        typedef VirtualGridFunction<GridType, RangeType> GridFunctionType;

        typedef LocalOperatorAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
        static const int dim = GridType::dimension;


    public:
        typedef typename Base::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        /**
         * \brief Construct WeightedMassAssembler
         *
         * \param grid
         */
        WeightedMassAssembler(const GridType& grid, const FunctionType& weight, QuadratureRuleKey weightQuadKey):
            weightGridFunctionP_(makeGridFunction(grid, weight)),
            weightQuadKey_(weightQuadKey)
        {}

        void indices(const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            isNonZero = true;
        }

        void assemble(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE) const
        {
            typedef typename Dune::template FieldVector<double,dim> FVdim;
            typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // check if ansatz local fe = test local fe
            if (not Base::isSameFE(tFE, aFE))
                DUNE_THROW(Dune::NotImplemented, "WeightedMassAssembler is only implemented for ansatz space=test space!");

            int rows = localMatrix.N();
            int cols = localMatrix.M();

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localMatrix = 0.0;

            // get quadrature rule
            QuadratureRuleKey quadKey = QuadratureRuleKey(weightQuadKey_)
                .product(QuadratureRuleKey(tFE))
                .product(QuadratureRuleKey(aFE));
            const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store values of shape functions
            std::vector<RangeType> values(tFE.localBasis().size());

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const FVdim& quadPos = quad[pt].position();

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // evaluate basis functions
                tFE.localBasis().evaluateFunction(quadPos, values);

                RangeType z;
                weightGridFunctionP_->evaluateLocal(element, quadPos, z);

                // compute matrix entries
                z *= quad[pt].weight() * integrationElement;
                for(int i=0; i<rows; ++i)
                {
                    double zi = values[i]*z;

                    for (int j=i+1; j<cols; ++j)
                    {
                        double zij = values[j] * zi;
                        Dune::MatrixVector::addToDiagonal(localMatrix[i][j],zij);
                        Dune::MatrixVector::addToDiagonal(localMatrix[j][i],zij);
                    }
                    Dune::MatrixVector::addToDiagonal(localMatrix[i][i], values[i] * zi);
                }
            }
        }

    protected:
        std::shared_ptr<const GridFunctionType> weightGridFunctionP_;
        QuadratureRuleKey weightQuadKey_;
};


#endif

