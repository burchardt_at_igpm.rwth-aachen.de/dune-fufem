// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_FUNCTIONAL_ASSEMBLER_HH
#define DUNE_FUFEM_LOCAL_ASSEMBLERS_GEOMETRICALLY_EXACT_ST_VENANT_KIRCHHOFF_FUNCTIONAL_ASSEMBLER_HH

#include <array>
#include <memory>

#include <dune/common/fvector.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/symmetrictensor.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>

/** \brief Local assembler of the first derivative of the geometrically exact St.Venant material at a displacement u
 *
 *  \f[
 *      J(u) = \int_{\Omega} E(u):C:E(u) dx
 *  \f]
 *
 *  which results in the linearform
 *
 *    \f[
 *      l_u(v) := \int_{\Omega} D\varphi(u)\sigma(u):Dv dx = \int_{\Omega}\sigma(u): E'_u (v) dx
 *    \f]
 *   with
 *      - \f$D\varphi\f$: the deformation gradient
 *      - \f$E\f$: the nonlinear strain tensor
 *      - \f$E'_u(v)\f$: the linearized strain tensor at the point u in direction v
 *      - \f$\sigma(u) = C:E(u)\f$: the second Piola-Kirchhoff stress tensor
 *      - \f$C\f$: the Hooke tensor
 *      - \f$Dv\f$: the gradient of the test function
*/ 
template <class GridType, class TrialLocalFE>
class GeomExactStVenantKirchhoffFunctionalAssembler :
    public LocalFunctionalAssembler<GridType, TrialLocalFE, Dune::FieldVector<typename GridType::ctype ,GridType::dimension> >

{
    static const int dim = GridType::dimension;
    typedef typename GridType::ctype ctype;
    typedef typename Dune::FieldVector<ctype,dim> T;
    typedef LocalFunctionalAssembler<GridType, TrialLocalFE, T> Base;

public:
    typedef typename Base::Element Element;
    typedef typename Base::LocalVector LocalVector;
    typedef Dune::VirtualFunction<T, T> Function;
    typedef VirtualGridFunction<GridType, T> GridFunction;

     //! Constructor
    GeomExactStVenantKirchhoffFunctionalAssembler(ctype E,ctype nu, std::shared_ptr<GridFunction> configuration)
       : E_(E), nu_(nu),
        configuration_(configuration)
    {}


    //! Constructor
    GeomExactStVenantKirchhoffFunctionalAssembler(ctype E, ctype nu) :
        E_(E), nu_(nu), configuration_(NULL)
    {}

    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    { 

        typedef typename Dune::FieldMatrix<ctype,dim,dim> FMdimdim;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;    
        typedef typename Element::Geometry::LocalCoordinate LocalCoordinate; 
        int rows = tFE.localBasis().size();
        localVector = 0.0;

        // get quadrature rule
        const int order = (element.type().isSimplex()) 
            ? (tFE.localBasis().order())
            : (tFE.localBasis().order()*dim);

        // get quadrature rule
        const Dune::template QuadratureRule<ctype, dim>& quad = QuadratureRuleCache<ctype, dim>::rule(element.type(), order, IsRefinedLocalFiniteElement<TrialLocalFE>::value(tFE) );

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
        std::vector<T> gradients(tFE.localBasis().size());

        // store geometry mapping of the entity
        const typename Element::Geometry geometry = element.geometry();

        // loop over quadrature points
        for (size_t pt=0; pt < quad.size(); ++pt) {

            // get quadrature point
            const LocalCoordinate& quadPos = quad[pt].position();

            // get transposed inverse of Jacobian of transformation
            const FMdimdim& invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const ctype integrationElement = geometry.integrationElement(quadPos);

            // get gradients of shape functions
            tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // compute gradients of base functions
            for (size_t i=0; i<gradients.size(); ++i) {

                // transform gradients
                gradients[i] = 0.0;
                invJacobian.umv(referenceGradients[i][0], gradients[i]);
            }

            // evaluate the displacement gradients of the configuration at the quadrature point
            typename GridFunction::DerivativeType localConfGrad;
            if (configuration_->isDefinedOn(element))
                configuration_->evaluateDerivativeLocal(element, quadPos, localConfGrad);
            else
                configuration_->evaluateDerivative(geometry.global(quadPos),localConfGrad);


            // /////////////////////////////////////////////
            //   Compute stress of configuration at quad point
            // /////////////////////////////////////////////

            /* Compute the nonlinear strain tensor from the displacement gradient*/
            SymmetricTensor<dim> strain;
            computeStrain(localConfGrad,strain);

            // and the stress
            SymmetricTensor<dim> stress = hookeTimesStrain(strain);

            // make deformation gradient out of the discplacement
            for (int i=0;i<dim;i++)
                localConfGrad[i][i] += 1;        

            //compute linearized strain for all shapefunctions
            std::vector<std::array<SymmetricTensor<dim>,dim> > linStrain(rows);

            for (int i=0; i<rows; i++)
                for (int k=0; k<dim; k++) {

                    // The deformation gradient of the shape function
                    FMdimdim deformationGradient(0);
                    deformationGradient[k] = gradients[i];

                    // The linearized strain is the symmetric product of defGrad and (Id+localConf)
                    linStrain[i][k]=symmetricProduct(deformationGradient,localConfGrad);
                }

            ctype z = quad[pt].weight()*integrationElement;

            // add vector entries
            for(int row=0; row<rows; row++) 
                for (int rcomp=0;rcomp<dim;rcomp++)
                    localVector[row][rcomp] +=(stress*linStrain[row][rcomp])*z;

        }
        return;
    }

    /** \brief Set new configuration. In Newton iterations this needs to be assembled more than one time.*/
    void setConfiguration(std::shared_ptr<Function> newConfig) {
        configuration_ = std::dynamic_pointer_cast<GridFunction>(newConfig);

        if (!configuration_)
            DUNE_THROW(Dune::Exception,"In [GeomExactStVenantKirchhoffFunctionalAssembler]: GridFunction cast failed!");
    }

protected:
    /** \brief Young's modulus */
    ctype E_;

    /** \brief The Poisson ratio */
    ctype nu_;

    /** \brief The configuration at which the functional is evaluated.*/
    std::shared_ptr<GridFunction> configuration_;

    /** \brief Compute nonlinear strain tensor from the deformation gradient. */
    void computeStrain(const Dune::FieldMatrix<ctype, dim, dim>& grad, SymmetricTensor<dim>& strain) const {
        strain = 0;
        for (int i=0; i<dim ; ++i) {
            strain(i,i) +=grad[i][i];

            for (int k=0;k<dim;++k)
                strain(i,i) += 0.5*grad[k][i]*grad[k][i];

            for (int j=i+1; j<dim; ++j) {
                strain(i,j) += 0.5*(grad[i][j] + grad[j][i]);
                for (int k=0;k<dim;++k)
                    strain(i,j) += 0.5*grad[k][i]*grad[k][j];
            }
        }
    }

    /** \brief Compute the stress tensor from the nonlinear strain tensor. */
    SymmetricTensor<dim> hookeTimesStrain(const SymmetricTensor<dim>& strain) const {

        SymmetricTensor<dim> stress = strain;
        stress *= E_/(1+nu_);

        ctype f = E_*nu_ / ((1+nu_)*(1-2*nu_)) * strain.trace();

        stress.addToDiag(f);

        return stress;
    }

    /** \brief Compute the symmetric product of two matrices, i.e.
     *      0.5*(A^T * B + B^T * A )
     */
    SymmetricTensor<dim> symmetricProduct(const Dune::FieldMatrix<ctype, dim, dim>& a, const Dune::FieldMatrix<ctype, dim, dim>& b) const {

        SymmetricTensor<dim> result(0.0);
        for (int i=0;i<dim; i++)
            for (int j=i;j<dim;j++)
                for (int k=0;k<dim;k++)
                    result(i,j)+= a[k][i]*b[k][j]+b[k][i]*a[k][j];   
        result *= 0.5;                
        
        return result;
    }
};

#endif


