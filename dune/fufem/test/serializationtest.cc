#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#ifdef HAVE_BOOST_SERIALIZATION

#include <dune/fufem/dataio.hh>
#include <dune/fufem/dunedataio.hh>

#endif

using namespace Dune;

template<int n, int m>
class Data
{
    public:

        Data(int k1, int k2):
            x(k1),
            y(k2)
        {
            for (size_t i = 0; i < x.size(); ++i)
                x[i] = i;
            for (size_t i = 0; i < y.size(); ++i)
                y[i] = 2*i;
        }

        template <class Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar & x;
            ar & y;
        }

        bool equals(Data& other) const
        {
            if ((x.size() != other.x.size()) or (y.size() != other.y.size()))
                return false;

            for (size_t i = 0; i < x.size(); ++i)
                if (x[i] != other.x[i])
                    return false;

            for (size_t i = 0; i < y.size(); ++i)
                if (y[i] != other.y[i])
                    return false;
            return true;
        }

    private:
        Dune::BlockVector<Dune::FieldVector<double,n> > x;
        Dune::BlockVector<Dune::FieldVector<int,m> > y;
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

#ifdef HAVE_BOOST_SERIALIZATION

    Data<7,5> data(123, 456);

    DataIO::writeData(data, "dataiotest");

    Data<7,5> readData(1, 1);

    DataIO::loadData(readData, "dataiotest");

    if (not(data.equals(readData)))
        DUNE_THROW(Dune::Exception, "Read data is not equal to written data!");
#else
    std::cout << "Nothing is tested since boost serialization was not found!" << std::endl;
    return 77;
#endif
}
