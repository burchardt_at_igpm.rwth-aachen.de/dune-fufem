#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/stringutility.hh>

#include <dune/istl/bcrsmatrix.hh>

#include <dune/geometry/virtualrefinement.hh>

#include <dune/fufem/assemblers/transferoperatorassembler.hh>
#include <dune/fufem/utilities/gridconstruction.hh>
#include <dune/fufem/test/common.hh>

using namespace Dune;

constexpr size_t level = 3;

struct TestSuite {

  template <class Grid>
  bool check(const Grid& grid) const {
    try {
      checkDimension(grid);
    } catch (const Exception& e) {
      std::cout << e << std::endl;
      return false;
    }
    return true;
  }

  template <class Grid>
  bool checkDimension(const Grid& grid) const {
    using TOA = TransferOperatorAssembler<Grid>;
    using Matrix = BCRSMatrix<FieldMatrix<double, 1, 1>>;
    using Hierarchy = std::vector<std::shared_ptr<Matrix>>;
    Hierarchy hierarchy;
    TOA toa(grid);
    toa.assembleMatrixHierarchy(hierarchy);

    constexpr size_t dim = Grid::dimension;
    GeometryType simplex(GeometryTypes::simplex(dim));
    auto& r = buildRefinement<dim, double>(simplex, simplex);
    for (size_t i = 0; i < level; ++i) {
      const auto& m = hierarchy[i];
      size_t M = r.nVertices(refinementLevels(i));
      size_t N = r.nVertices(refinementLevels(i + 1));
      if (m->M() != M || m->N() != N) {
        std::string error_msg =
            "Transfer dimensions of level " + std::to_string(i) + " are " +
            std::to_string(m->M()) + "," + std::to_string(m->N()) +
            " but expected was " + std::to_string(M) + "," + std::to_string(N);
        DUNE_THROW(Exception, error_msg);
        return false;
      }
    }
    return true;
  }
};

template <size_t D>
using MyALUGrid = Dune::ALUGrid<D, D, ALUGridElementType::simplex,
                                ALUGridRefinementType::nonconforming>;

template <size_t D>
bool testDim() {
  bool passed = true;

  TestSuite suite;
  passed &= checkWithAdaptiveGrid<Dune::UGGrid<D>>(suite, level, 0);
//  passed &= checkWithAdaptiveGrid<MyALUGrid<D>>(suite, level, 0);

#if HAVE_DUNE_SUBGRID
  passed &= checkWithSubGrid<Dune::UGGrid<D>>(suite, level, 0, Dune::formatString("SubGrid<%d,UGGrid<%d>>", D, D), false);
  passed &= checkWithSubGrid<MyALUGrid<D>>(suite, level, 0, Dune::formatString("SubGrid<%d,ALUGrid<...>>", D), false);
#endif
  return passed;
}

int main(int argc, char* argv[]) {
  using namespace Dune;
  using namespace Hybrid;
  MPIHelper::instance(argc, argv);

  bool passed = true;
  passed = passed && testDim<2>();
  passed = passed && testDim<3>();

  return passed ? 0 : 1;
}
