#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/h1functionalassembler.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include "common.hh"

/** \brief Wrapper for (Generic)GridFunctions representing the GridFunction's derivative
 *
 */
template <class GridViewType, class GridFunctionType>
class GridFunctionDerivative:
    public VirtualGridViewFunction<GridViewType,typename GridFunctionType::DerivativeType>
{
        typedef VirtualGridViewFunction<GridViewType,typename GridFunctionType::DerivativeType> BaseType;
        typedef typename GridViewType::template Codim<0>::Entity Element;

        GridFunctionType& gridfunction_;

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;

        GridFunctionDerivative(const GridViewType& gridview, GridFunctionType& gridfunction):
            VirtualGridViewFunction<GridViewType, RangeType>(gridview),
            gridfunction_(gridfunction)
        {}

        virtual void evaluateLocal(const Element& e, const DomainType& x, RangeType& y) const
        {
            y=0.0;

            gridfunction_.evaluateDerivativeLocal(e,x,y);
        }

        virtual void evaluateDerivativeLocal(const Element& e, const DomainType& x, DerivativeType& d) const
        {
            DUNE_THROW(Dune::NotImplemented,"DerivativeLocal not implemented for GridFunctionDerivative");
        }

};

/** \brief TestSuite for H1FunctionalAssembler
 *
 *  This TestSuite tests for consistency of the H1FunctionalAssembler with the LaplaceAssembler, i.e. it wraps canonical
 *  P1BasisFunctions such that the wrapper returns the Derivative and plugs this wrapper into the H1FunctionalAssembler.
 *  Values should then correspond to those in the discrete Laplacian.
 */
struct H1FunctionalAssemblerTestSuite
{
    template <typename GridType>
    bool check(const GridType& grid)
    {
        typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<typename GridType::LeafGridView>> Basis;
        typedef typename Basis::LocalFiniteElement FE;

        const int block_size=2;

        Basis basis(grid.leafGridView());

        /* the canonical basis functions and its derivative */
        typedef typename Dune::BlockVector<Dune::FieldVector<double,1> > ScalarCoefficients;
        typedef typename Dune::BlockVector<Dune::FieldVector<double,block_size> > VectorCoefficients;

        typedef BasisGridFunction<Basis, ScalarCoefficients> ScalarGridFunction;
        typedef BasisGridFunction<Basis, VectorCoefficients> VectorGridFunction;

        ScalarCoefficients coeffs(basis.size());
        VectorCoefficients coeffsVec(basis.size());
        coeffs = 0.0;
        coeffsVec = 0.0;

        ScalarGridFunction scalar_basis_function(basis, coeffs);
        VectorGridFunction vector_basis_function(basis, coeffsVec);
        GridFunctionDerivative<typename GridType::LeafGridView, ScalarGridFunction> scalar_basis_function_derivative(grid.leafGridView(),scalar_basis_function);
        GridFunctionDerivative<typename GridType::LeafGridView, VectorGridFunction> vector_basis_function_derivative(grid.leafGridView(),vector_basis_function);

        /* containers for stiffness matrix and assembled functional */
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat;
        Dune::BlockVector<Dune::FieldVector<double,1> > f_i(basis.size());
        Dune::BlockVector<Dune::FieldVector<double,block_size> > g_i(basis.size());

        /* create assemblers and assemble Laplacian */
        Assembler<Basis, Basis> assembler(basis, basis);
        LaplaceAssembler<GridType,FE,FE> laplaceAssembler;
        assembler.assembleOperator(laplaceAssembler, stiff_mat);

        H1FunctionalAssembler<GridType,FE> h1functionalassembler(scalar_basis_function_derivative);
        H1FunctionalAssembler<GridType, FE,Dune::FieldVector<double,block_size> > h1functionalassemblerVec(vector_basis_function_derivative);

//        for (size_t i=0; i<basis.size(); ++i)
        for (size_t i=0; i<basis.size(); i+=20)
        {
            /* set canonical basis function */
            coeffs = 0.0;
            coeffs[i] = 1.0;
            coeffsVec = 0.0;
            coeffsVec[i] = 1.0;

            /* assemble corresponding h1 functional */
            assembler.assembleFunctional(h1functionalassembler, f_i);
            assembler.assembleFunctional(h1functionalassemblerVec, g_i);

            for (size_t j=0; j<basis.size(); ++j)
            {
                if ((stiff_mat.exists(i,j) && std::abs(stiff_mat[i][j]-f_i[j])>1e-14) or (not(stiff_mat.exists(i,j)) && std::abs(f_i[j])>1e-14))
                {
                    std::cout << "stiff_mat(" << i << ", " << j << ")= ";
                    if (stiff_mat.exists(i,j))
                        std::cout << stiff_mat[i][j];
                    else
                        std::cout <<  0.0;
                    std::cout << "  f_" << i << "(" << j << ")= " << f_i[j] << std::endl;
                    return false;
                }
                for (int k=0; k<block_size; ++k)
                {
                    if ((stiff_mat.exists(i,j) && std::abs(stiff_mat[i][j]-g_i[j][k])>1e-14) or (not(stiff_mat.exists(i,j)) && std::abs(g_i[j][k])>1e-14))
                    {
                        std::cout << "stiff_mat(" << i << ", " << j << ")= ";
                        if (stiff_mat.exists(i,j))
                            std::cout << stiff_mat[i][j];
                        else
                            std::cout <<  0.0;
                        std::cout << "  g_" << i << "(" << j << ")[" << k << "]= " << g_i[j][k] <<  "  f_" << i << "(" << j << ")= " << f_i[j] << std::endl;
                        std::cout << std::endl << "H1FunctionalAssemblerTest failed" << std::endl << std::endl;
                        return false;
                    }
                }
            }

        }

        return true;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the H1FunctionalAssemblerTest" << std::endl;

    H1FunctionalAssemblerTestSuite tests;

    bool passed = true;

    passed = checkWithStandardAdaptiveGrids(tests);


    return passed ? 0 : 1;

}
