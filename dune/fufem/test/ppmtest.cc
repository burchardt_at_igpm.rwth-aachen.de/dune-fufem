#include <config.h>

#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/timer.hh>
#include <dune/istl/bvector.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/portablepixelmap.hh>


// This test tests the ColorMap and PortablePixelMap classes by
// * creating a ColorMap from single keys
// * writing the ColorMap to files in the .cm and AmiraMesh formats
// * rereading colormaps from the above files and comparing to the original
// * creating a GridFunction and exporting it to a pgm-file (in binary as well as ascii)
// * reading the above pgm-files into PortablePixelMap-Objects (internally a GridFunction)
// * checking the two for equal coefficient vectors (this can only work for grids with 2^n+1
//   nodes in each dimension, because when reading a pgm, for efficiency reasons, the underlying 
//   grid will have such a resolution)
//
// This is tested on YaspGrid only because
// * it is onboard in the dune-grid core module
// * PortablePixelMap uses a YaspGrid internally. Hence comparison is made easy.
//
// This test does NOT test all of ColorMap's resp. PortablePixelMap's functionalities. Actually it doesn't really test any.
// Only consistency of the read and export routines is tested.


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is PPMTest v0.02" << std::endl;
    typedef Dune::YaspGrid<2> GridType;
    typedef Dune::FieldVector<double, 1> RangeType;
    typedef Q1NodalBasis<GridType::LeafGridView> BasisType;
    typedef BasisGridFunction<BasisType, Dune::BlockVector<RangeType> > FunctionType;

    int n = 16;

    Dune::FieldVector<double,2> L(n-1);
    std::array<int,2> s = {{n-1, n-1}};

    GridType grid(L,s);
    BasisType basis(grid.leafGridView());

    /* create GridFunction */
    Dune::BlockVector<RangeType> disc_function_ori(basis.size());

    for (size_t i=0; i < disc_function_ori.size(); ++i)
        disc_function_ori[i] = i;

    FunctionType function(basis, disc_function_ori);

    /* construct colormap from single keys */
    ColorMap colormap;
    ColorMap::Color color(0);
    std::cout << "inserting keys in colorkey...";
    color[0] = 255;
    colormap.insertInColorKey(0,color);
    std::cout << "1...";
    color[0] = 0;
    color[1] = 255;
    colormap.insertInColorKey(127,color);
    std::cout << "2...";
    color[1] = 0;
    color[2] = 255;
    colormap.insertInColorKey(255,color);
    std::cout << "3...";
    std::cout << "done!" << std::endl;

    std::cout << "exporting colormaps...";
    std::string filename_cm("colormap.cm");
    std::string filename_am("colormap.am");
    colormap.exportColorMap(filename_cm,ColorMap::CM);
    colormap.exportColorMap(filename_am,ColorMap::AmiraMesh);
    std::cout << "done!" << std::endl;

    std::cout << "reading colormaps...";
    ColorMap cmap_cm(filename_cm);
    ColorMap cmap_am(filename_am);
    std::cout << "done!" << std::endl;

    std::cout << "checking colormaps...";
    for (int i=0; i<256; ++i)
    {
        if (colormap.map(0,255,i)!=cmap_cm.map(0,255,i))
        {
            char err[1024];
            ColorMap::Color col1= colormap.map(0,255,i),
                            col2= cmap_cm.map(0,255,i);

            sprintf(err,"Value mismatch original vs reconstructed from CM at key %d original->(%d %d %d) recon->(%d %d %d)", i, int(col1[0]), int(col1[1]), int(col1[2]), int(col2[0]),int(col2[1]),int(col2[2]));
            remove(filename_am.c_str());
            remove(filename_cm.c_str());
            DUNE_THROW(Dune::Exception, err);
        }
        if (colormap.map(0,255,i)!=cmap_am.map(0,255,i))
        {
            char err[1024];
            ColorMap::Color col1= colormap.map(0,255,i),
                            col2= cmap_am.map(0,255,i);

            sprintf(err,"Value mismatch original vs reconstructed from AmiraMesh at key %d original->(%d %d %d) recon->(%d %d %d)", i, int(col1[0]), int(col1[1]), int(col1[2]), int(col2[0]),int(col2[1]),int(col2[2]));
            remove(filename_am.c_str());
            remove(filename_cm.c_str());
            DUNE_THROW(Dune::Exception, err);
        }
    }
    std::cout << "done!" << std::endl;

    remove(filename_am.c_str());
    remove(filename_cm.c_str());

    /* export GridFunction to ppm */
    std::string filename_ascii("ppmtest_ascii.ppm");
    std::string filename_bin("ppmtest_bin.ppm");
    std::cout << "exporting pixelmaps...";
    PortablePixelMap::exportPixelMap(filename_ascii.c_str(), function, 0, basis.size()-1, 0, L[0], 0, L[1], n, n, "pgmtest", colormap, PortablePixelMap::ASCII);
    PortablePixelMap::exportPixelMap(filename_bin.c_str(), function, 0, basis.size()-1, 0, L[0], 0, L[1], n, n, "pgmtest", colormap, PortablePixelMap::Bin);
    std::cout << "done!" << std::endl;

    /* read ppm to PortablePixelMap */
    std::cout << "reading pixelmaps...";
    PortablePixelMap ppm_ascii(0,basis.size()-1,colormap);
    ppm_ascii.readPixelMap(filename_ascii.c_str());
    PortablePixelMap ppm_bin(0,basis.size()-1,colormap);
    ppm_bin.readPixelMap(filename_bin.c_str());
    std::cout << "done!" << std::endl;

    /* translate PortablePixelMap to actual function */
    Dune::BlockVector<RangeType> disc_function_recon_a(basis.size());
    Dune::BlockVector<RangeType> disc_function_recon_b(basis.size());

    typedef GridType::LeafGridView::Codim<GridType::dimension>::Iterator NodeIterator;
    NodeIterator node = grid.leafGridView().begin<GridType::dimension>();
    NodeIterator node_end = grid.leafGridView().end<GridType::dimension>();

    for (; node != node_end; ++node)
    {
        ppm_ascii.evaluate(node->geometry().corner(0),disc_function_recon_a[grid.leafGridView().indexSet().index<GridType::dimension>(*node)]);
        ppm_bin.evaluate(node->geometry().corner(0),disc_function_recon_b[grid.leafGridView().indexSet().index<GridType::dimension>(*node)]);
    }


    std::cout << "checking pixmaps for consistency...";
    /* check for consistency */
    for (size_t i = 0; i<disc_function_ori.size(); ++i)
        if (disc_function_ori[i] != std::floor(disc_function_recon_a[i]+0.5) and disc_function_ori[i] != std::floor(disc_function_recon_b[i]+0.5))
        {
            std::cout << "original:\n" << disc_function_ori[i] << std::endl;
            std::cout << "reconstructed from ascii file:\n" << disc_function_recon_a[i] << std::endl;
            std::cout << "reconstructed from binary file:\n" << disc_function_recon_b[i] << std::endl;
            remove(filename_ascii.c_str());
            remove(filename_bin.c_str());
            DUNE_THROW(Dune::Exception, "Original and reconstructed function values don't match.\n This might not necessarily indicate test failure, as writing/reading of PixelMaps from/to scalar functions is not quite invertible. If n==2^k, k<9 this message should not have popped up, however.");
        }
    std::cout << "done!" << std::endl;

    /* delete the PPM-file again */
    remove(filename_ascii.c_str());
    remove(filename_bin.c_str());

    return 0;
}
