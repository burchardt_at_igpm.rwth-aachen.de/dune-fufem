#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>

#include <dune/fufem/functions/polynomial.hh>

#include "common.hh"

/** \brief TestSuite for Polynomial
 *
 *  + compares Polynomial::evaluate to manual evaluation of polynomial (hm, well...)
 *  + compares Polynomial::evaluateDerivative to manual evaluateDerivative of derivative of polynomial (hm, well)
 *  + checks consistency of Polynomial::evaluateDerivative() and Polynomial::D().evaluate()
 *
 */
struct PolynomialTestSuite
{
    bool check()
    {

        const int n_testpoints=10,
                  n_testpolynomials = 5,
                  maxdegree = 10,
                  maxcoeff = 100;

        typedef Dune::FieldVector<double,1> DomainType;
        typedef Dune::FieldVector<double,1> RangeType;

        typedef Polynomial<DomainType,RangeType> PType;
        typedef PType::CoeffType CoeffType;
        typedef PType::DerivativeType DerivativeType;

        for (int p=0; p<n_testpolynomials; ++p)
        {
            CoeffType coeffs((rand() % maxdegree)  + 2);
            std::cout << p << ": coeffs= ";
            for (size_t c=0; c<coeffs.size(); ++c)
            {
                coeffs[c] = (2.0*maxcoeff*rand())/RAND_MAX - maxcoeff;
                std::cout << coeffs[c] << " " ;
            }
            std::cout << std::endl;

            PType polynomial(coeffs);

            DomainType x(0.0);
            RangeType  y(0.0),
                       ycheck(0.0),
                       Dcheck(0.0);
            DerivativeType d(0.0),
                           dcheck(0.0);

            for (int run=0; run<n_testpoints; ++run)
            {
                x = (4.0*rand())/RAND_MAX - 2.0;

                ycheck = 0.0;
                for (size_t j=0; j<coeffs.size(); ++j)
                    ycheck += coeffs[j]*std::pow(x,j);

                polynomial.evaluate(x,y);

                if (std::abs((y-ycheck)/y)>1e-12)
                {
                    std::cout << "Polynomial::evaluate(" << x << ") = " << y << std::endl;
                    std::cout << "manual evaluation: " << ycheck << std::endl;
                    return false;
                }

                dcheck = 0.0;
                for (size_t j=1; j<coeffs.size(); ++j)
                    dcheck += coeffs[j]*j*std::pow(x,j-1);

                polynomial.evaluateDerivative(x,d);
                polynomial.D(1).evaluate(x,Dcheck);

                if (std::abs((d-dcheck)/d)>1e-12)
                {
                    std::cout << "Polynomial::evaluateDerivative(" << x << ") = " << d << std::endl;
                    std::cout << "manual derivative evaluation: " << dcheck << std::endl;
                    return false;
                }
                if (std::abs((d-Dcheck)/d)>1e-12)
                {
                    std::cout << "Polynomial::evaluateDerivative(" << x << ") = " << d << std::endl;
                    std::cout << "Polynomial::D().evaluate(" << x << ") = " << Dcheck << std::endl;
                    return false;
                }

            }
        }
        return true;
    }
};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the PolynomialTest" << std::endl;

    PolynomialTestSuite tests;

    bool passed = tests.check();
    if (passed)
        std::cout << "Polynomial test passed" << std::endl;
    else
        std::cout << "Polynomial test failed" << std::endl;

    return passed ? 0 : 1;

}
