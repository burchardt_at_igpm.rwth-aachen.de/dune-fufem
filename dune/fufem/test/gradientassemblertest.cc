#include <config.h>

#include <cstdio>
#include <iomanip>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/gradientassembler.hh>
#include <dune/fufem/functiontools/gradientbasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include "common.hh"



/** \brief TestSuite for GradientAssembler
 *
 *  This TestSuite tests for consistency of the GradientAssembler with BasisGridFunction<P1NodalBasis>::evaluateDerivative, i.e. whether for the assembled operator \f$B\in\mathbb R^{m\times n}\ BU=(\nabla u|_\mathcal T_i)_{i=1}^m\f$ where
 *  \f$m=\f$#elements and \f$n=\f$#nodes, holds.
 */
struct GradientAssemblerTestSuite
{
    template <typename GridType>
    bool check(const GridType& grid)
    {
        const int dim = GridType::dimension;

        typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<typename GridType::LeafGridView>> Basis;
        typedef typename Basis::LocalFiniteElement FE;

        Basis basis(grid.leafGridView());

        typedef Dune::BlockVector<Dune::FieldVector<double,1> > ScalarVector;
        typedef Dune::BlockVector<Dune::FieldVector<double,dim> > BlockedVector;

        ScalarVector U(basis.size());

        typedef BasisGridFunction<Basis,ScalarVector> DiscFunction;
        DiscFunction u(basis, U);

        for (size_t i=0; i<basis.size(); ++i)
        {
            U[i] = (10.0*rand())/RAND_MAX;
        }

        typedef Dune::BCRSMatrix< Dune::FieldMatrix< double, dim, 1 > > GradientMatrix;
        typedef typename GradientBasis<Basis>::type GBasis;
        typedef typename GBasis::LocalFiniteElement GradientFE;
        typedef typename Basis::LocalFiniteElement FE;

        GradientMatrix grad_mat;

        GBasis gradientBasis(basis.getGridView());

        Assembler<GBasis, Basis> assembler(gradientBasis, basis);
        GradientAssembler<GridType, GradientFE, FE> gradientAssembler;

        assembler.assembleOperator(gradientAssembler, grad_mat);

        BlockedVector DUa(gradientBasis.size()), DUb(gradientBasis.size());
        DUb = DUa = 0.0;

//        std::cout << "# of nodes:    " << grid.size(dim) << std::endl;
//        std::cout << "# of elements: " << grid.size(0) << std::endl;
//        std::cout << "basis.size = " << basis.size() << std::endl;
//        std::cout << "gradientBasis.size = " << gradientBasis.size() << std::endl;
//        std::cout << "grad_mat.N() = " << grad_mat.N() << std::endl;
//        std::cout << "grad_mat.M() = " << grad_mat.M() << std::endl;
//        std::cout << "U.size = " << U.size() << std::endl;

        grad_mat.mv(U,DUa);

        for (const auto& element : elements(basis.getGridView()))
        {
            unsigned int idx=grid.leafGridView().indexSet().template index<0>(element);
            typename DiscFunction::DerivativeType DUlocal;
            u.evaluateDerivativeLocal(element,Dune::FieldVector<double,dim>(1.0/3.0),DUlocal);
            for (int i=0; i<dim; ++i)
                DUb[idx][i] = DUlocal[0][i];
        }

        for (size_t i=0; i<gradientBasis.size(); ++i)
        {
            typename BlockedVector::block_type DUi=DUa[i];
            DUi -= DUb[i];
            if (DUi.two_norm()>1e-8)
            {
                std::cout << std::setprecision(15) << "DUa[" << i << "]= " << DUa[i] << " != " << DUb[i] << " = DUb[" << i << "]" << std::endl;
                return false;
            }
        }

        return true;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the GradientAssemblerTest" << std::endl;

    GradientAssemblerTestSuite tests;

    bool passed = true;

    passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;

}
