#include <config.h>

#ifdef HAVE_PYTHON
#include <Python.h>
#endif

#include <map>
#include <memory>
#include <vector>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>
#include <dune/common/parametertree.hh>

#if HAVE_UG
#include <dune/grid/uggrid.hh>
#include <dune/grid/uggrid/uggridfactory.hh>
#include <dune/grid/common/gridfactory.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>
#endif

#include <dune/functions/common/differentiablefunctionfromcallables.hh>

#include <dune/fufem/dunepython.hh>



int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    // Use must start the python interpreter first
    Python::start();

    // You will normally want to execute code in the context of the main
    // module denoted __main__ inside of python.  For using other modules
    // see below.
    auto pyMain = Python::main();


    // In order to run python scripts from in the working
    // directory you have to add it to the search path.
    pyMain.runStream()
        << std::endl << "import sys"
        << std::endl << "sys.path.append('.')"
        << std::endl;

    {
        pyMain.runStream()
            << std::endl << "def f():"
            << std::endl << "    return 'f'"
            << std::endl << ""
            << std::endl << "def g(ff):"
            << std::endl << "    return ff()"
            << std::endl;
    }


    std::cout << std::endl;
    std::cout << "Example  1: Execute a single line of python code *******************************" << std::endl;
    {
        pyMain.run("print('Hello world!')");
    }



    std::cout << std::endl;
    std::cout << "Example  2: Executed multiple lines of python code using a stream **************" << std::endl;
    {
        // Create the class A in module/namespace __main__ referenced
        // by pyMain.
        pyMain.runStream()
            << std::endl << "class A:"
            << std::endl << "    def __init__(self, s):"
            << std::endl << "        self.s=s"
            << std::endl << "    def f(self):"
            << std::endl << "        print(self.s)"
            << std::endl << ""
            << std::endl << "a=A('Hello world!')"
            << std::endl << "a.f()";

        // You can retrieve objects in a module using get(for more see below).
        std::cout << pyMain.get("A").str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  3: python references **************************************************" << std::endl;
    {
        // All python obects are represented by Python::Reference objects
        // The objects encapsulate the manual reference counting needed by the python-C api.
        // You can always safely create, copy, destroy them.
        //
        // In priciple they act like shared_ptr's in C++. A python object
        // will be destroyed if there is no reference left to it. Here
        // 'no reference' means no Python::Reference on the C++ side,
        // no reference by any other python object, and no reference
        // in the python interpreter.
        //
        // References of the last type are for exapmle hold by names of
        // objects. If you create an instance c of class C by c=C()
        // inside of some module (see below), then the module contains
        // the member 'c' storing a refence to the actual object.

        // Python objects are, e.g., obtained by evaluating expressions.
        Python::Reference e = pyMain.evaluate("1+2");

        // All python objects provide a string representation which
        // is accessable by the str() method.
        std::cout << "Result of the expression 1+2: " << e.str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  4: Using python modules ***********************************************" << std::endl;
    {
        // Python modules are defined in python files named [module].py.
        // You can import them by name to make them visible inside
        // of another module.
        //
        // The first time you call the import() method the module
        // is actually imported and the contained initialization
        // code is executed. Later calls will only return
        // a Reference to the already imported module.

        // Import the module dunepythontest defined by the file
        // dunepythontest.py which must be contained in the search path.
        Python::Module module = pyMain.import("dunepythontest");

        // The Module class Reference and provides some special methods
        // for executing code in their context and importing other module
        // into them. Members of modules can be accessed using the get() method.

        // Get objects named 'add', 'foo', 'bar' from imported module.
        Python::Reference add = module.get("add");
        Python::Reference foo = module.get("foo");
        Python::Reference bar = module.get("bar");

        // Print string representations of the objects
        std::cout << add.str() << std::endl;
        std::cout << foo.str() << std::endl;
        std::cout << bar.str() << std::endl;

        // You can also import all members of a python module into __main__.
        pyMain.run("from dunepythontest import *");
        std::cout << pyMain.get("foo").str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example  5: Types derived from Reference ***************************************" << std::endl;
    {
        // There are various classes derived from Reference, e.g., for
        // modules, callables, ... providing corresponding special methods.
        // Depending on the a-priori knowledge methods will either
        // return a Reference (e.g. get()) or a derived type (e.g. import()).
        // However you can always try to convert a Reference
        // to such a derived type. This will automatically check
        // if the underlying python object is suitable and throw
        // an exception otherwise. In most cases it will be convenient
        // to use auto.

        // module1 is already a Module since import() has a-priori knowledge.
        auto module1 = pyMain.import("dunepythontest");

        // module2 is only a Reference since get() can return anything
        auto module2 = pyMain.get("dunepythontest");

        // You can obtain a Module by converting a Reference
        auto module3 = Python::Module(pyMain.get("dunepythontest"));

        // Converting a nonmatching object will fail.
        pyMain.run("someInt = 3");
        auto someInt = pyMain.get("someInt");
        std::cout << someInt << std::endl;
        try {
            Python::Module someIntAsModule(someInt);
        }
        catch (Dune::Exception e)
        {
            std::cout << "Trying to convert an integert to a Module object failed expectedly:" << std::endl;
            std::cout << e << std::endl;
        }
    }



    std::cout << std::endl;
    std::cout << "Example  6: Creating modules ***************************************************" << std::endl;
    {
        // You can also create modules manually without loading them from
        // a file. In order to execute code in an arbitrary module
        // you can hand it to the evaluate(), run(), runStream(), runFile()
        // as additional argument.

        // Until now there is no module 'mymodule' available.
        Python::runStream()
            << std::endl << "try:"
            << std::endl << "    import mymodule"
            << std::endl << "except:"
            << std::endl << "    print('mymodule was not found!')"
            << std::endl;

        // Create a new module not contained in a file
        auto myModule = Python::createModule("mymodule");

        // Populate the module by executing code inside
        myModule.run("a=1");
        myModule.run("b=1");
        myModule.runStream()
            << std::endl << "def f(x,y):"
            << std::endl << "    return x+y"
            << std::endl;

        // Now we can import the new module into other module, e.g., main
        // using its name.
        pyMain.import("mymodule");
        pyMain.run("print(mymodule.f(mymodule.a, mymodule.b))");

        // You can also import a Module by giving another name.
        pyMain.importAs(myModule, "mymodule2");
        pyMain.run("print(mymodule2.f(mymodule.a, mymodule.b))");
    }



    std::cout << std::endl;
    std::cout << "Example  7: Access any kind of member using get() ******************************" << std::endl;
    {
        // Almost all python objects act like dictionaries
        // and you can access their entries using the get()
        // methon. This is for example true for members
        // of modules, classes, and objects.
        //
        // You can also store the result of the get()
        // for later usage.

        // Get the the class 'A'.
        auto A = pyMain.get("A");

        // Get the instance 'c' of class 'C' created above
        auto a = pyMain.get("a");
        std::cout << a.str() << std::endl;

        // Get the method 'f' of instance 'c'
        auto a_f = a.get("f");
    }



    std::cout << std::endl;
    std::cout << "Example  8: Setting python members using set() *********************************" << std::endl;
    {
        // Conversely to the get method you can set members to point
        // to another python object using the set() method.

        // Create a new python object storing the result of evaluation of an expression.
        auto e = pyMain.evaluate("'foo'");

        // If we would delete e there would be no reference left
        // to the result and hence it would be destroyed.

        // Give the result a name in the __main__ module
        // This is the same as doing run("result = 'foo'")
        pyMain.set("result", e);

        // Now the name 'result' in __main__ does also hold a reference to the result object.
        std::cout << pyMain.get("result").str() << std::endl;

        // Reassign by something different.
        pyMain.set("result", Python::evaluate("2+3"));

        // Now the name 'result' in __main__ holds a reference to the new result object.
        pyMain.run("print(result)");
    }



    std::cout << std::endl;
    std::cout << "Example  9: Calling things in python *******************************************" << std::endl;
    {
        // Python has a very broad notation of callable objects covering
        //
        //   free functions,
        //   lambda functions,
        //   classes,             (calling a class object means calling a constructor)
        //   methods of objects,  (you can use member functions like free functions g=c.f)
        //   functors             (instances of classes implementing the __call__ method)
        //
        // All of these can be accessed using the operator () on a
        // Python::Callable which refers to a callable python object.
        // To obtain a Callable convert an appropriate object.
        //
        // You can pass to Callable objects and C++ objects
        // as arguments. In the latter case they will be converted
        // to python objects before the actual python call is
        // executed automatically (see below).

        auto module = pyMain.import("dunepythontest");

        auto foo = module.get("foo");
        auto bar = module.get("bar");

        // Convert 'add' to Callable. This will only succeed for
        // callable python objects.
        auto add = Python::Callable(module.get("add"));

        // 'add' is a free function defined in the module taking two arguments.
        // We can call it using the () operator.
        auto result = add(foo, bar);

        // Finaly we print a string representation of the result.
        std::cout << "Result of add(foo,bar)=" << result.str() << std::endl;

        // We can also pass C++ objects as arguments.
        std::cout << "Result of add(3.14159, 2.71828)=" << add(3.14159, 2.71828).str() << std::endl;

        // Create some more callable stuff
        pyMain.runStream()
            << std::endl << "class C:"
            << std::endl << "    def __init__(self, s):"
            << std::endl << "        self.s=s"
            << std::endl
            << std::endl << "    def printConstructorArg(self):"
            << std::endl << "        return self.s"
            << std::endl
            << std::endl << "    def f(self, x):"
            << std::endl << "        return x+self.s"
            << std::endl
            << std::endl << "    def add(self, x, y):"
            << std::endl << "        return x+y"
            << std::endl
            << std::endl << "    def sum(self, x):"
            << std::endl << "        return sum(x)"
            << std::endl
            << std::endl << "    def __call__(self, x):"
            << std::endl << "        return x+self.s+x"
            << std::endl;

        // Get the class 'C'. It is callable due to its constructor.
        auto C = Python::Callable(pyMain.get("C"));

        // Call a constructor to create an object of class C.
        // It is callable itself because it implements __call__()
        // which is the python analogue of operator().
        auto c = Python::Callable(C("c_foo"));

        // Call a method of an object.
        // This will print the stored argument of the constructor call for c.
        std::cout << Python::Callable(c.get("printConstructorArg"))().str() << std::endl;

        // Call the operator() of c (__call__ method of python object).
        std::cout << c("bar").str() << std::endl;

        // Call 'c' s method 'sum'.
        std::cout << Python::Callable(c.get("sum"))(Python::evaluate("[1,2,3]")).str() << std::endl;

        // Get the method 'add' of c.
        auto c_add = Python::Callable(c.get("add"));

        // Call 'c' s method 'add'.
        std::cout << "Result of c.add(foo,bar): " << c_add(foo, bar).str() << std::endl;

        // You can also call a lambda function
        auto lambda = Python::Callable(Python::evaluate("lambda x,y: (x+y)"));
        std::cout << "Result of call to lambda function: " << lambda(foo, bar).str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 10: Converting from python to C ****************************************" << std::endl;
    {
        // Conversion of a python object px to a C object cx of type T
        // can be done using
        //
        //   px.toC(cx)
        //
        // The conversion is always done into an already existing C object.
        //
        // For the experts:
        // You can extend the conversion to a custom type T by
        // specializing the struct Convertion<T> and implementing
        // Convertion<T>::toC() using the raw python api.
        // If you do so be carefull to do the reference counting
        // correctly and _not_ to steal a reference to the
        // converted python object.

        auto module = pyMain.import("dunepythontest");

        // Get objects named 'int_list', 'str_list', 'int_tuple' from imported module.
        auto int_list = module.get("int_list");
        auto str_list = module.get("str_list");
        auto int_tuple = module.get("int_tuple");

        // Modify first argument of int_list.
        // Similar to calling a Callable you can either hand
        // a Reference or some C++ type. In the latter case
        // it will be converted automatically using makeObject()
        Python::setItem(int_list, 0, 7);

        // Convert python integer list to vector and print results:
        std::vector<int> int_vec;
        int_list.toC(int_vec);
        std::cout << "python integer list (string representation):" << int_list.str() << std::endl;
        std::cout << "python integer list converted to vector<int>:" << std::endl;
        for(size_t i=0; i<int_vec.size(); ++i)
            std::cout << int_vec[i] << std::endl;

        // Convert python string list to vector and print results:
        std::vector<std::string> str_vec;
        str_list.toC(str_vec);
        std::cout << "python string list (string representation):" << str_list.str() << std::endl;
        std::cout << "python string list converted to vector<string>:" << std::endl;
        for(size_t i=0; i<str_vec.size(); ++i)
            std::cout << str_vec[i] << std::endl;

        // Convert python integer tuple to vector and print results:
        std::vector<int> int_vec2;
        int_tuple.toC(int_vec2);
        std::cout << "python integer tuple (string representation):" << int_tuple.str() << std::endl;
        std::cout << "python integer tuple converted to vector<int>:" << std::endl;
        for(size_t i=0; i<int_vec2.size(); ++i)
            std::cout << int_vec2[i] << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 11: Converting from C to python ****************************************" << std::endl;
    {
        // Conversion of a C object cx of type T to a python object px
        // can be done using
        //
        //   Python::Reference px = Python::makeObject(cx);
        //
        // The conversion will always create a new python object.
        //
        // For the experts:
        // You can extend the conversion to a custom type T by
        // specializing the struct Convertion<T> and implementing
        // Convertion<T>::toPy() using the raw python api.
        // If you do so be carefull to do the reference counting
        // correctly and to return a _new_ reference to the
        // created python object.

        // Create a C++ vector.
        std::vector<int> v(3);
        v[0] = 10;
        v[1] = 11;
        v[2] = 12;

        // Create a python list with the same content.
        // Similar to calling a Callable you can either hand
        // a Reference or some C++ type. In the latter case
        // it will be converted automatically using makeObject()
        pyMain.set("v", v);

        // Let python print the content of the list.
        pyMain.run("print(v)");
    }



    std::cout << std::endl;
    std::cout << "Example 12: Using python to define Dune::VirtualFunction's *********************" << std::endl;
    {
        // There are various ways to wrap callable objects from python into a
        // PythonFunction that implements the Dune::VirtualFunction interface.
        // The basic requirement is that there is a C->python conversion
        // for the domain type and a python->C conversion for the range type.
        // PythonFunction also implements classic function call syntax via
        // operator().

        int x=2;
        double y;

        // We can construct a PythonFunction from a callable object in python.

        // This can be a function, ...
        pyMain.runStream()
            << std::endl << "def f(x):"
            << std::endl << "    return x + 3.141593 + 1"
            << std::endl;

        PythonFunction<int, double> f(pyMain.get("f"));
        f.evaluate(x, y);
        std::cout << "Result of evaluating the wrapped function f through VirtualFunction interface: " << y << std::endl;
        std::cout << "Result of evaluating the wrapped function f: " << f(x) << std::endl;

        // ... a lambda function, ...
        PythonFunction<int, double> lambda(Python::evaluate("lambda x: (x+2)"));
        lambda.evaluate(x, y);
        std::cout << "Result of evaluating a wrapped lambda function: " << lambda(x) << std::endl;

        // ... a functor (instance with __call__ method), ...
        Python::run("c = C(1)");
        PythonFunction<int, double> c(pyMain.get("c"));
        std::cout << "Result of evaluating the wrapped functor c: " << c(x) << std::endl;

        // ... or a method of an instance.
        PythonFunction<int, double> c_f(pyMain.get("c").get("f"));
        std::cout << "Result of evaluating the wrapped method c.f: " << c_f(x) << std::endl;

        // We can also construct a PythonFunction from a python expression
        // using the argument named x. This will create a python
        // function with the given name, taking the argument x,
        // and returning the result of the expression evaluation.
        PythonFunction<int, double> g("g", "x + 3.14159");
        std::cout << "Result of evaluating the function g created from an expression: " << g(x) << std::endl;

        // You can now also call the function in python directly...
        std::cout << "Result of evaluating the created function g in python directly: ";
        pyMain.run("print(g(2))");

        // ... or by calling it manually.
        std::cout << "Result of evaluating the created function g in python manually through the C++ interface: ";
        std::cout << Python::Callable(pyMain.get("g"))(2).str() << std::endl;
    }



    std::cout << std::endl;
    std::cout << "Example 13: Conversion for dune types ******************************************" << std::endl;
    {
        // Conversion is implemented for some dune types

        // One can convert a nested python dictionary to a ParameterTree.
        // Be carefull a python key 'foo.bar' containing a dot will
        // result in two levels 'foo' followed by 'bar' in the ParameterTree.
        pyMain.runStream()
            << std::endl << "tree = {}"
            << std::endl
            << std::endl << "tree[1] = 'one'"
            << std::endl << "tree['two'] = 2e-0"
            << std::endl << "tree['3'] = {}"
            << std::endl << "tree['3']['a'] = 'aaa'"
            << std::endl << "tree['3']['function'] = (lambda x: x*x)"
            << std::endl << "tree['a.b.c'] = 'abc'"
            << std::endl << "tree['a'] = {'b' : {'d':'d'}}"
            << std::endl;

        Dune::ParameterTree param;
        pyMain.get("tree").toC(param);
        std::cout << "python dictionary as ParameterTree:" << std::endl;
        param.report();
    }



    std::cout << std::endl;
    std::cout << "Example 14: Functions acting on dune types *************************************" << std::endl;
    {
        // Conversion is also implementing for FieldVector so we can use this in PythonFunction

        // We can use vector valued functions
        {
            typedef Dune::FieldVector<double, 3> DT;
            typedef Dune::FieldVector<double, 2> RT;
            pyMain.runStream()
                << std::endl << "def h(x):"
                << std::endl << "    return (x[0] + x[1], x[1] + x[2])"
                << std::endl;
            PythonFunction<DT, RT> h(pyMain.get("h"));
            std::cout << "Result of calling a PythonFunction<FV<double,3> , FV<double,2> > : " << h({1, 2, 3}) << std::endl;
        }

        // Python scalars are treated like sequences with size one
        {
            typedef Dune::FieldVector<double, 2> DT;
            typedef Dune::FieldVector<double, 1> RT;
            typedef Dune::VirtualFunction<DT, RT> FBase;
            typedef PythonFunction<DT, RT> F;

            DT x = {1, 2};
            RT y;
            pyMain.runStream()
                << std::endl << "def h2(x):"
                << std::endl << "    return x[0] + x[1]"     // Result is a scalar
                << std::endl << "def h3(x):"
                << std::endl << "    return (x[0] + x[1],)"  // Result is a list with size one
                << std::endl;

            // Call constructor directly
            F h2(pyMain.get("h2"));

            // Convert callable to shared_ptr of interface class
            auto h3 = pyMain.get("h3").toC<std::shared_ptr<FBase>>();

            std::cout << "Result of calling a PythonFunction<FV<double,3> , FV<double,1> > (python result is scalar): " << h2(x) << std::endl;
            h3->evaluate(x,y);
            std::cout << "Result of calling a PythonFunction<FV<double,3> , FV<double,1> > (python result is sigleton list): " << y << std::endl;
        }

        // We can also construct scalar differentiable functions...
        {
            typedef Dune::FieldVector<double, 2> DT;
            typedef Dune::FieldVector<double, 1> RT;
            typedef DifferentiablePythonFunction<DT, RT> F;
            typedef F::DerivativeType DRT;

            DT x = {1, 2};
            RT y;
            DRT z;

            pyMain.import("math");
            pyMain.runStream()
                << std::endl << "def f(x):"
                << std::endl << "    return math.sin(x[0]) + math.cos(x[1])"     // Result is a scalar
                << std::endl << "def df(x):"
                << std::endl << "    return ((math.cos(x[0]),-math.sin(x[1])),)"  // Single row FieldMatrix can only be constructed from sequence of sequences
                << std::endl;

            // Call constructor directly
            F f1(pyMain.get("f"), pyMain.get("df"));

            f1.evaluate(x,y);
            std::cout << "Result of calling a DifferentiablePythonFunction<FV<double,3> , FV<double,1> >::evaluate: " << y << std::endl;

            f1.evaluateDerivative(x,z);
            std::cout << "Result of calling a DifferentiablePythonFunction<FV<double,3> , FV<double,1> >::evaluateDerivative: " << z << std::endl;
        }

        // .. and vector valued differentiable functions...
        {
            typedef Dune::FieldVector<double, 3> DT;
            typedef Dune::FieldVector<double, 2> RT;
            typedef DifferentiablePythonFunction<DT, RT> F;
            typedef VirtualDifferentiableFunction<DT, RT> FBase;
            typedef F::DerivativeType DRT;

            DT x = {1, 2, 3};
            RT y;
            DRT z;

            pyMain.import("math");
            pyMain.runStream()
                << std::endl << "def f(x):"
                << std::endl << "    return (x[0]**2+x[1], x[1]**2+x[2])"     // Result is a list
                << std::endl << "def df(x):"
                << std::endl << "    return ((2*x[0], 1, 0),(0, 2*x[1], 1))"  // Nested tuple as matrix
                << std::endl << "fdf = (f, df)"
                << std::endl;

//            F f(pyMain.get("f"), pyMain.get("df"));
            auto f = pyMain.get("fdf").toC<std::shared_ptr<FBase>>();

            f->evaluate(x,y);
            std::cout << "Result of calling a DifferentiablePythonFunction<FV<double,3> , FV<double,2> >::evaluate: " << y << std::endl;

            f->evaluateDerivative(x,z);
            std::cout << "Result of calling a DifferentiablePythonFunction<FV<double,3> , FV<double,2> >::evaluateDerivative: " << z << std::endl;
        }
    }



    std::cout << std::endl;
    std::cout << "Example 15: Importing function dictionaries ************************************" << std::endl;
    {
        // Since Conversion is implemented for
        //
        //   std::map,
        //   shared_ptr<T>,
        //   PythonFunction*,
        //   VirtualFunction*
        //
        // it is also possible to import dictionaries of callable
        // python objects into a map of functions.

        // Create callable python objects with proper signature.
        pyMain.runStream()
            << std::endl << "class G:"
            << std::endl << "    def __init__(self, z):"
            << std::endl << "        self.z=z"
            << std::endl
            << std::endl << "    def __call__(self, x):"
            << std::endl << "        return (x[0] + self.z, x[1] + self.z)"
            << std::endl
            << std::endl << "    def f(self, x):"
            << std::endl << "        return (x[1] + self.z, x[2] + self.z)"
            << std::endl
            << std::endl << "g = G(3)"
            << std::endl;

        // Create dictionary of callable python objects.
        pyMain.runStream()
            << std::endl << "functions = {}"
            << std::endl
            << std::endl << "functions['h'] = h"
            << std::endl << "functions['sumsum'] = (lambda x: (sum(x), sum(x)) )"
            << std::endl << "functions['one'] = (lambda x: (1,1))"
            << std::endl << "functions['g'] = g"
            << std::endl << "functions['g.f'] = g.f"
            << std::endl;

        typedef Dune::FieldVector<double, 3> DT;
        typedef Dune::FieldVector<double, 2> RT;
        typedef Dune::VirtualFunction<DT, RT> Function;
        typedef std::shared_ptr<Function> FunctionPointer;
        typedef std::map<std::string, FunctionPointer> FunctionMap;

        // Import dictionaries of callables to map of functions.
        FunctionMap functions;
        pyMain.get("functions").toC(functions);

        DT x;
        RT y;
        x[0] = 1;
        x[1] = 2;
        x[2] = 3;

        FunctionMap::iterator it = functions.begin();
        FunctionMap::iterator end = functions.end();
        for(; it != end; ++it)
        {
            it->second->evaluate(x,y);
            std::cout << it->first << "(x) = " << y << std::endl;
        }
    }

    std::cout << std::endl;
    std::cout << "Example 16: Creating Dune-functions functions **********************************" << std::endl;
    {
        // Functions implementing the interface in dune-functions can also be created
        // directly

        using Domain = Dune::FieldVector<double, 3>;
        using Range = Dune::FieldVector<double, 2>;

        pyMain.import("math");
        pyMain.runStream()
            << std::endl << "def f(x):"
            << std::endl << "    return (x[0]**2+x[1], x[1]**2+x[2])"     // Result is a list
            << std::endl << "def df(x):"
            << std::endl << "    return ((2*x[0], 1, 0),(0, 2*x[1], 1))"  // Nested tuple as matrix
            << std::endl << "fdf = (f, df)"
            << std::endl;

        // makeDifferentiableFunction automatically converts to Python::Callable
        auto f = Python::makeDifferentiableFunction<Range(Domain)>(pyMain.get("f"), pyMain.get("df"));

        std::cout << "Result of calling a function FV<double,2>(FV<double,3>) : " << f({1, 2, 3}) << std::endl;
        std::cout << "Result of calling the derivative of a function FV<double,2>(FV<double,3>) : " << derivative(f)({1, 2, 3}) << std::endl;

        // functions can also be cretaed from labda expressions
        auto pyG = Python::evaluate("lambda x: math.sin(x)");
        auto pyDG = Python::evaluate("lambda x: math.cos(x)");
        auto pyDDG = Python::evaluate("lambda x: -math.sin(x)");
        auto pyDDDG = Python::evaluate("lambda x: -math.cos(x)");

        auto g = Python::makeDifferentiableFunction<double(double)>(pyG, pyDG, pyDDG, pyDDDG, pyG);

        std::cout << "Result of calling a function double(double) : " << g(0) << std::endl;
        std::cout << "Result of calling the derivative of a function double(double) : " << derivative(g)(0) << std::endl;
        std::cout << "Result of calling the 2nd derivative of a function double(double) : " << derivative(derivative(g))(0) << std::endl;
        std::cout << "Result of calling the 3rd derivative of a function double(double) : " << derivative(derivative(derivative(g)))(0) << std::endl;
        std::cout << "Result of calling the 4rd derivative of a function double(double) : " << derivative(derivative(derivative(derivative(g))))(0) << std::endl;

        // If you only need the function but no derivatives, you can also use makeFunction()
        auto h = Python::makeFunction<double(double)>(Python::evaluate("lambda x: x*x"));
        std::cout << "Result of calling a function double(double) : " << h(0.5) << std::endl;
    }


#if HAVE_UG
    std::cout << std::endl;
    std::cout << "Example 17: Filling a GridFactory form python **********************************" << std::endl;
    {
        // prepare types and grid factory
        using Grid = Dune::UGGrid<2>;
        using GridFactory = Dune::GridFactory<Grid>;
        GridFactory gridFactory;

        // Get grid describtion from python and convert it to GridFactory
        auto pyGrid = pyMain.get("grid");
        pyGrid.toC(gridFactory);

        // Let GridFactory create grid
        std::unique_ptr<Grid> gridPtr(gridFactory.createGrid());
        auto& grid = *gridPtr;

        // Refine and write grid to visualize parametrized boundary
        grid.globalRefine(1);
        grid.globalRefine(1);

        Dune::VTKWriter<Grid::LeafGridView> vtkWriter(grid.leafGridView());
        vtkWriter.write("python-generated-grid");

        std::cout << "number of elements in created grid : " << grid.leafGridView().indexSet().size(0) << std::endl;
    }
#endif

    // Before exiting you should stop the python interpreter.
    Python::stop();

    return 0;
}
