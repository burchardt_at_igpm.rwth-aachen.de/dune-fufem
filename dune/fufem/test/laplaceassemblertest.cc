#include <config.h>

#include <array>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/istl/io.hh>

#include <dune/fufem/boundarypatch.hh>
#include <dune/fufem/assemblers/boundaryoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

using namespace Dune;


int main (int argc, char *argv[])
{
  bool passed = true;
  Dune::MPIHelper::instance(argc, argv);

  // Build a test grid
  typedef YaspGrid<2> GridType;

  FieldVector<double,2> h(3);
  std::array<int,2> n = {{3,3}};

  GridType grid(h,n);

  // Construct a function space basis for the grid
  typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<GridType::LeafGridView>> P1Basis;
  P1Basis p1Basis(grid.leafGridView());

  // Construct a boundary patch for the entire grid boundary
  BoundaryPatch<GridType::LeafGridView> boundary(grid.leafGridView(), true);

  // Assemble Laplace-Beltrami matrix on the boundary
  BoundaryOperatorAssembler<P1Basis,P1Basis> interfaceAssembler(p1Basis, p1Basis, boundary);

  typedef BCRSMatrix<FieldMatrix<double,1,1> > MatrixType;
  MatrixType h1Matrix;

  LaplaceAssembler<GridType,P1Basis::LocalFiniteElement,P1Basis::LocalFiniteElement> laplaceAssembler;
  interfaceAssembler.assemble(laplaceAssembler, h1Matrix);

  //////////////////////////////////////////////////////////////////////////
  //  Test whether the matrix is correct (well, plausible)
  //////////////////////////////////////////////////////////////////////////

  // All rows belonging to non-boundary vertices should be zero
  for (size_t i=0; i<h1Matrix.N(); i++)
    if (not (*boundary.getVertices())[i][0])
      passed = passed and h1Matrix[i].infinity_norm() < 1e-6;

  // Constant vectors are in the kernel
  typedef BlockVector<FieldVector<double,1> > VectorType;
  VectorType constantVector(h1Matrix.M());
  constantVector = 1;
  VectorType zero(h1Matrix.N());
  h1Matrix.mv(constantVector,zero);

  passed = passed and zero.infinity_norm() < 1e-6;
  return passed ? 0 : 1;
}
