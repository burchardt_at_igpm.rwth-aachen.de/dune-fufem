#include <config.h>

#include <cmath>
#include <cstdio>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/function.hh>


#include <dune/fufem/functions/composedfunction.hh>
#include <dune/fufem/functions/composedgridfunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/fufem/functionspacebases/p1nodalbasis.hh>

#include "common.hh"

/** \brief inflates by zeros or cuts off trailing components according to Domain and Range dimensions. for domaindim==rangedim it's simply the identity
 *
 */
template <class DT, class RT>
class InflateOrCut_Base:
    public Dune::VirtualFunction<DT,RT>
{
        typedef Dune::VirtualFunction<DT,RT> BaseType;

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        enum {DomainDim = DomainType::dimension,
              RangeDim  = RangeType::dimension};

        InflateOrCut_Base()
        {}

        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            y = 0.0;

            for (int i=0; i<DomainDim and i<RangeDim; ++i)
            {
                y[i]=x[i];
            }

            return;
        }
};

/** \brief inflates by zeros or cuts off trailing components according to Domain and Range dimensions. for domaindim==rangedim it's simply the identity
 *
 */
template <class GridType, class RT>
class InflateOrCut_GridF:
    public VirtualGridFunction<GridType,RT>
{
        typedef VirtualGridFunction<GridType,RT> BaseType;

    public:
        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        typedef typename BaseType::Element Element;

        enum {DomainDim = DomainType::dimension,
              RangeDim  = RangeType::dimension};

        InflateOrCut_GridF(GridType& grid):
            BaseType(grid)
        {}

        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            y = 0.0;

            DomainType xglobal = e.geometry().global(x);

            for (int i=0; i<DomainDim and i<RangeDim; ++i)
            {
                y[i]=xglobal[i];
            }

            return;
        }

        virtual bool isDefinedOn(const Element&) const
        {
            return true;
        }

};
/** \brief inflates by zeros or cuts off trailing components according to Domain and Range dimensions. for domaindim==rangedim it's simply the identity
 *
 */
template <class GridViewType, class RT>
class InflateOrCut_GridVF:
    public VirtualGridViewFunction<GridViewType,RT>
{
        typedef VirtualGridViewFunction<GridViewType,RT> BaseType;

    public:
        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;

        typedef typename BaseType::Element Element;

        enum {DomainDim = DomainType::dimension,
              RangeDim  = RangeType::dimension};

        InflateOrCut_GridVF(const GridViewType& gridview):
            BaseType(gridview)
        {}

        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            y = 0.0;

            DomainType xglobal = e.geometry().global(x);

            for (int i=0; i<DomainDim and i<RangeDim; ++i)
            {
                y[i]=xglobal[i];
            }

            return;
        }
};

/** \brief TestSuite for ComposedFunction
 *
 *  Takes InflateOrCut_Base functions f,g such that \f$f\circ g:\mathbb R^n\longrightarrow\mathbb R^m\longrightarrow \mathbb R^d\f$ with \f$m > n \geq d\f$. So on the first d components
 *  the composition should be the identity.
 */
struct ComposedFunctionTestSuite
{
    bool check()
    {
        const std::size_t n=3,
                  m=5,
                  d=2;

        std::size_t n_testpoints=100;

        typedef Dune::FieldVector<double,n> DomainType;
        typedef Dune::FieldVector<double,m> IntermediateRangeType;
        typedef Dune::FieldVector<double,d> RangeType;

        typedef InflateOrCut_Base<DomainType,IntermediateRangeType> InnerFunctionType;
        typedef InflateOrCut_Base<IntermediateRangeType,RangeType> OuterFunctionType;

        InnerFunctionType g;
        OuterFunctionType f;

        ComposedFunction<DomainType,IntermediateRangeType,RangeType> f_after_g(f,g);

        DomainType x(0.0);
        RangeType  y(0.0);

        for (std::size_t run=0; run<n_testpoints; ++run)
        {
            for (std::size_t dcomp=0; dcomp<n; ++dcomp)
                x[dcomp] = (5.0*rand())/RAND_MAX - 2.5;

            f_after_g.evaluate(x,y);

            for (std::size_t rcomp=0; rcomp<d; ++rcomp)
                if (std::abs(y[rcomp]-x[rcomp])>1e-15)
                {
                    std::cout << "y[" << rcomp << "] = " << y[rcomp] << ", x[" << rcomp << "] = " << x[rcomp] << std::endl;

                    return false;
                }
        }

        return true;
    }
};
/** \brief TestSuite for ComposedFunction
 *
 *  Takes InflateOrCut functions f,g such that \f$f\circ g:\mathbb R^n\longrightarrow\mathbb R^m\longrightarrow \mathbb R^d\f$ with \f$m > n,\ m > d\f$. So on the first min(n,d) components
 *  the composition should be the identity.
 */
struct ComposedGridFunctionTestSuite
{
    template <class GridType>
    bool check(GridType& grid)
    {
        const unsigned int n=GridType::dimension,
                  m=5,
                  d=4;

        const std::size_t n_testpoints=100;

        typedef Dune::FieldVector<typename GridType::ctype,n> DomainType;
        typedef Dune::FieldVector<double,m> IntermediateRangeType;
        typedef Dune::FieldVector<double,d> RangeType;

        typedef InflateOrCut_GridF<GridType,IntermediateRangeType> InnerFunctionType;
        typedef InflateOrCut_GridVF<typename GridType::LeafGridView,IntermediateRangeType> InnerFunctionType2;
        typedef InflateOrCut_Base<IntermediateRangeType,RangeType> OuterFunctionType;

        InnerFunctionType g(grid);
        InnerFunctionType2 gg(grid.leafGridView());
        OuterFunctionType f;

        ComposedGridFunction<GridType,IntermediateRangeType,RangeType> f_after_g(f,g);
        ComposedGridFunction<GridType,IntermediateRangeType,RangeType> f_after_gg(f,gg);

        DomainType x(0.0);
        RangeType  y(0.0),yy(0.0);

        for (std::size_t run=0; run<n_testpoints; ++run)
        {
            double sum=0;
            for (std::size_t dcomp=0; dcomp<n; ++dcomp)
            {
                x[dcomp] = std::min((1.0*rand())/(n-1)/RAND_MAX, 1-sum);
                sum += x[dcomp];
            }

            f_after_g.evaluate(x,y); // let's just use evaluate. It's the default implementation using evaluateLocal internally so the GridFunction aspect should be checked well enough
            f_after_gg.evaluate(x,yy);

            std::size_t kmin = std::min(d,n);

            for (std::size_t rcomp=0; rcomp<kmin; ++rcomp)
                if (std::abs(y[rcomp]-x[rcomp])>1e-15 or std::abs(yy[rcomp]-x[rcomp])>1e-15)
                {
                    std::cout << "y[" << rcomp << "] = " << y[rcomp] << ", x[" << rcomp << "] = " << x[rcomp] << std::endl;

                    return false;
                }

            for (std::size_t rcomp=kmin; rcomp<d; ++rcomp)
                if (y[rcomp]!=0.0 or yy[rcomp]!=0.0)
                    return false;
        }

        return true;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the ComposedFunctionTest" << std::endl;

    bool passed = true;

    {
        ComposedFunctionTestSuite cftests;

        passed = cftests.check();
        if (passed)
            std::cout << "ComposedFunction test passed" << std::endl;
        else
            std::cout << "ComposedFunction test failed" << std::endl;
    }

    {
        ComposedGridFunctionTestSuite cftests;
        passed = checkWithStandardAdaptiveGrids(cftests);
    }

    return passed ? 0 : 1;

}
