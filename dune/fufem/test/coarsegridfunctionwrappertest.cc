#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/function.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functions/coarsegridfunctionwrapper.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include "common.hh"



template <int dim, class RT>
class SinSin :
    public Dune::VirtualFunction<typename Dune::FieldVector<double, dim>, RT>
{
    public:
        void evaluate(const Dune::FieldVector<double, dim>& x, RT& y) const
        {
            double r=1.0;
            for(int i=0; i<dim; ++i)
                r*=sin(x[i]);
            y = r;
        }
};



struct CoarseGridFunctionWrapperTestSuite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        typedef typename Dune::template FieldVector<double, 1> RangeType;

        const int dim = GridType::dimensionworld;

        typedef typename Dune::BlockVector<RangeType> Vector;

        typedef typename GridType::LevelGridView CoarseGridView;
        typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<CoarseGridView>> CoarseBasis;
        typedef BasisGridFunction<CoarseBasis, Vector> CoarseGridFunction;

        typedef typename GridType::LeafGridView FineGridView;
        typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<FineGridView>> FineBasis;
        typedef BasisGridFunction<FineBasis, Vector> FineGridFunction;

        typedef CoarseGridFunctionWrapper<CoarseGridFunction> WrappedCoarseGridFunction;


        CoarseBasis cBasis(grid.levelGridView(0));
        FineBasis fBasis(grid.leafGridView());


        SinSin<dim, RangeType> f;

        std::cout << "Interpolating analytic function on coarse grid." << std::endl;
        Vector cCoeff;
        Functions::interpolate(cBasis, cCoeff, f);
        CoarseGridFunction cFunc(cBasis, cCoeff);

        WrappedCoarseGridFunction wrappedCFunc(cFunc);

        std::cout << "Interpolating wrapped coarse grid function on fine grid." << std::endl;
        Vector fCoeff;
        Functions::interpolate(fBasis, fCoeff, wrappedCFunc);
        FineGridFunction fFunc(fBasis, fCoeff);


        bool passed = true;

        std::cout << "Comparing coarse grid function and fine grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(cFunc, fFunc, fBasis.getGridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        std::cout << "Comparing wrapped coarse grid function and coarse grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(wrappedCFunc, cFunc, fBasis.getGridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        std::cout << "Comparing wrapped coarse grid function and fine grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(wrappedCFunc, fFunc, fBasis.getGridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        std::cout << "Comparing wrapped coarse grid function and fine grid function by local evaluation." << std::endl;
        if (not(compareEvaluateLocalByGridViewQuadrature(wrappedCFunc, fFunc, fBasis.getGridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        return passed;
    }

};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    CoarseGridFunctionWrapperTestSuite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;
}
