#include <config.h>

#include <cmath>
#include <cstdio>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/constantfunction.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/virtualdifferentiablefunction.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/generalizedlaplaceassembler.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include "common.hh"


/** \brief TestSuite for GeneralizedLaplaceAssembler
 *
 *  This TestSuite tests for consistency of the GeneralizedLaplaceAssembler with the LaplaceAssembler
 */
struct GeneralizedLaplaceAssemblerTestSuite
{
    template <typename GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        std::cout << "Checking with scalar type FieldVector<double,1>" << std::endl;
        passed = passed and checkWithScalarType<GridType, typename Dune::FieldVector<double, 1> >(grid);

        std::cout << "Checking with scalar type FieldMatrix<double,1,1>" << std::endl;
        passed = passed and checkWithScalarType<GridType, typename Dune::FieldMatrix<double, 1, 1> >(grid);

        std::cout << "Checking with scalar type double" << std::endl;
        passed = passed and checkWithScalarType<GridType, double>(grid);

        return passed;
    }


    template<typename Range>
    std::vector<typename Range::size_type> orderedRangePattern(const Range& r)
    {
        std::vector<typename Range::size_type> pattern;
        auto it = r.begin();
        auto end = r.end();
        for(; it!=end; ++it)
            pattern.push_back(it.index());
        std::sort(pattern.begin(), pattern.end());
        return pattern;
    }

    template<typename MatrixType>
    bool checkMatrixEquality(const MatrixType& A, const MatrixType& B)
    {
        const double tol = 1e-15;
        if (not((A.N() == B.N()) and (A.M() == B.M())))
        {
            std::cout << "Matrices have different sizes" << std::endl;;
            return false;
        }
        for (size_t i=0; i<A.N(); ++i)
        {
            if (orderedRangePattern(A[i]) != orderedRangePattern(B[i]))
            {
                std::cout << "Matrices have different pattern in row " << i << std::endl;
                return false;
            }
            auto it = A[i].begin();
            auto end = A[i].end();
            for(; it!=end; ++it)
            {
                if (std::abs(*it - B[i][it.index()]) > tol)
                {
                    std::cout << "Matrices entries at (" << i << "," << it.index() << ") differ by more than " << tol << std::endl;
                    return false;
                }
            }
        }
        return true;
    }

    template <typename GridType, typename ScalarRangeType>
    bool checkWithScalarType(const GridType& grid)
    {
        bool result = true;

        const int dim = GridType::dimension;

        typedef DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<typename GridType::LeafGridView>> Basis;
        typedef typename Basis::LocalFiniteElement FE;

        Basis basis(grid.leafGridView());

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate DomainType;
        typedef Dune::FieldMatrix<double, dim, dim> MatrixRangeType;

        typedef ConstantFunction<DomainType, ScalarRangeType> ConstantScalarFunction;
        ConstantScalarFunction scalarconstant1(1.0);
        ConstantScalarFunction scalarconstant2(2.0);

        MatrixRangeType diag(0.0);
        for (int d=0; d<dim; ++d)
            diag[d][d] = 1.0;
        typedef ConstantFunction<DomainType, MatrixRangeType> ConstantMatrixFunction;
        ConstantMatrixFunction matrixconstant1(diag);
        diag *= 2.0;
        ConstantMatrixFunction matrixconstant2(diag);

        Assembler<Basis, Basis> assembler(basis, basis);

        // assemble without coefficient (LaplaceAssembler)
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat;
        LaplaceAssembler<GridType,FE,FE> laplaceAssembler;
        assembler.assembleOperator(laplaceAssembler, stiff_mat);

        // assemble and check with scalar coeffient 1
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_s1;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantScalarFunction> genLaplaceAssembler_s1(scalarconstant1,2);
        assembler.assembleOperator(genLaplaceAssembler_s1, stiff_mat_s1);
        result = result and checkMatrixEquality(stiff_mat, stiff_mat_s1);

        // assemble and check with scalar coeffient 2
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_s2;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantScalarFunction> genLaplaceAssembler_s2(scalarconstant2,2);
        assembler.assembleOperator(genLaplaceAssembler_s2, stiff_mat_s2);
        stiff_mat_s2 *= 0.5;
        result = result and checkMatrixEquality(stiff_mat, stiff_mat_s2);

        // assemble and check with matrix coeffient 1*Id
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_m1;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantMatrixFunction> genLaplaceAssembler_m1(matrixconstant1,2);
        assembler.assembleOperator(genLaplaceAssembler_m1, stiff_mat_m1);
        result = result and checkMatrixEquality(stiff_mat, stiff_mat_m1);

        // assemble and check with matrix coeffient 2*Id
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_m2;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantMatrixFunction> genLaplaceAssembler_m2(matrixconstant2,2);
        assembler.assembleOperator(genLaplaceAssembler_m2, stiff_mat_m2);
        stiff_mat_m2 *= 0.5;
        result = result and checkMatrixEquality(stiff_mat, stiff_mat_m2);

        return result;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the GeneralizedLaplaceAssemblerTest" << std::endl;

    GeneralizedLaplaceAssemblerTestSuite tests;

    bool passed = true;

    passed = checkWithStandardAdaptiveGrids(tests);


    return passed ? 0 : 1;

}
