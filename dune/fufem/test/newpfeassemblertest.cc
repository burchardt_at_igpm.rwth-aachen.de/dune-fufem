#include "config.h"

#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/functions/basisgridfunction.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/mechanics/cubictensor.hh>
#include <dune/fufem/mechanics/isotropictensor.hh>
#include <dune/fufem/mechanics/newpfeassembler.hh>
#include <dune/fufem/mechanics/tetratensor.hh>
#include <dune/fufem/test/common.hh>

#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

template <int n>
struct eigenStrainComputer {
  enum { dim = n };
  void getEigenstrain(Dune::FieldVector<double, 2> const &x,
                      SymmetricTensor<n> &y) const {
    y = 1.0;
  }
};

template <int n>
struct IsotropicSampleAlloy : public eigenStrainComputer<n> {
  void getElasticityTensor(Dune::FieldVector<double, 2> const &x,
                           ElasticityTensor<n> &y) const {
    y = IsotropicTensor<n>(1, 0.25);
  }
};

template <int n>
struct CubicSampleAlloy : public eigenStrainComputer<n> {
  void getElasticityTensor(Dune::FieldVector<double, 2> const &x,
                           ElasticityTensor<n> &y) const {
    y = CubicTensor<n>(1, 2, 3);
  }
};

template <int n>
struct TetragonalSampleAlloy;

template <>
struct TetragonalSampleAlloy<3> : public eigenStrainComputer<3> {
  void getElasticityTensor(Dune::FieldVector<double, 2> const &x,
                           ElasticityTensor<3> &y) const {
    y = TetraTensor<3>(1, 2, 3, 4, 5, 6);
  }
};

template <>
struct TetragonalSampleAlloy<2> : public eigenStrainComputer<2> {
  void getElasticityTensor(Dune::FieldVector<double, 2> const &x,
                           ElasticityTensor<2> &y) const {
    y = TetraTensor<2>(1, 2, 3, 4);
  }
};

template <class Alloy>
void runTest() {
  auto const dim = Alloy::dim;
  int const uniformRefinements = 4;

  using Vector = Dune::BlockVector<Dune::FieldVector<double, dim>>;

  using Grid = Dune::YaspGrid<dim>;
  auto grid = constructCoarseYaspGrid<dim>();
  grid->globalRefine(uniformRefinements);

  using GridView = typename Grid::LeafGridView;

  using Basis = DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<GridView>>;
  using LocalBasis = typename Basis::LocalFiniteElement;
  using Function = BasisGridFunction<Basis, Vector>;

  using MaterialAssembler =
      NewPFEAssembler<Grid, LocalBasis, LocalBasis, Function, Alloy>;

  Basis basis(grid->leafGridView());
  Vector coefficients(basis.size());
  coefficients = 1.0;
  Function oneFunction(basis, coefficients);

  Assembler<Basis, Basis> assembler(basis, basis);

  Alloy alloy;
  MaterialAssembler materialAssembler(oneFunction, alloy);
  Dune::BCRSMatrix<Dune::FieldMatrix<double, dim, dim>> M;
  assembler.assembleOperator(materialAssembler, M);

  std::cout << M.frobenius_norm() << std::endl;
}

// This test shows how to use a NewPFEAssembler and makes sure that it
// continues to compile. It does not check the correctness of any
// computations.
int main (int argc, char *argv[]) {
  Dune::MPIHelper::instance(argc, argv);

  runTest<IsotropicSampleAlloy<2>>();
  runTest<IsotropicSampleAlloy<3>>();
  runTest<CubicSampleAlloy<2>>();
  runTest<CubicSampleAlloy<3>>();
  runTest<TetragonalSampleAlloy<2>>();
  runTest<TetragonalSampleAlloy<3>>();
}
