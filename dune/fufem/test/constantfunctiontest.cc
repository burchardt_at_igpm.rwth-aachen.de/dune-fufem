#include <config.h>

#include <type_traits>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>

#include <dune/fufem/functions/constantfunction.hh>

using namespace Dune;

template<class DT, class RT>
struct ConstantFunctionTestSuite
{
    private:
        typedef typename ConstantFunction<DT, RT>::DerivativeType D;

        static bool checkEvaluate(const ConstantFunction<DT,RT>& constantFunction, const RT& constantFunctionValue)
        {
            DT x(5);
            RT y(0);

            constantFunction.evaluate(x,y);

            return (y == constantFunctionValue);
        }

        static bool checkEvaluateDerivative(const ConstantFunction<DT,RT>& constantFunction)
        {
            DT x(5);
            D d;
            constantFunction.evaluateDerivative(x,d);

            return checkDerivativeEqualsZero(d);
        }

        static bool checkDerivativeEqualsZero(DerivativeTypeNotImplemented& d)
        {
            return true;
        }

        template<class T, typename std::enable_if<not(std::is_same<T, DerivativeTypeNotImplemented>::value),int >::type = 0>
        static bool checkDerivativeEqualsZero(T& d)
        {
            return d == D(0);
        }

    public:
        static bool check()
        {
            bool passed = true;

            const RT constantFunctionValue(RT(1));
            const ConstantFunction<DT, RT> constantFunction(constantFunctionValue);

            passed = passed and checkEvaluate(constantFunction,constantFunctionValue);
            passed = passed and checkEvaluateDerivative(constantFunction);

            return passed;
        }
};

int main (int argc, char *argv[])
{
    Dune::MPIHelper::instance(argc, argv);

    bool passed = true;

    passed = passed and ConstantFunctionTestSuite< double, double >::check();
    passed = passed and ConstantFunctionTestSuite< float, double >::check();
    passed = passed and ConstantFunctionTestSuite< double, int >::check();

    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,2>, Dune::FieldVector<double,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,3>, Dune::FieldVector<double,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,2>, Dune::FieldVector<double,3> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldMatrix<double,2,2>, Dune::FieldVector<double,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,2>, Dune::FieldMatrix<double,1,1> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldMatrix<double,1,1>, Dune::FieldMatrix<double,1,1> >::check();

    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<float,3>, Dune::FieldVector<double,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<float,3>, Dune::FieldVector<float,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,3>, Dune::FieldVector<float,2> >::check();
    passed = passed and ConstantFunctionTestSuite< Dune::FieldVector<double,3>, Dune::FieldVector<int,2> >::check();

    if (not passed)
        std::cout << "ConstantFunctionTest failed." << std::endl;

    return passed ? 0 : 1;
}
