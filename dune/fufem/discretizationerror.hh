#ifndef DISCRETIZATION_ERROR_HH
#define DISCRETIZATION_ERROR_HH

#include <cmath>

#include <dune/geometry/quadraturerules.hh>

#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

template <class GridView>
class DiscretizationError
{
    enum {dim = GridView::dimension};

    typedef typename GridView::Grid::ctype ctype;

public:

    /** \brief Compute L2 error between a grid function and an arbitrary function
     */
    template <int blocksize>
    static double computeL2Error(const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >* a,
               const Dune::VirtualFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* b,
               QuadratureRuleKey quadKey)
    {

    // The error to be computed
    double error = 0;

    const GridView& gridView = a->gridView();

    typename GridView::template Codim<0>::Iterator eIt    = gridView.template begin<0>();
    typename GridView::template Codim<0>::Iterator eEndIt = gridView.template end<0>();

    for (; eIt!=eEndIt; ++eIt) {

        // Get quadrature formula
        quadKey.setGeometryType(eIt->type());
        const Dune::QuadratureRule<ctype,dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

        for (size_t i=0; i<quad.size(); i++) {

            // Evaluate function a
            Dune::FieldVector<double,blocksize> aValue;
            a->evaluateLocal(*eIt, quad[i].position(),aValue);

            // Evaluate function b.  If it is a grid function use that to speed up the evaluation
            Dune::FieldVector<double,blocksize> bValue;
            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(b))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(b)->evaluateLocal(*eIt,
                                                                                                                      quad[i].position(),
                                                                                                                      bValue
                                                                                                                     );
            else
                b->evaluate(eIt->geometry().global(quad[i].position()), bValue);

            // integrate error
            error += (aValue - bValue) * (aValue - bValue) * quad[i].weight() * eIt->geometry().integrationElement(quad[i].position());

        }

    }

    return std::sqrt(error);

    }


    template <int blocksize>
    static double computeH1HalfNormDifferenceSquared(const GridView& gridView,
                                                     const VirtualDifferentiableFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* u,
                                                     const VirtualDifferentiableFunction<Dune::FieldVector<ctype,dim>, Dune::FieldVector<double,blocksize> >* v,
                                                     QuadratureRuleKey quadKey)
    {
    double norm = 0.0;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;

    ElementIterator eIt    = gridView.template begin<0>();
    ElementIterator eEndIt = gridView.template end<0>();
    for (; eIt!=eEndIt; ++eIt)
    {
        quadKey.setGeometryType(eIt->type());
        const Dune::QuadratureRule<ctype,dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

        for (size_t ip=0; ip<quad.size(); ++ip)
        {
            // Local position of the quadrature point
            const Dune::FieldVector<double,dim>& quadPos = quad[ip].position();

            const double weight = quad[ip].weight();

            const double integrationElement = eIt->geometry().integrationElement(quadPos);

            // Evaluate integral
            Dune::FieldMatrix<double,blocksize,dim> u_di;
            Dune::FieldMatrix<double,blocksize,dim> v_di;

            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(u))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(u)->evaluateDerivativeLocal(*eIt, quadPos, u_di);
            else
                u->evaluateDerivative(eIt->geometry().global(quadPos), u_di);

            if (dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(v))
                dynamic_cast<const VirtualGridViewFunction<GridView,Dune::FieldVector<double,blocksize> >*>(v)->evaluateDerivativeLocal(*eIt, quadPos, v_di);
            else
                v->evaluateDerivative(eIt->geometry().global(quadPos), v_di);

            u_di -= v_di;  // compute the difference
            norm += u_di.frobenius_norm2() * weight * integrationElement;
        }
    }
    return norm;
    }


};

#endif

