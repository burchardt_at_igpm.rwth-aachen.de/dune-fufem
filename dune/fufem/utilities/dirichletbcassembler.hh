// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_UTILITIES_DIRICHLET_BC_ASSEMBLER_HH
#define DUNE_FUFEM_UTILITIES_DIRICHLET_BC_ASSEMBLER_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/fufem/boundarypatchprolongator.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functions/coarsegridfunctionwrapper.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>
#include <dune/fufem/utilities/boundarypatchfactory.hh>

#include <dune/functions/functionspacebases/pq1nodalbasis.hh>

#include <dune/grid/io/file/amirameshreader.hh>

/** \brief This class provides a method to assemble the leaf Dirichlet values and dofs from coarse ones.
 *
 *         So far only P1 elements are supported.
 * */
template <class GridType>
class DirichletBCAssembler
{

    const static int dim = GridType::dimension;
public:

    /** \brief Assemble leaf grid Dirichlet Values and dofs.
     *
     *  \param grid The grid.
     *  \param coarseDirichletNodes The Dirichlet dofs on level 0.
     *  \param coarseDirichletValues The Dirichlet values on level 0.
     *  \param leafDirichletNodes The leaf Dirichlet dofs.
     *  \param leafDirichletValues The Dirichlet values on the leaf view.
     */
    template <class VectorType, int ncomp, int mcomp,
              class Enable = std::enable_if_t<((ncomp == 1) and (mcomp >=1)) or (ncomp == mcomp)> >
    static void assembleDirichletBC(const GridType& grid,
                                    const Dune::BitSetVector<ncomp>& coarseDirichletNodes,
                                    const VectorType& coarseDirichletValues,
                                    Dune::BitSetVector<mcomp>& leafDirichletNodes,
                                    VectorType& leafDirichletValues);

    /** \brief Assemble leaf grid Dirichlet Values and dofs.
     *
     *  \param grid The grid.
     *  \param config A config file with keys 'dnFile' and 'dvFile'.
     *  \param leafDirichletNodes The leaf Dirichlet dofs.
     *  \param leafDirichletValues The Dirichlet values on the leaf view.
     *  \param reader A string denoting which kind of reader to use.
     */
    template <class VectorType, int ncomp>
    static void assembleDirichletBC(const GridType& grid,
                                    const Dune::ParameterTree& config,
                                    Dune::BitSetVector<ncomp>& leafDirichletNodes,
                                    VectorType& leafDirichletValues, std::string path = "");

    /** \brief Assemble only the Dirichlet node information.
     *
     *  Method is only enabled if ncomp equal 1 and mcomp >= ncomp, or ncomp equal to mcomp
     *
     *  \param grid The grid.
     *  \param coarseDirichletNodes The Dirichlet dofs on level 0.
     *  \param leafDirichletNodes The leaf Dirichlet dofs.
    */
    template <int ncomp, int mcomp, class Enable = std::enable_if_t<((ncomp == 1) and (mcomp >=1)) or (ncomp == mcomp)> >
    static void assembleDirichletBC(const GridType&,
                                    const Dune::BitSetVector<ncomp>& coarseDirichletNodes,
                                    Dune::BitSetVector<mcomp>& leafDirichletNodes);

    template <int mcomp, int ncomp>
    static void inflateBitField(const Dune::BitSetVector<mcomp>& inField,
                                Dune::BitSetVector<ncomp>& vectorField) {

        vectorField.resize(inField.size(),false);
        for (size_t i=0; i<inField.size();i++)
            if (inField[i].any())
                vectorField[i].flip();
    }
};

template <class GridType>
template <class VectorType, int ncomp>
void DirichletBCAssembler<GridType>::assembleDirichletBC(const GridType& grid,
                                    const Dune::ParameterTree& config,
                                    Dune::BitSetVector<ncomp>& leafDirichletNodes,
                                    VectorType& leafDirichletValues, std::string path)
{
    // coarse values
    Dune::BitSetVector<ncomp> coarseDirichletNodes;
    VectorType coarseDirichletValues(grid.size(0,dim));

    // try to read in or construct coarse boundary information from geometric criterions
    BoundaryPatch<typename GridType::LevelGridView> dummy(grid.levelGridView(0));
    if (BoundaryPatchFactory<typename GridType::LevelGridView>::setupBoundaryPatch(config, dummy,path))
        inflateBitField(*dummy.getVertices(),coarseDirichletNodes);
    else {

        std::cout << "Warning: No *_dnFile found in the parameter set, unset all Dirichlet dofs!\n";
        leafDirichletNodes.resize(grid.size(dim),false);
        leafDirichletValues.resize(grid.size(dim));
        leafDirichletValues = 0;
        return;
    }

    // read values from amira file
    if (config.hasKey("amira_dvFile")) {
#if HAVE_AMIRAMESH
        Dune::AmiraMeshReader<GridType>::readFunction(coarseDirichletValues, path+config.get("amira_dvFile",""));
#endif
    }

    // construct constant field
    else if (config.hasKey("constant_dv")) {
        using BlockType = typename VectorType::block_type;
        auto constant_dv = config.get<BlockType>("constant_dv");
        for (size_t i=0; i<coarseDirichletValues.size(); i++) {
            if (coarseDirichletNodes[i].any())
                coarseDirichletValues[i] = constant_dv;
        }
    // set zero field
    } else {
        std::cout << "Warning: No Dirichlet values found in the parameter set, assuming homogeneous ones!\n";
        coarseDirichletValues = 0;
    }

    // prolongate data to leaf grid
    DirichletBCAssembler<GridType>::assembleDirichletBC(grid,
                                        coarseDirichletNodes, coarseDirichletValues,
                                        leafDirichletNodes, leafDirichletValues);
}

template <class GridType>
template <int ncomp, int mcomp, class Enable>
void DirichletBCAssembler<GridType>::assembleDirichletBC(const GridType& grid,
                                    const Dune::BitSetVector<ncomp>& coarseDirichletNodes,
                                    Dune::BitSetVector<mcomp>& leafDirichletNodes)
{
    // prolong coarse to fine dofs
    // TODO: Is there a simpler method?...
    leafDirichletNodes.resize(grid.size(dim),false);
    for (int i=0; i<ncomp; i++) {

        Dune::BitSetVector<1> coarseDirichletComp(coarseDirichletNodes.size(), false);
        for (size_t j=0; j<coarseDirichletComp.size(); j++)
            coarseDirichletComp[j] = coarseDirichletNodes[j][i];

        // make boundary patch for that component
        BoundaryPatch<typename GridType::LevelGridView> coarsePatch(grid.levelGridView(0), coarseDirichletComp);
        BoundaryPatch<typename GridType::LeafGridView> finePatch;
        BoundaryPatchProlongator<GridType>::prolong(coarsePatch, finePatch);

        const Dune::BitSetVector<1>& fineNodes = *finePatch.getVertices();
        for (size_t j=0; j<leafDirichletNodes.size(); j++)
            leafDirichletNodes[j][i] = fineNodes[j][0];
    }
}

template <class GridType>
template <class VectorType, int ncomp, int mcomp, class Enable>
void DirichletBCAssembler<GridType>::assembleDirichletBC(const GridType& grid,
                                    const Dune::BitSetVector<ncomp>& coarseDirichletNodes,
                                    const VectorType& coarseDirichletValues,
                                    Dune::BitSetVector<mcomp> &leafDirichletNodes,
                                    VectorType& leafDirichletValues)
{

    DirichletBCAssembler::template assembleDirichletBC<ncomp, mcomp, Enable>(grid, coarseDirichletNodes, leafDirichletNodes);

    // make a nodal bases function
    using P1Basis = DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<typename GridType::LeafGridView> >;
    P1Basis p1Basis(grid.leafGridView());

    using CoarseP1Basis = DuneFunctionsBasis<Dune::Functions::PQ1NodalBasis<typename GridType::LevelGridView> >;
    CoarseP1Basis coarseP1(grid.levelGridView(0));

    //////////////////////////////////////////
    // Prolongate dirichlet values to the fine grid
    ////////////////////////////////////////

    // If we have parametrized boundaries we have to be careful with the prolongation

    typedef BasisGridFunction<CoarseP1Basis, VectorType> CoarseBasisGridFunction;
    CoarseBasisGridFunction coarseDirichletFunction(coarseP1,coarseDirichletValues);

    CoarseGridFunctionWrapper<CoarseBasisGridFunction> wrappedDir(coarseDirichletFunction);
    Functions::interpolate(p1Basis, leafDirichletValues, wrappedDir,leafDirichletNodes);
}


#endif
