#ifndef AMIRA_MESH_BASIS_WRITER_HH
#define AMIRA_MESH_BASIS_WRITER_HH


#include <dune/grid/io/file/amirameshwriter.hh>

#include <dune/fufem/functiontools/basisinterpolator.hh>
#include <dune/fufem/functionspacebases/p1nodalbasis.hh>
#include <dune/fufem/functionspacebases/p0basis.hh>


template<class B>
class AmiraMeshBasisWriter :
    public Dune::AmiraMeshWriter<typename B::GridView>
{
    public:
        typedef B Basis;
        typedef typename B::ReturnType ReturnType;
        typedef typename B::GridView GridView;

    protected:

        typedef P1NodalBasis<GridView, ReturnType> WriterP1Basis;
        typedef P0Basis<GridView, ReturnType> WriterP0Basis;

        typedef typename Dune::AmiraMeshWriter<typename B::GridView> Base;

    public:

        AmiraMeshBasisWriter(const Basis& basis, bool split=false) :
            Base(),
            basis_(basis),
            gridview_(basis.getGridView()),
            split_(split)
        {
            Base::addGrid(gridview_, split_);
        }


        virtual ~AmiraMeshBasisWriter()
        {
        }


        using Base::addVertexData;
        using Base::addCellData;
        using Base::write;


        // \todo this assumes that Vector is of block_level 2 meaning it's a BlockVector<FieldVector<K, size> >. is that okay?
        template <class Vector, class FunctionBasis>
        void addP1Interpolation(const Vector& v, const FunctionBasis& b)
        {
            WriterP1Basis p1Basis(gridview_);
            Vector p1Vector;
            Functions::interpolate(p1Basis, p1Vector, Functions::makeFunction(b, v));
            Base::addVertexData(p1Vector, gridview_, split_);
        }


        template <class Vector>
        void addP1Interpolation(const Vector& v)
        {
            addP1Interpolation(v, basis_);
        }


        // \todo this assumes that Vector is of block_level 2 meaning it's a BlockVector<FieldVector<K, size> >. is that okay?
        template <class Vector, class FunctionBasis>
        void addP0Interpolation(const Vector& v, const FunctionBasis& b)
        {
            WriterP0Basis p0Basis(gridview_);
            Vector p0Vector;
            Functions::interpolate(p0Basis, p0Vector, Functions::makeFunction(b, v));
            Base::addCellData(p0Vector, gridview_, split_);
        }


        template <class Vector>
        void addP0Interpolation(const Vector& v)
        {
            addP0Interpolation(v, basis_);
        }


    private:

        const Basis& basis_;
        const GridView gridview_;
        bool split_;
};

#endif

