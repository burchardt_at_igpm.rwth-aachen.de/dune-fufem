// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
#define DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH

#include <type_traits>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/fufem/boundarypatch.hh>


/** \brief For a given basis and boundary patch, determine all degrees
    of freedom on the patch.

    \param boundaryDofs Bit is set if corresponding dofs belongs to the boundary patch
*/
template <class GridView, class Basis, int blocksize>
void constructBoundaryDofs(const BoundaryPatch<GridView>& boundaryPatch,
                           const Basis& basis,
                           Dune::BitSetVector<blocksize>& boundaryDofs)
{
    // Check consistency of the input
    static_assert((std::is_same<GridView, typename Basis::GridView>::value),
                       "BoundaryPatch and global basis must be for the same grid view!");

    // ////////////////////////////////////////////////////////
    //   Init output bitfield
    // ////////////////////////////////////////////////////////

    boundaryDofs.resize(basis.size());
    boundaryDofs.unsetAll();


    for (auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it) {

        const auto& inside = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(inside).localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // //////////////////////////////////////////////////
            //   Test whether dof is on the boundary face
            // //////////////////////////////////////////////////

            unsigned int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if (it.containsInsideSubentity(entity, codim))
                boundaryDofs[ basis.index(inside, i) ] = true;
        }
    }
}

#endif   // DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
